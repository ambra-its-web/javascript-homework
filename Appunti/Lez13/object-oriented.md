## Idea
- Uso le classi per: **ISTANZIARE** ed **EREDITARE**
  - Istanzio oggetti con le __stesse proprietà e metodi__ (con default se propr facoltativa)
  - Ereditano proprietà e metodi specificati (esempio book, ebook e paperback, le propr e metodi aggiuntivi non sono del book)
    - NON è come la factory di java, solo esempio per capire
  
## Esempio slide 6

```js
// Canine is called a Constructor Function
// typeof Canine is 'function'
let Canine = function (latinName) {
  this.genus = 'Canis'; // si riferisce all'oggetto corrente in creazione
  this.latinName = latinName;
};
// Use the new keyword to create new instances of this "class"
let dog = new Canine('Canis familiaris'); // { genus: 'Canis', latinName: 'Canis familiaris' }
let greyWolf = new Canine('Canis lupus'); // { genus: 'Canis', latinName: 'Canis lupus' }
```
- Canine è una funzione
- Sarebbe invocabile normalmente senza `new` *SUCCEDEREBBERO ROBE STRANE*
  - Ma usando **`new`** viene invocata come **FUNZIONE COSTRUTTRICE**
- `dog` sarà di tipo object, anche senza return `new Canine` ritornerà il nuovo oggetto e NON undefined
  
Per convenzione le funzioni constructor hanno la 1a lettera maiuscola
```js
Canine.prototype.howl = function () { // prototype è built in ad ogni valore di tipo function
  console.log('AAAAWWWOOOOOO');
};
dog.howl(); //  AAAAWWWOOOOOO
greyWolf.howl(); //  AAAAWWWOOOOOO
7
```
- Ogni valore di tipo function ha la proprietà `prototype` (sembra strano ma è come chiedersi perché string.length esista)
- Se lo metti qua, ogni istanza si riferisce ad una singola funzione (metodo), mentre se lo faccio nel costruttore ogni istanza avrebbe una sua funzione uguale per tutti (spreco)
- Per aggiungere metodi, devi passare per la proprietà `prototype` obbligatoriamente

### PLOT TWIST
- LA **PROPRIETA' PROTOTYPE** esiste solo per le **FUNZIONI**
- IL **PROTOTYPE** esiste di OGNI oggetto in javascript, anche non creati tramite `new`
- **`Canine` ha sia un prototype che una proprietà prototype** (le funzioni sono oggetti in js)
- **`dog` ha solo un prototype** (non è una funzione, ma è un oggetto)

## CHAIN DI PROTOTYPE
**[[prototype]]** = convenzione testuale per dire la **proprietà**

- è una propr nascosta che NON dovrebbe essere toccata
- vi si può accedere con `__proto__` (accessor property)
  - **NON usarla MAI**
  - per js, tutto ciò che è built-in con `_` non andrebbe toccato
  - `__proto__` viene usato da js di nascosto per la chain di prototype
  - non è ufficiale, potenzialmente deprecato
---
![alt text](image-1.png)
Se JS non trova una propr in un oggetto, la cerca nel prototype. Si ferma quando la trova. Se non lo trova arriva ad Object (il godfather), ritorna *undefined*. Il `null` nello schema significa solo che Object è l'ultimo

Infatti... (???)
```js
let x = 'ciao';   /   ==  /  'ciao'.toUpperCase();
x.toUpperCase();  /   ==  /
```

più oggetti possono puntare allo stesso prototipo ovviamente
```js
let x = 'ciao';
let y = 'hi'
x.toUpperCase();
y.toUpperCase();
```
- Il prototype di questi due è lo stesso, e i loro prototype saranno gli stessi
- x ed y sono fatti dalla stessa "fabbrica" di nascosto e puntano al loro prototype String, che ha i metodi
- JS implicitamente usa il new con il prototype dell'oggetto

Così posso aggiungere roba anche agli Ogg grossi (per Es 1 e 3)

es:
```js
let x = 'ciao';
x.magic();

String.prototype.magic = function() {
    console.log(`${this} wohoooo`) // per questo senza arrow function
}
```

Ora so cosa significa questo: `String.prototype.includes()` e altri titoli di MDB;
1. metodo includes
2. lo ha la stringa? no? 
3. lo ha il prototipo della stringa, l'oggetto String? sì
4. il metodo si applica sulla stringa usando il metodo del prototipo

## Creare oggetti senza constructor
In realtà quello che facciamo è settare manualmente il prototype dell'oggetto
Questo col metodo `create()` di Object:
"Creami un oggetto e assegnagli il prototipo che voglio io

```js
let person = {
  heart: 'black'
};
let adam = Object.create(person); // la funz crea ogg adam, assegna ad esso il prototipo person
let sam = Object.create(person);
console.log(adam.heart); // black, dal prototype di adam che è person
console.log(sam.heart); // black dal prototype di sam che è person
adam.heart = 'big';
console.log(adam.heart); // big, perché adam ha la proprietà
console.log(sam.heart); // black dal prototype di sam che è person
```

## Prendere e settare prototypes
- `Object.getPrototypeOf()`
- `Object.setPrototypeOf()`

Fanno quel che fa `__proto__` e parte di quel che `Object.create()` fa (senza la creazione)
Metodi migliori, ma andrebbe evitato il cambiamento di un prototype

## Definendo meglio le funzioni constructor e metodi
Voglio che tutti i libri che escono da questa funzione costruttrice abbiano stesse propr e metodi, che saranno nel prototype di ogni libro

```js
// constructor function
function Book(title, author, numPages) { // la maiusc segnala anche di dover usare il new nell'invocazione
  // The properties of this object
  this.title = title;
  this.author = author;
  this.numPages = numPages;
  this.currentPage = 0;
  // nessun return, tornerà oggetto perché sarà invocata con new
} 
// ha una proprietà prototype


// adding a method to the prototype object
Book.prototype.read = function () {
  this.currentPage = this.numPages;
  console.log('You read ' + this.numPages + ' pages!');
};
// instantiating a new Book object
let book = new Book('Robot Dreams', 'Isaac Asimov', 320);
book.read();
```
- Book è la funzione costruttrice
- usa il this, il book non esiste ancora, il THIS sarà riferito all'oggetto (prima nasce il bambino, poi diamo il nome book praticamente)
- aggancia il this all'oggetto
- fa puntare l'oggetto al prototipo
- Prende i parametri e con `this` li affida al libro come valori di proprietà
- L'oggetto viene creato dopo e chiamato `book`

^(questo dentro la funz)^

- si **aggiunge un metodo al prototipo di book**
- si stanzia la creazione di un nuovo oggetto Book chiamato book

## Estendere gli oggetti
ste due cose sono uguali, ma senza il this
```js
function alerberto(a,b) {
  console.log('arrivederci alerberto');
  return a+b;
}

let me = {};
alerberto(3,4);
alerberto.call(me,3,4); //a che cazz serve
```
.call cerca manualmente in alerberto() il metodo o proprietà

*Speculazione di lore:*
ste cose sono utili per GROSSE librerie di proprietà o metodi. se non so di CHI sia tra gli oggetti, chiamo il padre e funziona uguale

le funz costruttrici NON assegnano il loro prototype agli oggetti che generano! in più, l'oggetto è completamente sconnesso da tutta la "catena" una volta finita la sua creazione

ora con il this
```js
// constructor function
function PaperBack(title, author, numPages, cover) {
  Book.call(this, title, author, numPages);
  this.cover = cover;
}
// extending the Book prototype object
PaperBack.prototype = Object.create(Book.prototype);
// adding a method to PaperBack's prototype object
PaperBack.prototype.burn = function () {
  console.log('Omg, you burnt all ' + this.numPages + ' pages');
  this.numPages = 0;
};
// instantiating a new PaperBack object
let paperback = new PaperBack('1984', 'George Orwell', 250, 'cover.jpg');
paperback.read();
paperback.burn();
```
Qui Book NON è una funzione costruttrice, la chiamo in un'altra funz costruttrice per fare una "*sottocategoria*" di oggetto

```js
let paperback = PaperBack.call(this, '1984', 'George Orwell', 250, 'cover.jpg');
```
è (quasi) uguale a
```js
let paperback = new PaperBack('1984', 'George Orwell', 250, 'cover.jpg');
```

## Costruttori cleaner e oggetti di configurazione

ignorati male

## Optional properties,
```js
I pipe per le proprietà default
// some properties can be made optional by assigning default values
function Book(config) {
    // le prime 3 propr hanno un valore di default per evitare undefined
  this.title = config.title || 'Untitled';
  this.author = config.author || 'Unknown';
  this.numPages = config.numPages || 100;
  this.currentPage = 0;
}
let book = new Book({
  title: 'Robot Dreams',
  numPages: 320
});
```

## Estensione di oggetti
```js
// constructor function
function PaperBack(title, author, numPages, cover) {
  Book.call(this, title, author, numPages);
  this.cover = cover;
}
```
il `this` è l'oggetto appena creato

```js
// extending the Book prototype object
PaperBack.prototype = Object.create(Book.prototype);
```
se non trovo la propr dentro a paperback, cerco nel prototype etc etc

Ho creato un oggetto che ha ereditato le propr da book, e ne ha di aggiuntive grazie al prototipo settato manualmente

# JAVASCRIPT MODERNO

## Classi

```js
class Person {
 constructor(name) { // crea l'istanza di oggetto di tipo Person, gli setta una proprietà
   this.name = name; /* qui guy.prototype è undefined, perché guy è un oggetto e io non lo setto qui. 
   lo hanno le funzioni per poter settarlo come prototype degli oggetti che creano */
   // i metodi MAI nel costruttore
 }
 speak() {
   return 'My name is ' + this.name;
 }
}
class Teacher extends Person { // stessa cosa di Object.create e call(), estendo la classe esistente
 speak() {
   return super.speak() + ', I am a teacher'; // super prende dalla classe da cui eredita, come scrivere Person.prototype.speak.call(this)
 }
}
let teach = new Teacher('James');
let guy = new Person('Billy');
console.log(teach.speak()); // Output: 'My name is James, I am a teacher'
console.log(guy.speak()); // Output: 'My name is Billy'
```

Sono MOLTO più meglierrimo dei prototipi

## Proprietà e metodi statici

I **metodi statici** sono assegnati alla funzione class stessa, non al prototipo

è come JavaScript


Qui è utile perché mi permette di contare tutte le macchine create dalla funzione, dall'esterno sarebbe più difficile.

Solitamente si fa con utility che servono a tutte le istanze e non solo una
```js
// static methods and properties are assigned to the class function itself, not to its "prototype"
class Car {
  constructor(color) {
    this.color = color; // this refers to the new instance
    Car.instances += 1;
  }
  static instances = 0;
  static getInstances = function () {
    return this.instances; // this refers to Car (la classe)
    // return Car.instances; // could have used Car instead of this
  };
}
const cars = [new Car('red'), new Car('green'), new Car('orange')];
console.log(Car.getInstances()); // 3
```

## Proprietà private, protette e pubbliche
```js
class BankAccount {
  #widthdrawLimit = 500; // # per privato, che sia proprietà o metodo
  #limitedWithdraw(amount) {
    if (amount < 0) return 0; // se chiedo di prelevare -50 euro, non prelevi
    // se superi il limite, ti dà solo il limite
    if (amount > this.#widthdrawLimit) return this.#widthdrawLimit; // si scrive # anche qui
    return amount;
  }
  withdraw(amount) { // metodo pubblico obv, unica cosa visibile da fuori
    return this.#limitedWithdraw(amount); // GETTER???? SUS????
  }
}
```

Prima si faceva con l'underscore _ la privatizzazione delle propr e metodi

## Shorthand

non farle mai

## Proprietà dinamiche pazzerelle

Per poter sapere il nome della propr deve essere eseguito del codice
```js
const getStatus = function () {
  return 'employee';
};
const person = {
  name: 'adam',
  [getStatus() + '_' + 'number']: 13324
};
console.log(person.employee_number); // 13324
```

## GET e SET e let e met e wet e yet e pet e net e vet e ket e met e tet

I get mi permettono di vedere e leggere il valore di una proprietà da fuori

I set mi permettono di modificare o inserire il valore di una proprietà da fuori

```js
class Bank {
  #money; // puoi interpretarla come una propr fittizia, dato che neanche sarà visibile
  constructor(money) {
    this.#money = money;
  }
  get money() { // GETTER
    return this.#money < 1000 ? this.#money : 'you are rich!'; // ti ritorno il valore di money registrato nella propr privata della classe se <1000, sennò 'you are rich!'
  }
  set money(newMoney) { // SETTER
    this.#money = newMoney < 0 ? 0 : newMoney; // se mi dai newMoney < 0 ti restituisco 0
  }
}
```

```js
const bank = new Bank(80);
console.log(bank.money); // 80
bank.money = 30;
console.log(bank.money); // 30
bank.money = -100; // setting
console.log(bank.money); // 0, si tenta di accedere a bank.money, viene invocato il getter 
bank.money = 1300;
console.log(bank.money); // 'you are rich!'
```