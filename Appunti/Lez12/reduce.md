## REDUCE

metodo di Array che permette di iterare su un array e ritornare un valore (tendenzialmente singolo), (anche più, ma tende a ridurre)

prende: 1 una funzione di call back e la invoca per ogni elem dell'array (tante volte quanto è lungo l'array)
2 il valore iniziale che sarà assegnato all'accumulatore
```js
let numbers = [1, 2, 3, 4];
 
// using for
let totalNumber = 0;
for (let i = 0; i < numbers.length; i++) {
  totalNumber += numbers[i] * 2;
}
 
// using reduce
let totalNumber2 = numbers // applico all'array
  .map(function (number) { // metodo map qui perché ne voglio una mappa
    return number * 2;
  }) // il big boi è qua sotto
  .reduce(function (total, number) { // 1° param, funz di callback

    return total + number; // cicla l'array e per ogni suo elemento invoca la funz e ritorno manualmente. SE NON METTO RETURN ESPLICITO È UNDEFINED. il val di ritorno passa all'accumulatore nell'invocazione della callback della prox iteraz

  }, 0); // 0 è il val iniz. dell'accumulatore, posso omettere ma meglio specificare anche se 0
  // tendenzialmente deve matchare cosa contengono i valori, (tipo array vuoto se è un array di array(?))
```

(ricorda: in js le funzioni possono essere passate come valori a funzioni o assegnate a variabili, sono come ogni altro tipo di valore)