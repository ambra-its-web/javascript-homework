/**
* @file: main.js
* @author: Fabri
* Introduces logic printed on the console accordingly with the specifcations of the n exercise of unit m.
* 
*/

/**
 * Returns a grade for the given score.
 * @param {number} score - The score to be graded.
 * @returns {string} - The grade.
 */
function assignGrade(score) {
    if (score >= 90) {
        return "A";
    } else if (score >= 80) {
        return "B";
    } else if (score >= 70) {
        return "C";
    } else if (score >= 60) {
        return "D";
    } else {
        return "F";
    }
}

// Call assignGrade function for different scores and log the result
console.log(assignGrade(95)); // Output: A
console.log(assignGrade(82)); // Output: B
console.log(assignGrade(73)); // Output: C
console.log(assignGrade(65)); // Output: D
console.log(assignGrade(50)); // Output: F