## Author
- **Name:** Fabrizio Cristian Duca
- **Contact:** fabrizio.duca@edu.itspiemonte.it

## Exercise: Assign Grade Function

### Requirements
- Write a function named `assignGrade` that:
  - Takes 1 argument, a number score.
  - Returns a grade for the score, either "A", "B", "C", "D", or "F".
- Call that function for a few different scores and log the result to make sure it works.

### Approach to Solution
I defined a function `assignGrade` that takes one argument: `score`. Within the function:
- Used conditional statements to determine the grade based on the provided score.
- Called the function for different scores and logged the result to the console to test its functionality.

### Files
- `main.js`: Contains the implementation of the `assignGrade` function.
- `index.html`: HTML file with a reference to the `main.js` file.

To view the results, open `index.html` in a web browser and check the console.