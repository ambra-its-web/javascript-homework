## Author
- **Name:** Fabrizio Cristian Duca
- **Contact:** fabrizio.duca@edu.itspiemonte.it

## Exercise: Hello World Function

### Requirements
- Write a function named `helloWorld` that:
  - Takes 1 argument, a language code (e.g. "es", "de", "en").
  - Returns "Hello, World" for the given language, for at least 3 languages. It should default to returning English.
- Call that function for each of the supported languages and log the result to make sure it works.

### Approach to Solution
I defined a function `helloWorld` that takes one argument: `languageCode`. Within the function:
- Used a switch statement to return the greeting message based on the provided language code.
- Called the function for different languages and logged the result to the console to test its functionality.

### Files
- `main.js`: Contains the implementation of the `helloWorld` function.
- `index.html`: HTML file with a reference to the `main.js` file.

To view the results, open `index.html` in a web browser and check the console.