/**
* @file: main.js
* @author: Fabri
* Introduces logic printed on the console accordingly with the specifcations of the n exercise of unit m.
* 
*/

/**
 * Function helloWorld
 * Returns "Hello, World" in the given language.
 * @param {string} languageCode - Language code (e.g., "es", "de", "en").
 * @returns {string} - Greeting message.
 */
function helloWorld(languageCode) {
    switch (languageCode) {
        case "es":
            return "¡Hola, Mundo!";
        case "de":
            return "Hallo, Welt!";
        default:
            return "Hello, World";
    }
}

// Invoke helloWorld function for different languages and log the result
console.log(helloWorld("en")); // Output: Hello, World
console.log(helloWorld("es")); // Output: ¡Hola, Mundo!
console.log(helloWorld("de")); // Output: Hallo, Welt!