/**
* @file: main.js
* @author: Fabri
* Introduces logic printed on the console accordingly with the specifcations of the 1st exercise of unit 3.
* **Global Scope:**
  - Declares the global variable `varGlob`.
  - Defines the function `addNumbers` to increment `varGlob`.
  - Showcases changes to `varGlob` both within and outside the function.

* **Local and Global Scope Interaction:**
  - Introduces `multiplyByvar`, a function that uses both local and global variables by multiplying
    a given parameter by `varLoc` and `varGlob`, printing both separately.
  - Attempts to access the local variable `varLoc` outside of the function it's declared in, 
    resulting in an error due to its limited scope.
*/

// example with a global variable
let varGlob = 10;

console.log("Global variable outside of the function, initial value:", varGlob);

/**
 *  Returns varGlob + 1
 * @param  {number} varGlob - global variable, the only parameter
 * @returns {number} varGlob - modified value, varGlob + 1
 */
function addNumbers() {
    varGlob++;
    console.log("Global variable within the sum function, modified value:", varGlob);
}

addNumbers();// 3 function invocations
addNumbers();
addNumbers();
console.log("Global variable outside of the function:", varGlob,); //printing varGlob outside of the function, it keeps its modified value


console.log(""); // printing an empty line into the console to better visualize the two different steps


// function multiplyByvar
// Function that uses both local and global variables
/**
 *  Returns the parameter multiplied by the two variables, global and local
 * @param  {number} num - number to multiply varLoc by, the only parameter
 * @returns {number, number} 1)localResult - result of num multiplied by varLoc, 2)globalResult - result of num multiplied by varGlob
 */
function multiplyByvar(num) {
    let varLoc = 2; // local variable
    let localResult = num * varLoc // result variable which consists of the expression num multiplied by varLoc
    let globalResult = num * varGlob; // result variable which consists of the expression num multiplied by varGlob
    console.log("Local variable in the multiplication function:", varLoc);
    // refer to the comments from line 60 to 62 for the example of using a local variable outside of the function
    console.log("Result of ", num, " times ",varLoc, ": ", localResult);
    console.log("Result of ", num, " times ", varGlob, ": ", globalResult);
}

multiplyByvar(5); // invoking the multiplication function with 5 as argument (value of num)
// console.log("Local variable outside of the function:" + varLoc);
// using a local variable outside of its function, which is its scope in this case
// generates an error because it's only defined within its scope