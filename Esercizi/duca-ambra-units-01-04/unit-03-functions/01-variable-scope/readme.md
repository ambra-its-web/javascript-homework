#### Author Dageils
- **Name:** Fabrizio Cristian Duca
- **Contact:** fabrizio.duca@edu.itspiemonte.it

## Exercise 1.Variable Scope

### Requirements
● Recreate the local and global scope examples in your browser
● Try to call the function “addNumbers” a few more times
● Write a new .js file that uses both local and global variables in the same
project
● Make sure that you understand exactly what’s happening at every stage

#### Approach to Solution
- **Global Scope:**
  - Declared the global variable `varGlob`.
  - Defined the function `addNumbers` to increment `varGlob`.
  - Showcased changes to `varGlob` both within and outside the function.

- **Local and Global Scope Interaction:**
  - Introduced `multiplyByvar`, a function that uses both local and global variables by multiplying a given parameter by `varLoc` and    
`varGlob`, printing both separately.
  - Attempted to access the local variable `varLoc` outside of the function it's declared in, resulting in an error due to its limited scope.

### Files
- `main.js`: Contains the implementation of the `addNumbers` and `multiplyByVar` functions.
- `index.html`: HTML file with a reference to the `main.js` file.

To view the results, open `index.html` in a web browser and check the console.