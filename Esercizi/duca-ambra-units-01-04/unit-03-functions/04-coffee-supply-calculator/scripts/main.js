// Definizione della funzione calculateSupply
function calculateSupply(age, caffeAlgg) {
    // Costante per l'età massima
    const ageMax = 90;

    // Calcola il consumo totale di caffè per il resto della vita
    let anniRimasti = ageMax - age;
    let quantNecessaria = Math.round(anniRimasti * 365 * caffeAlgg);

    // Stampa il risultato
    console.log("Avendo " + age + " anni " + "avrai bisogno di " + quantNecessaria + " tazze di caffè per durare fino all'età di " + ageMax + ".");
}

// Invocazione alla funzione calculateSupply con valori diversi per l'età e la quantità giornaliera
calculateSupply(22, 2.5); // Invocazione con 30 anni di età e 2 tazze di caffè al giorno
calculateSupply(40, 2); // Invocazione con 40 anni di età e 1.5 tazze di caffè al giorno (0.3 litri di caffè)
calculateSupply(25, 3); // Invocazione con 25 anni di età e 3 tazze di caffè al giorno