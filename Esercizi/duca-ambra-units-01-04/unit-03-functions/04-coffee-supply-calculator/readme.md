#### Author Dageils
- **Name:** Fabrizio Cristian Duca
- **Contact:** fabrizio.duca@edu.itspiemonte.it

## Exercise: Coffee Supply Calculator

### Requirements
- Write a function named `calculateSupply` that:
  - Takes 2 arguments: age and amount per day.
  - Calculates the amount consumed for the rest of life (based on a constant max age).
  - Outputs the result to the screen like so: "You will need NN cups of coffee to last you until the age of X"
- Call that function three times, passing in different values each time.
- Bonus:
  - Calculate in liters, accepting floating-point values for amount per day (e.g., 0.3 liters of coffee).
  - Round the result to a round number.

### Approach to Solution
I defined a function `calculateSupply` that takes two arguments: `age` (age) and `caffeAlgg` (amount of coffee per day). Within the function:
- I set a constant `ageMax` representing the maximum age.
- Calculated the total coffee consumption for the remaining life using the formula: `(ageMax - age) * 365 * caffeAlgg`.
- Rounded the result to a whole number using `Math.round()`.
- Printed the result to the console in the requested format.

Finally, I called the `calculateSupply` function three times with different values for age and daily coffee consumption to test its functionality.

### Files
- `main.js`: Contains the implementation of the `calculateSupply` function.
- `index.html`: HTML file with a reference to the `main.js` file.

To view the results, open `index.html` in a web browser and check the console.