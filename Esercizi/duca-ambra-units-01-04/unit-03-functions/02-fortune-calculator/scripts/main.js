/**
* @file: main.js
* @author: Fabri
* Introduces logic printed on the console accordingly with the specifcations of the 2nd exercise of unit 3.
* Defines a function `tellFortune` that takes four arguments: 
    `children`, `partner`, `geoLoc`, and `jobTitle`. 
* Within the function, the string `fortune` is declared, representing the fortune told based on the provided arguments. 
* It includes the job title (`jobTitle`), geographic location (`geoLoc`), partner's name (`partner`), and the number of children (`children`). 
* Finally, it prints the fortune string to the console. It's called three times with different values to test its functionality.
*/

// function tellFortune
// Function that has 4 parameters and prints a string using them
/**
 *  Returns the parameter multiplied by the two variables, global and local
 * @param  {number, string, string, string} children - amount of children,  partner - who they'll be married to, geoLoc - where they're going to work, jobTitle - what job they'll have
 * @returns {null} - the result of the function is the printing it does within itself
 */
function tellFortune(children, partner, geoLoc, jobTitle) {
    // variable fortune, a string with multiple variables in it
    let fortune = "You will be a " + jobTitle + " in " + geoLoc + ", and married to " + partner + " with " + children + " kids.";
    
    // print of the fortune told
    console.log(fortune);
}

// invoking the function tellFortune with different argument values
tellFortune(0, "nessuno", "Copenhagen", "programmatore");
tellFortune(3, "qualcuno", "Milano", "fumettista");
tellFortune(1, "Martina", "Torino", "clavicembalista");