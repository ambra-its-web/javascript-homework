#### Author Dageils
- **Name:** Fabrizio Cristian Duca
- **Contact:** fabrizio.duca@edu.itspiemonte.it

## Exercise 2. Fortune Calculator

### Requirements
- Write a function named `tellFortune` that:
  - Takes 4 arguments: number of children, partner's name, geographic location, job title.
  - Outputs your fortune to the screen like so: "You will be a X in Y, and married to Z with N kids."
- Call that function 3 times with 3 different sets of values for the arguments

### Approach to Solution

For this exercise, I defined a function `tellFortune` that takes four arguments: `children`, `partner`, `geoLoc`, and `jobTitle`. Within the function, I declared the string `fortune` which represents the fortune based on the provided arguments. It includes the job title (`jobTitle`), geographic location (`geoLoc`), partner's name (`partner`), and the number of children (`children`). Finally, I printed the fortune string to the console.
I called the `tellFortune` function three times with different sets of values for the arguments to test its functionality.
