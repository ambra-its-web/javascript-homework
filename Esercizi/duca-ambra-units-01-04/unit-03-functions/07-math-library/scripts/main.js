/**
 * @file: main.js
 * @author: Ambra
 * @description: Defines and calls 4 functions, each printing their result rounded to 2 decimals
 * - squareNumber: handles the squaring of a number
 * - halfNumber: handles the halving of a number
 * - percentOf: calculates what percentage of number2 number1 is
 * - areaOfCircle: handles the calculation of the area of a circle of a given radius 
 * Then invokes the two functions
*/

/**
 * Function that squares a number and prints the rounded result in a string 
 * @param {Number} - A number to be squared
 * @returns {void}
*/
function squareNumber(number) {
    let squaredNumber = number*number;

    // Prints the result, rounding it to 2 decimals
    console.log(number + ' squared is ' + squaredNumber.toFixed(2))
}

/**
 * Function that halves a number and prints the rounded result in a string 
 * @param {Number} - A number to be halved
 * @returns {void}
*/
function halfNumber(number) {
    let halvedNumber = number/2;

    // Prints the result, rounding it to 2 decimals
    console.log(number + ' halved is ' + halvedNumber.toFixed(2))
}

/**
 * Function that calculates what percentage a number is of another number
 * then prints the rounded result in a string
 * @param {Number} - number1, the dividend
 * @param {Number} - number2, the divisor
 * @returns {void}
*/
function percentOf(number1, number2) {
    // Calculates the percentage value of number1 for number2
    let percentage = (number1/number2)*100;

    // Prints the result, rounding it to 2 decimals
    console.log(number1 + ' is ' + percentage.toFixed(2) + ' of ' + number2)
}

/**
 * Function that takes a radius and prints its resulting circle area in a string
 * @param {radius} - The radius of the circle
 * @returns {void}
*/
function areaOfCircle(radius) {
    // Calculates the area of the circle with the formula A = pi * r^2
    let area = Math.PI * Math.pow(radius, 2);

    // Prints the result, rounding it to 2 decimals
    console.log("A circle with radius " + radius + " has an area of " + area.toFixed(2));
}

// Series of function invocations
squareNumber(2);
squareNumber(-2);

halfNumber(2);
halfNumber(3);

percentOf(1, 10);
percentOf(20, 35);

areaOfCircle(1);
areaOfCircle(-4);
areaOfCircle(6);