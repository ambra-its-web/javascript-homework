// Definizione della funzione calculateDogAge
function calculateDogAge(age, convValue = 7) { //
    // Calcola l'età del cane in anni canini
    let dogAge = age * convValue;

    // Stampa risultato
    console.log("Your dog is " + dogAge + " years old in dog years!");
}

// Invocazione della funzione calculateDogAge con più valori per l'età del cane
calculateDogAge(5); // Invocazione con età del cane a 5 anni (il valore predefinito di conversione 7)
calculateDogAge(12); // Invocazione con età del cane a 12 anni (il valore predefinito di conversione 7)
calculateDogAge(12, 5); // Invocazione con età del cane a 12 anni (valore di conversione 5)