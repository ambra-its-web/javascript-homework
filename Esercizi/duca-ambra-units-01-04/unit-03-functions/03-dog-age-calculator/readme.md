#### Author Dageils
- **Name:** Fabrizio Cristian Duca
- **Contact:** fabrizio.duca@edu.itspiemonte.it

## Exercise 3. Dog Age Calculator

### Requirements
- Write a function named `calculateDogAge` that:
  - Takes 1 argument: the dog's age in human years.
  - Calculates the dog's age based on the provided or default conversion rate.
  - Outputs the result to the screen like so: "Your dog is NN years old in dog years!"
- Call the function three times with different sets of values for the arguments.
- Bonus:
  - Add another argument to the function that takes the conversion rate of human to dog years

### Approach to Solution
I defined a function `calculateDogAge` that takes one required argument `age` (the dog's age in human years) and one optional argument `convValue` (the conversion rate of human to dog years, defaulting to 7). Within the function, I calculated the dog's age in dog years by multiplying the human age by the provided conversion rate, or the default one if not specified. I then printed the result to the console in the requested format.

Finally, I called the `calculateDogAge` function three times with different values for the dog's age in human years and conversion rates to test its functionality.

### Files
- `main.js`: Contains the implementation of the `calculateDogAge` function.
- `index.html`: HTML file with a reference to the `main.js` file.

To view the results, open `index.html` in a web browser and check the console.