/**
 * @file: main.js
 * @author: Ambra
 * @description: Creates two functions that convert a set temperature 
 * from Celsius to Fahrenheit and viceversa, and both log their result within the function.
 * Then invokes the two functions
*/

/**
 * Function that, to convert a celsius temperature to a fahrenheit one:
 * stores a number variable, then defines another variable
 * whose value is an expression involving the previous one, then finally
 * prints a string to the console involving both variables
 * @param {void}
 * @returns {void}
*/
function celsiusToFahrenheit() {
    let celsius = 30;

    // Conversion formula: (°C × 9/5) + 32 = °F
    let fahrenheit = (celsius * 9/5) + 32;

    // Output the result
    console.log(celsius + ' C° is ' + fahrenheit + '°F');
}

/**
 * Function that, to convert a fahrenheit temperature to a celsius one:
 * stores a number variable, then defines another variable
 * whose value is an expression involving the previous one, then finally
 * prints a string to the console involving both variables
 * @param {void}
 * @returns {void}
*/
function fahrenheitToCelsius() {
    let fahrenheit = 30;

    // Conversion formula: (°F − 32) × 5/9 = °C
    let celsius = (fahrenheit - 32) * 5/9;

    // Output the result
    console.log(fahrenheit + ' F° is ' + celsius + "°C");
}

// Invoking the functions
celsiusToFahrenheit();
fahrenheitToCelsius();