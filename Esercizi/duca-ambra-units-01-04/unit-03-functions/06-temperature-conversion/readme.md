#### Author
- **Name:** Ambra Duca

## Exercise: Temperature conversion

### Requirements
Create a function called celsiusToFahrenheit:
- Store a celsius temperature into a variable.
- Convert it to fahrenheit and output "NN°C is NN°F".
Create a function called fahrenheitToCelsius:
- Now store a fahrenheit temperature into a variable.
- Convert it to celsius and output "NN°F is NN°C."

### Approach to Solution
I defined two functions: `celsiusToFahrenheit` and `fahrenheitToCelsius`. 
- The `celsiusToFahrenheit` function declares a number variable `celsius` with a value, declares another one `fahrenheit` whose value is the conversion formula from celsius to fahrenheit, then prints a string to showcase the conversion.
- The `fahrenheitToCelsius` function declares a number variable `fahrenheit` with a value, declares another one `celsius` whose value is the conversion formula from fahrenheit to celsius, then prints a string to showcase the conversion.

### Files
- `main.js`: Contains the implementation of the exercise code.
- `index.html`: HTML file with a reference to the `main.js` file.

To view the results, open `index.html` in a web browser and check the console.