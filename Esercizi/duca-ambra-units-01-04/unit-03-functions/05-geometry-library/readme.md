#### Author Dageils
- **Name:** Fabrizio Cristian Duca
- **Contact:** fabrizio.duca@edu.itspiemonte.it

## Exercise: Geometry Library

### Requirements
- Create a function called `calcCircumference`:
  - Pass the radius to the function.
  - Calculate the circumference based on the radius, and output "The circumference is NN".
- Create a function called `calcArea`:
  - Pass the radius to the function.
  - Calculate the area based on the radius, and output "The area is NN".

### Approach to Solution
I defined two functions: `calcCircumference` and `calcArea`. 
- The `calcCircumference` function takes the radius as input, calculates the circumference using the formula `2 * Math.PI * raggio`(radius), and prints the result to the console.
- The `calcArea` function also takes the radius as input, calculates the area using the formula `Math.PI * Math.pow(raggio, 2)`, and prints the result to the console.

`Math.pow` is a built-in JS function that returns the first argument to the power of the second argument.
`toFixed(2)` rounds the results to two decimal places.

### Files
- `main.js`: Contains the implementation of the `calcCircumference` and `calcArea` functions.
- `index.html`: HTML file with a reference to the `main.js` file.

To view the results, open `index.html` in a web browser and check the console.