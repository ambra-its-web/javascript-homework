// Funzione per calcolare la circonferenza
function calcCircumference(raggio) {
    // Calcolo della circonferenza
    let circonferenza = 2 * Math.PI * raggio;

    // Stampa risultato
    console.log("La circonferenza è " + circonferenza.toFixed(2));
}

// Funzione per calcolare l'area
function calcArea(raggio) {
    // Calcola l'area utilizzando la formula A = pi * r^2
    let area = Math.PI * Math.pow(raggio, 2);

    // Stampa risultato
    console.log("L'area è " + area.toFixed(2));
}

// Invocazione della funzione calcCircumference
calcCircumference(5);

// Invocazione della funzione calcArea
calcArea(5);