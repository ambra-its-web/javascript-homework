/**
 * @file: main.js
 * @author: Ambra
 * @description: Defines 5 functions, the first 4 returning a value
 * - squareNumber: handles the squaring of a number
 * - halfNumber: handles the halving of a number
 * - percentOf: calculates what percentage of number2 number1 is
 * - areaOfCircle: handles the calculation of the area of a circle of a given radius 
 * - calculator: while printing logs of each step and storing each result
 *  - takes one number
 *  - uses halfNumber to halve it
 *  - uses squareNumber to square that result
 *  - uses areaOfCircle to calculate the area of the value used as radius
 *  - uses percentOf to calculate what percentage that area is of the squared result from squareValue
 * Then invokes the function multiple times and prints an empty line between each
*/

/**
 * Function that squares a given number, returning it after
 * @param {Number} - A number to be squared
 * @returns {Number} - the squared number
*/
function squareNumber(number) {
    // Squaring of the number
    let squaredNumber = number*number;

    // Returns the result
    return squaredNumber;
}

/**
 * Function that halves a given number and returns it after
 * @param {Number} - A number to be halved
 * @returns {Number} - the halved number
*/
function halfNumber(number) {
    // Halving of the number
    let halvedNumber = number/2;

    // Returns the result
    return halvedNumber;
}

/**
 * Function that calculates what percentage a given number is of another given number, returning it after
 * @param {Number} - number1, the dividend
 * @param {Number} - number2, the divisor
 * @returns {Number} - the percentage
*/
function percentOf(number1, number2) {
    // Calculates the percentage value of number1 for number2
    let percentage = (number1/number2)*100;

    // Returns the result
    return percentage;
}

/**
 * Function that takes a radius and gets the resulting circle area, returning it after
 * @param {radius} - The radius of the circle
 * @returns {void}
*/
function areaOfCircle(radius) {
    // Calculates the area of the circle with the formula A = pi * r^2
    let area = Math.PI * Math.pow(radius, 2);

    // Returns the result
    return area;
}


/**
 * Function that takes a number and does a series of calculations through other functions
 * - while printing logs of each step and storing each result
 * - takes one number
 * - uses halfNumber to halve it
 * - uses squareNumber to square that result
 * - uses areaOfCircle to calculate the area of the value used as radius
 * - uses percentOf to calculate what percentage that area is of the squared result from squareValue
 * @param {radius} - The radius of the circle
 * @returns {void}
 * 
*/
function calculator(number) {

    // Halves the number with a function
    let halvedNumber = halfNumber(number);

    // Logs the step, will do so again every step
    console.log(number + ' was halved to ' + halvedNumber);

    // Squares the halved number with a function
    let squaredNumber = squareNumber(halvedNumber);
    console.log(halvedNumber + ' was squared to ' + squaredNumber);

    // Calculates the area of a circle using the squaredNumber as radius with a function
    let areaFromNumber = areaOfCircle(squaredNumber);
    console.log(squaredNumber + ' as a radius has a ' + areaFromNumber + ' area');

    // Calculates what percentage that area is of the squared number with a function 
    let areaPercentOfSquared = percentOf(areaFromNumber, squaredNumber);
    console.log(number + ' led to ' + areaFromNumber + ' being ' + areaPercentOfSquared + '% of ' + squaredNumber)
}

// Series of function invocations with string prints
calculator(1);
console.log();

calculator(4);
console.log();
calculator(7);
console.log();
calculator(3.829);
console.log();
calculator(-4);
console.log();
calculator(0); // Special case, returns NaN on percentage value since it becomes 0/0