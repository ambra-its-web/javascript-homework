#### Author
- **Name:** Ambra

## Exercise: Geometry Library

### Requirements
Write a function that will take one argument (a number) and perform the following operations, using the functions you wrote earlier:
- Take half of the number and store the result
- Square the result of #1 and store that result
- Calculate the area of a circle with the result of #2 as the radius
- Calculate what percentage that area is of the squared result (#3)

### Approach to Solution
I defined two functions: `calcCircumference` and `calcArea`. 
- The `calcCircumference` function takes the radius as input, calculates the circumference using the formula `2 * Math.PI * raggio`(radius), and prints the result to the console.
- The `calcArea` function also takes the radius as input, calculates the area using the formula `Math.PI * Math.pow(raggio, 2)`, and prints the result to the console.

`Math.pow` is a built-in JS function that returns the first argument to the power of the second argument.
`toFixed(2)` rounds the results to two decimal places.

### Files
- `main.js`: Contains the implementation of the `calcCircumference` and `calcArea` functions.
- `index.html`: HTML file with a reference to the `main.js` file.

To view the results, open `index.html` in a web browser and check the console.