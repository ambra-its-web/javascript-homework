## Author
- **Name:** Ambra Duca
- **Contact:** [fabrizio.duca@edu.itspiemonte.it](mailto:fabrizio.duca@edu.itspiemonte.it)

## Exercise: Not Bad

### Requirements
Write regular expressions to validate the following inputs
#### 1.Email Address
Expected pattern: [any characters]@[any characters].[2-4 letters]
#### 2.Phone Number
Expected pattern: [optional + or country code] [digits, possibly separated by dashes or spaces]
#### 3.Password
Expected pattern: [at least 8 characters, including at least one uppercase letter, one lowercase
letter, one digit, and one special character]
#### 4.URL
Expected pattern: [protocol]://[domain].[top-level domain]/[optional path]?[optional query
string]#[optional fragment]
Note
Invent multiple test cases to thoroughly test your regular expressions

### Approach to Solution
I:
- Implemened a series of regular expressions that follow the expected patterns from the assignment.
- Declared multiple arrays, each containing various test elements for:
    - emails, phone numbers, passwords, URLs
- Commented next to each case where the pattern isn't found an explanation of where the regEx failed to find the pattern
- Implemented a series of validation functions that use the .test method on the element of the array passed as parameter
- Added console.logs that print the test cases out through the use of various for loops that print each element of each array and its boolean value, which is true if the pattern was found in the string, and false if it wasn't.

### Files
- `main.js`: Contains all of the exercise logic.
- `index.html`: HTML file with a reference to the `main.js` file and instructions to check the console for results.

To test the functions further, call them with different input strings and observe the returned results in the console.