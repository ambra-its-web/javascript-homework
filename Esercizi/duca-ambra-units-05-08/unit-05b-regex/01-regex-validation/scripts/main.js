/**
 * @file: main.js
 * @author: Ambra
 * - Implements a series of regular expressions that follow the expected patterns from the assignment.
 * - Declares multiple arrays, each containing various test elements for:
 *   - emails, phone numbers, passwords, URLs
 * - Each case where the pattern isn't found has an explanation of where the regEx failed to find the pattern
 * - Implements a series of validation functions that use the .test method on the element of the array passed as parameter
 * - Prints the test cases out through the use of various for loops that print each element of each array and its boolean value,
 *   which is true if the pattern was found in the string, and false if it wasn't.
 */

const emailRegex = /^[^\s@#]+@[^\s@#]+\.[a-zA-Z]{2,4}$/; // pattern: [any characters]@[any characters].[2-4 letters]
const phoneRegex = /^\+?[\d\s-]+$/m; // [optional + or country code] [digits, possibly separated by dashes or spaces]
const passwordRegex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[\W_]).{8,}$/; // [at least 8 characters, at least one uppercase letter, one lowercase, one digit, one special character]
const urlRegex = /^(https?:\/\/)?[\w.-]+\.[a-zA-Z]{2,}(\/.*)?(\?.*)?(#.*)?$/; // [protocol]://[domain].[top-level domain]/[optional path]?[optional query string]#[optional fragment]

// Test Cases
const emails = [
    "test@example.it", // True
    "ambraDuca@realmail.com", // True
    "ambra@sub.domain.com", // True, Any character allowed in the username and domain
    "very#real,email", // False No "@" found
    "fake@short.c", // False, Top level domain is too short
    "fake@email.toolong", // False, Top level domain is too long
    "fake@angry.com!" // False, invalid character in the top level domain
];

const phones = [
    "+123 456 7890", // True, Prefixes accepted
    "123-456-7890", // True, Dashes accepted
    "123 456-7890", // True, Spaces accepted, even mixed with dashes
    "(123) 456-7890", // False, Parentheses not allowed
    "1234567890", // True, No spaces is allowed
    "123" // True, No number limit on purpose (i.e emergency numbers)
];

const passwords = [
    "Password1!", // True
    "passW0rd=", // True
    "p@ssw0rD", // True
    "password", // False, 1 Special character required, 1 uppercase required
    "PASSWORD123!", // False, 1 Lower case required
    "Pa1!" // False, Too short
];

const urls = [
    "http://example.com", // True, The s in the protocol is optional
    "https://www.example.com/path?query=value#fragment", // True, Optional path + optional query string + # + optional fragment accounted for
    "http://sub.domain.co.it", // True, Same as the previous case, just some optionals are missing
    "ftp://invalid.protocol.com", // False, Protocol is not https or http
    "http://domain", // False, Top level domain missing
    "https://domain.c" // False, Top level domain too short
];

/* Validation Functions, they all take one argument, 
/* the array element that matches the for loop index value,
/**
 * Validation functions
 * They all take one argument, the array element that matches
 * the current for loop index value, and return the boolean value
 * obtained from the test method used on the emailRegex with
 * the current element as argument
 */
function checkEmail(email) {
    return emailRegex.test(email); // .test method to get a boolean value, true if the pattern exists, else false
}

function checkPhone(phone) {
    return phoneRegex.test(phone);
}

function checkPassword(password) {
    return passwordRegex.test(password);
}

function checkURL(url) {
    return urlRegex.test(url);
}

// Test Cases, printed through for loops iterating through the different arrays
// to then pring out the correspondent boolean value next to each element
console.log("Email Validation:");
for (let i = 0; i < emails.length; i++) {
    console.log(emails[i] + ": " + checkEmail(emails[i]));
}

console.log("\nPhone Validation:");
for (let i = 0; i < phones.length; i++) {
    console.log(phones[i] + ": " + checkPhone(phones[i]));
}

console.log("\nPassword Validation:");
for (let i = 0; i < passwords.length; i++) {
    console.log(passwords[i] + ": " + checkPassword(passwords[i]));
}

console.log("\nURL Validation:");
for (let i = 0; i < urls.length; i++) {
    console.log(urls[i] + ": " + checkURL(urls[i]));
}