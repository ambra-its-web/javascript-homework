/**
 * @file: main.js
 * @author: Ambra
 * Implements functions to get the weekday in short format for different languages.
 * It contains 3 functions:
 * 
 * The getWeekDayEN function (short format return value)
 * - Takes a Date object as input.
 * - Retrieves the weekday index from the Date object (0 for Sunday, 1 for Monday, ..., 6 for Saturday).
 * - Uses an array of English weekday abbreviations to return the corresponding abbreviation.
 * 
 * The getWeekDayIT function (short format return value)
 * - Similar to getWeekDayEN, but uses Italian weekday abbreviations
 * 
 * The getWeekDay function:
 * - Accepts two parameters: a Date object and a language code ('en' for English, 'it' for Italian).
 * - Checks the language parameter and calls either getWeekDayEN or getWeekDayIT
 * - Returns the weekday abbreviation accordingly with the language code, or an error message if it's invalid.
 * 
 * Three logs to check the functionality of getWeekDay, first one for getWeekDayEN, second one for getWeekDayIT and the third one for the invalid language code case 
 */

/**
* Function to get the weekday in English (short format).
* - Declares an array constant weekdaysEN containing the 7 days of the week in english short form
* - Returns the english short form weekday element of the array based on getDay's number returned
* @param {Date} date - The date the weekday is determined by
* @returns {string} - The weekday in short format (i.e 'MO', 'TU', etc.).
*/
function getWeekDayEN(date) {
  // Declaring the array of short form weekdays in English as a constant
  const weekdaysEN = ['SU', 'MO', 'TU', 'WE', 'TH', 'FR', 'SA'];
  /* Returns an array element of the english short form weekdays array whose index is a number ranging from 0, Sunday, to 6, Saturday based on the specified date (not specified means current date) through the .getDay method */
  return weekdaysEN[date.getDay()];
}

/**
* Function to get the weekday in italian (short format).
* - Declares an array constant weekdaysIT containing the 7 days of the week in Italian short form
* - Returns the italian short form weekday element of the array based on getDay's number returned
* @param {Date} date - The date the weekday is determined by
* @returns {string} - The weekday in short format (i.e 'LU', 'MA', etc.)
*/
function getWeekDayIT(date) {
  // Declaring the array of short form weekdays in Italian as a constant
  const weekdaysIT = ['DO', 'LU', 'MA', 'ME', 'GI', 'VE', 'SA'];
  /* Returns an element of the italian short form weekdays array whose index is a number ranging from 0, Sunday, to 6, Saturday based on the specified date (not specified means current date) through the .getDay method */
  return weekdaysIT[date.getDay()];
}

/**
* Function to get the weekday in short format for the language code given
* @param {Date} date - The date for which the weekday needs to be determined
* @param {string} lang - The language code ('en' for English, 'it' for Italian)
* @returns {string} - The weekday in short format based on the language code
*/
function getWeekDay(date, lang) {
  if (lang == 'en') {
    return getWeekDayEN(date);
  } else if (lang == 'it') {
    return getWeekDayIT(date);
  } else {
    return 'Invalid language code. "en" for English or "it" for Italian only.';
  }
}

// Example logs
const date = new Date(); // No argument, current date
console.log(getWeekDay(date, 'en')); // Output: current day of the week shortened in English
console.log(getWeekDay(date, 'it')); // Output: current day of the week shortened in Italian
console.log(getWeekDay(date, 'sp')); // Output: 'Invalid language code. "en" for English or "it" for Italian only.'