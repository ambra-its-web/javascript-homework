## Author
- **Name:** Ambra
- **Contact:** [fabrizio.duca@edu.itspiemonte.it](mailto:fabrizio.duca@edu.itspiemonte.it)

## Exercise: Scheduling Functions in JavaScript

### Requirements
- Write a function “useful” that does something useful in Javascript
- Schedule it to run after 10 seconds
- Write another function that cancels the scheduling of the first function
- Use the second function to cancel the first one after 5 seconds and output
‘function cancelled’ to the console

### Approach to Solution
Implements two functions in javascript:
- **useful**, is scheduled to be inovked after 10 seconds and would log a message to the console saying that the function does something useful.
- **cancelUseful**: is scheduled to be called after 5 seconds using another `setTimeout` and cancels the scheduling of the 'useful' function by clearing the timeout set for it and logs 'function cancelled' to the console

### Files
- `main.js`: Contains the implementation of the two functions.
- `index.html`: HTML file with a reference to the `main.js` file and instructions to check the console for results.

To view the exercise solution, open the console and run the script in `main.js`.