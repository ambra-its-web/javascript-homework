/**
 * @file: main.js
 * @author: Ambra
 * Implements two functions in JavaScript:
 * - useful: Performs a useful task.
 * - cancelUseful: Cancels the scheduling of the 'useful' function and outputs 'function cancelled' to the console.
 * Schedules the 'useful' function to run after 10 seconds and cancels it after 5 seconds.
 */

/**
* Function that is useful
* @param {void}
* @returns {void}
*/
function useful() {
  console.log("This function does something useful.");
}

/**
* Function that cancels the scheduling of the 'useful' function and logs 'function cancelled' to the console.
* @param {void}
* @returns {void}
*/
function cancelUseful() {
  clearTimeout(timeoutId);
  console.log("Function cancelled.");
}

// Schedule the 'useful' function to run after 10 seconds
const timeoutId = setTimeout(useful, 10000);

// Cancel the scheduling of the useful function after 5 seconds
setTimeout(cancelUseful, 5000);