/**
 * @file: main.js
 * @author: Ambra
 * Introduces logic printed on the console accordingly with the specifications of the 2nd exercise of unit 3
 * Defines a function `mySetInterval` that takes two arguments: 
 *   `call` and `delay` 
 * `call` is the function to be passed each time the interval elapses
 * `delay` is the number of milliseconds between each execution of the `call`
 * The function executes the `call` repeatedly with a fixed time delay between each call, similar to `setInterval()`
 * The execution stops automatically after 15 intervals
 * Finally, it tests the function by printing a message every second for 15 seconds
 */

/**
 * Function that mimicks setInterval using setTimeout
 * Replicates the behavior of setInterval by invoking a call function every second
 * with a fixed time delay between each call. Stops after 15 intervals
 *
 * @param {function} call - The function to be executed each time the interval passes
 * @param {number} delay - The number of milliseconds between each execution of the call
 * @returns {void}
 */
function mySetInterval(call, delay) {
  // Initializing an interval counter variable to count the amount of intervals that passed
  let intervalCount = 0;

  function runInterval() {
    // Increase the interval counter at the start of every runInterval invocation
    intervalCount++;

    // Check if the interval count is less than or equal to 15
    if (intervalCount <= 15) {
      // If yes, invoke call, the function provided, and schedule the next interval with setTimeout
      call();
      setTimeout(runInterval, delay); // After its first invocation within mySetInterval, runInterval is instead invoked by setTimeout, with the delay provided
    }
  }

  // First invocation of runInterval within mySetInterval
  runInterval();
}

/** 
 * Test function to be passed to mySetInterval
 * Just prints a string
 * @param {null}
 * @returns {void}
 */
function testFunc() {
  console.log("An interval has passed");
}

// Testing mySetInterval by invoking testFunc every second for 15 seconds
mySetInterval(testFunc, 1000);