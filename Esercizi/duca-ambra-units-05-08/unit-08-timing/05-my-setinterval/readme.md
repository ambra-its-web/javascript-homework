## Author
- **Name:** Ambra
- **Contact:** [fabrizio.duca@edu.itspiemonte.it](mailto:fabrizio.duca@edu.itspiemonte.it)

## Exercise: My setInterval

### Requirements
- Pretend that setInterval() doesn't exist
- Re-create it using setTimeout naming your function mySetInterval
- Test your new function
- Modify your function so that it automatically stops after 15 intervals

### Approach to Solution
I defined a function `mySetInterval` to mimick `setInterval()` by using `setTimeout()`:
- It initializes an interval counter variable to keep track of the number of intervals that have passed.
- Inside `mySetInterval`, I defined the function `runInterval` which invokes the `call` function, the one provided as an argument, and schedules the next interval.
    - The `runInterval` function is initially invoked within `mySetInterval`, as its last part, to start the interval loop.
    - The interval counter is increased with each invocation of `runInterval`.
    - If the interval count is less than or equal to 15, the `call` function is invoked, and the next interval is scheduled using `setTimeout`.
    - After 15 intervals, `runInterval` stops since the interval counter no longer fulfills the if statement's requirements, and the function exits.
Finally, `mySetInterval` is tested by invoking a test function, `testFunc` every second for 15 seconds.

### Files
- `main.js`: Contains the implementation of the two functions.
- `index.html`: HTML file with a reference to the `main.js` file and instructions to check the console for results.

To view the exercise solution, open the console and run the script in `main.js`.