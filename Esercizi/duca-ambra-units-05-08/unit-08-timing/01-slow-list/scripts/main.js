/**
 * @file: main.js
 * @author: Ambra
 * 
 */

// Array holding a list of 30 items
const itemList = [
    "Wagon", "Eye liner", "Cork", "Clothes", "Spoon",
    "La Terra dei Figli", "Seat belt", "House", "Bottle", "Toothpaste",
    "Wallet", "Thermometer", "Bread", "Vase", "Lemon",
    "Purse", "Drawer", "Ice cube", "Candle", "Carrots",
    "Nail", "Key", "Sofa", "Sand paper", "Street lights",
    "Pineapple", "Headphones", "Bass guitar", "Clay", "Bell"
  ];
  
/**
* Function to print one item of the list every second using setInterval
* - Declares an index variable and initializes it to 0
* - Declares the Interval ID constant and sets it equal to the setInterval function
*   - the setInterval function invokes an anonymous function (the code to be executed) every second 
*     (1000 milliseconds, number value found at the end of the function as the parenthesis closes)
* - The anonymous function:
*   - Checks if the index is lower than the length of the itemList array
*     - If it is, it logs said array element and increases the index
*     - If it isn't, clears the interval by calling the clearInterval function with argument the intervalID set
* @param {void}
* @returns {void}
*/
function printInterval() {
  let i = 0; // Declaring the index and setting it to 0
  /* Declaring the constant intervalID and setting it equal to the setInterval function, which executes the
  code in the anonymous function at an interval of 1 second */
  const intervalId = setInterval(function() {
    // The anonymous function checks if the index is lower than the length of the itemList array
    if (i < itemList.length) {
      console.log(itemList[i]); // If it is, print the current item
      i++; // and increase the index
    } else {
      clearInterval(intervalId); // If it isn't, clear the interval by calling the clearInterval function with argument the intervalID set
    }
  }, 1000); // The interval amount is put last due to the use of an anonymous function, defined right before it
}

/**
* Function to print one item of the list every second using setTimeout
* - Declares an index variable and initializes it to 0
* - Declares a function called printItem that:
*   - Checks if the index is lower than the length of the itemList array
*     - If it is, it logs said array element, increases the index and calls setTimeout with arguments printItem and 1 second
* - The function printTimeout then invokes printItem again until every item is printed
* @param {void}
* @returns {void}
*/
function printTimeout() {
    let i = 0;
    function printItem() {
      if (i < itemList.length) {
        console.log(itemList[i]);
        i++;
        setTimeout(printItem, 1000);
      }
    }
    printItem();
}
  
// Test invocations of the two main functions which will print the same elements simultaneously on the console
printInterval();
printTimeout();