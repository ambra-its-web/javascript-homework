## Author
- **Name:** Ambra Duca
- **Contact:** [fabrizio.duca@edu.itspiemonte.it](mailto:fabrizio.duca@edu.itspiemonte.it)

## Exercise: Print Items from an Array

### Requirements
- Create an array that holds a list of 30 items.
- Print one item of the list every second until the list is completely printed.
  - Use `setInterval` to achieve this goal.
  - Do the same thing using `setTimeout`.

### Approach to Solution

Implemented two functions in JavaScript:
- `printInterval`: Prints one item of the list every second using `setInterval`.
- `printTimeout`: Prints one item of the list every second using `setTimeout`.

Both functions utilize an array named `itemList` containing 30 items. Each function iterates through the array and prints each item at a one-second interval.

### Function Details

#### `printInterval`
- Declares an index variable and initializes it to 0.
- Uses `setInterval` to invoke an anonymous function every second.
- The anonymous function checks if the index is less than the length of the `itemList` array.
  - If true, it logs the array element and increases the index.
  - If false, it clears the interval.

#### `printTimeout`
- Declares an index variable and initializes it to 0.
- Defines an inner function called `printItem`.
- `printItem` checks if the index is less than the length of the `itemList` array.
  - If true, it logs the array element, increases the index, and sets a new timeout to call itself after one second.
- `printItem` is iinvoked to start the timeout loop.

### Files
- `main.js`: Contains the implementation of the slow-list functions.
- `index.html`: HTML file with a reference to the `main.js` file and instructions to check the console for results.

To view the exercise solution, open the console and run the script in `main.js`.