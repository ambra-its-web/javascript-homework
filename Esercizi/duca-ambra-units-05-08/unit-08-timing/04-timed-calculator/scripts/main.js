/**
* @file: main.js
* @author: Ambra
* Introduces logic printed on the console accordingly with the specifcations of the 2nd exercise of unit 3 with the addition of
* Defines a function `tellFortune` that takes four arguments: 
    `children`, `partner`, `geoLoc`, and `jobTitle`. 
* Within the function, the string `fortune` is declared, representing the fortune told based on the provided arguments. 
* It includes the job title (`jobTitle`), geographic location (`geoLoc`), partner's name (`partner`), and the number of children (`children`). 
* 
* |Timed Calculator specifics|:
* The function then prints the fortune string to the console after a delay of 3 seconds.
* It's called three times with different values to test its functionality.
*/

// function tellFortune
// Function that has 4 parameters and prints a string using them
/**
 *  Returns the parameter multiplied by the two variables, global and local
 * @param  {number, string, string, string} children - amount of children,  partner - who they'll be married to, geoLoc - where they're going to work, jobTitle - what job they'll have
 * @returns {undefined} - the result of the function is the printing it does within itself
 */
function tellFortune(children, partner, geoLoc, jobTitle) {
    // Delayed print of fortune
    setTimeout(function() {
        // variable fortune, a string with multiple variables in it
        let fortune = "You will be a " + jobTitle + " in " + geoLoc + ", and married to " + partner + " with " + children + " kids.";
        
        // print of the fortune told
        console.log(fortune);
    }, 3000);
}

// invoking the function tellFortune with different argument values
tellFortune(0, "nobody", "Copenhagen", "programmer");
tellFortune(3, "somebody", "Milano", "comic artist");
tellFortune(1, "Martina", "Torino", "harpsichord teacher");