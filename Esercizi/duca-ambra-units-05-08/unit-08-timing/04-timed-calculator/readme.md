#### Author Dageils
- **Name:** Ambra
- **Contact:** fabrizio.duca@edu.itspiemonte.it

## Exercise 4. Timed calculator

### Requirements
- We will modify ‘Calculator’ exercise from the lesson about functions
- Rewrite the last function that performs all 4 operations so that there is a
delay of 3 seconds between one operation and the next

### Approach to Solution

##### Fortune calculator (Old)
For this exercise, I defined a function `tellFortune` that takes four arguments: `children`, `partner`, `geoLoc`, and `jobTitle`. Within the function, I declared the string `fortune` which represents the fortune based on the provided arguments. It includes the job title (`jobTitle`), geographic location (`geoLoc`), partner's name (`partner`), and the number of children (`children`). Finally, I printed the fortune string to the console.

### Timed calculator (New)
- The function then prints the fortune string to the console after a delay of 3 seconds.
- It's called three times with different values to test its functionality.

### Files
- `main.js`: Contains the implementation of the modified function from.
- `index.html`: HTML file with a reference to the `main.js` file and instructions to check the console for results.