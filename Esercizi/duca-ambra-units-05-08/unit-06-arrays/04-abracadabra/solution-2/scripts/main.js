/**
 * @file: main.js
 * @author: Ambra
 * solution-2
 * This solution converts the string into an array, modifies it, then turns it into a string again
 *  - Declares a variable str = "Abracadabra"
 *  - Logs str's type to show it's a string as of now
 *  - Declares a variable strArray, obtained through the use of the method .split 
 *    which turns each character of a string into an array element 
 *    divided by whatever's in the parentheses, in this case an empty space ""
 *  - Logs strArray's type, showing it's an array, which is logged with commas
 *    inbetween each element and with nothing added inbetween due to the empty space
 *  - Gets access to the 4th character of strArray and changes it to the character "X"
 *  - Declares the variable modStr which is obtained by using the .join method on strArray with empty spaces
 *  - It then logs modStr's type to the console to show it's a string again
 *  - Finally, it logs modStr to show the result, the replacement of the 4th character with an X
 */

// Original string
let str = "Abracadabra";
console.log(str + " is a " + typeof str) // string

// Converting the string to an array to manip single characters
let strArray = str.split(""); // Using the .split method, with empty spaces dividing the elements of the array
console.log(strArray + " is instead an " + typeof strArray) // object, since arrays are objects in javascript.
															// That's why it's shown with commas between each character

// Replacing the 4th character with "X" in the array (0-1-2-3, it's the 4th)
strArray[3] = "X";

// Turn the array back into a string with the .join method
let modStr = strArray.join("");
console.log("The modified string is once again a " + typeof modStr)

// Log of the modified string
console.log(modStr);