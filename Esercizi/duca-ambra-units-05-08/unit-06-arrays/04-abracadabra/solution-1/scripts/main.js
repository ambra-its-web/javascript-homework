/**
 * @file: main.js
 * @author: Ambra
 * solution-1
 * It uses string manipulation through the method .substring
 *  - Declares a variable str = "Abracadabra"
 *  - Declares a variable modStr that's made by:
 *  - The first 3 charaacters gotten through .substring(0, 3), "Abr"
 *  - "X" string added as a character right after
 *  - The rest of the original string as substring(4)
 * It then logs the modified string to the console
 */


// Original string
let str = "Abracadabra";

// Replace the 4th character with "X" using substring
let modStr = str.substring(0, 3) // Taking the first 3 characters, "Abr"
+ "X" // Adding the "X" string as a character
+ str.substring(4); // Adding the rest of the string from the 5th character onward,
                    //  bypassing the 4th one (0-1-2-3-4, it's the 5th)

// Log of the modified string
console.log(modStr);