/**
 * @file: main.js
 * @author: Ambra
 * solution-3
 * This solution adds each character into a new string, changing only the 4th
 *  - Declares a variable str = "Abracadabra"
 *  - Declares a variable modStr and initializes it as an empty string
 *  - Starts a for loop which:
 *    - Iterates through each character of str in a for loop
 *    - Checks if it's the 4th character
 *		- If it is, add "X"
 *		- If it isn't, add the original string's corresponding character based on the current loop index value
 *  - At the end, modStr is logged to show the result on the console
 */

// Original string
let str = "Abracadabra";

// Initializing an empty modified string
let modStr = "";

// for loop iterating through each character in the original string
for (let i = 0; i < str.length; i++) {
	if (i == 3) {     // Check if it's the 4th character
		modStr += "X"; // If it is, add "X"
	} else {
		modStr += str[i]; // If it isn't, add the original string's corresponding character
	}
}

// Log of the modified string
console.log(modStr);