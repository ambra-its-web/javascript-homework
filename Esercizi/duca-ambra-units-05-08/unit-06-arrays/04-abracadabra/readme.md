## Author
- **Name:** Ambra Duca
- **Contact:** [fabrizio.duca@edu.itspiemonte.it](mailto:fabrizio.duca@edu.itspiemonte.it)

## Exercise: Abracadabra

### Requirements
- Code 3 different solutions to change the 4th letter in the following string "Abracadabra" into an "X"
- Each solution should be in a separate folder.
    - Name them solution-1, solution-2, etc.
- Also include a doc file in which you explain what 3 ways you used

### Approach to Solution

#### Solution 1 - Modifying a string by using .substring
I used string manipulation through the method `.substring`. I:
- Declared a variable `str` = "Abracadabra"
- Declared a variable ``modStr`` that's composed of:
    1. The first 3 characters gotten through `.substring(0, 3)`, "Abr"
    2. The `"X"` string added as a character right after
    3. The rest of the original string as `substring(4)`
- Finally, I logged the modified string to the console to show the result, the replacement of the 4th character with "X"".

Note that the original string was untouched and I needed to make new variables to store the new string values, since in javascript strings are immutable and we can't change them by, for example, doing `str = "Abracadabra"; str = "AbrXcadabra"`

#### Solution 2 - Converting a string into an array to modify it 
I converted a string into an array, modified it, then converted it back into a string again. 
I:
- Declared a variable `str = "Abracadabra"`
- Logged str's type to show its type, string
- Declared a variable `strArray`, obtained through the use of the method .split which turns each character of a string into an array element divided by whatever's in the parentheses, in this case an empty space (`""`)
- Logged `strArray`'s type, showing it's an array, which is logged with commas inbetween each element and with nothing added inbetween due to the empty space
- Got access to the 4th character of `strArray` and changes it to the character "X"
- Declared the variable `modStr` which is obtained by using the `.join` method on `strArray` with empty spaces
- Logged `modStr`'s type to the console to show it's a string again
- Finally, I logged `modStr` to show the result, the replacement of the 4th character with "X".

#### Solution 3
I added each character into a new string, changing only the 4th through a for loop.
I:
- Declared a variable `str = "Abracadabra"`
- Declared a variable `modStr` and initialized it as an empty string
- Used a for loop which:
  - Iterates through each character of `str` in a for loop
  - Checks if it's the 4th character
        - If it is, add "X"
        - If it isn't, add the original string's corresponding character based on the current loop index value
- At the end, I logged `modStr` to show the result on the console, the replacement of the 4th character with "X".

### Files
Each solution-X folder contains:
- an `index.html` with the instruction
- its corresponding solution in a `main.js` file found within a scripts folder