function sumValues(firstValue, secondValue) {
    if(typeof firstValue === "number" && typeof secondValue === "number") {

        const sum = firstValue + secondValue;

        console.log("ciao");
        console.log("ciao ciao");
        
    return sum;
} // one of the values isn't "number"
//NOTE OPTION 1   console.log("One of the two values is not of type 'number'")

// OPTION 2 - using NaN
// return Number.NaN; 

// Option 3 - using null
// return null;
}    

const result = sumValues(2, "ciao") + sumValues(3,5);

console.log("adding 2 ciao ciao 4 gives: ${result}");