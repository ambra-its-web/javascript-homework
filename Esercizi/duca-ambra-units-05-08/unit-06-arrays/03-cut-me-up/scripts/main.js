/**
 * @file: main.js
 * @author: Ambra
 * @description: Demonstrates the usage of the array methods slice() and splice() by
 * providing examples showcasing their differences and results
*/

// slice() example
const array1 = [1, 2, 3, 4, 5];
const slicedArray = array1.slice(1, 4); // Copies elements from index 1 to 3 (4 is not included)
console.log(slicedArray); // Output: [2, 3, 4]
console.log(array1); // Output: [1, 2, 3, 4, 5] (original array remains unchanged)

// splice() example
const array2 = [1, 2, 3, 4, 5];
const splicedArray = array2.splice(1, 2); // Removes 2 elements starting from index 1
console.log(splicedArray); // Output: [2, 3] (removed elements)
console.log(array2); // Output: [1, 4, 5] (original array modified, elements at index 1 and 2 removed)

// splice() with adding elements example
const array3 = [1, 2, 3, 4, 5];
const splicedWithAddition = array3.splice(2, 1, 'a', 'b', 'c'); // Removes 1 element at index 2 and adds 'a', 'b', 'c'
console.log(splicedWithAddition); // Output: [3] (removed element)
console.log(array3); // Output: [1, 2, 'a', 'b', 'c', 4, 5] (original array modified)