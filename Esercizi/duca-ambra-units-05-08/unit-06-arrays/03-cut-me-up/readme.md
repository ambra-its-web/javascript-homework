## Author
- **Name:** Ambra Duca
- **Contact:** [fabrizio.duca@edu.itspiemonte.it](mailto:fabrizio.duca@edu.itspiemonte.it)

## Exercise: Cut me up

### Requirements
In the exercise folder create a .txt or .doc or .md file in which you explain
the difference between the following array methods:
- `.slice()`, `.splice()`
- Explain the differences in terms of parameters and behavior
- Provide code examples to prove your point

### Files
- `main.js`: Contains the implementation of the code examples for the exercise.
- `index.html`: HTML file with a reference to the `main.js` file and instructions to check the console for the examples.
- `answer.md`: A markdown file containing the answer in written form.