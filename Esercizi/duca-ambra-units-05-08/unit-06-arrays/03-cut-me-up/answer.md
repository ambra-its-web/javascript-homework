# `.slice()` vs `.splice()`:

### `.slice()`
It creates a new array by copying a portion of an existing array into a new one, without changing the original array.
### `.splice()`
It changes an existing array by removing or replacing its elements and/or adding new ones.

## Parameters and behaviour:

### `.slice()`:
#### Parameters:
It takes two optional parameters: start index and the end one. If no parameters are provided, it copies the entire array.
#### Behaviour:
It copies elements from the start index up to, but not including, the end index. If the end index is not provided, it copies until the end of the array.

### `.splice()`:
#### Parameters:
It takes three required parameters: start index, number of elements to remove, and optional new elements to add.
#### Behaviour:
It removes elements from the array starting at the specified index. It can also insert new elements at that position.