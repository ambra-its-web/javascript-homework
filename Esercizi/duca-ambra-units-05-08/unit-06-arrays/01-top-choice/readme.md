## Author
- **Name:** Ambra Duca
- **Contact:** [fabrizio.duca@edu.itspiemonte.it](mailto:fabrizio.duca@edu.itspiemonte.it)

## Exercise: Top choice

### Requirements
- Create an array to hold your top choices (colors, pets, books, whatever).
- For each choice, log to the screen a string like: "My #1 choice is blue."

### Approach to Solution
- I defined an array called `choices`, initializing it with four different string elements.
- I printed each element within a different string in the console by using its array position.
- Showcased how 'undefined' is printed when picking a non-existent element position such as 4 and -1.

### Files
- `main.js`: Contains the implementation of the requested array.
- `index.html`: HTML file with a reference to the `main.js` file and instructions to check the console for results.