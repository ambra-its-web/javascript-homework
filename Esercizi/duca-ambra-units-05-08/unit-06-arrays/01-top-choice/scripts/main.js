/**
 * @file: main.js
 * @author: Ambra
 * This file declares and initializes an array containing 4 elements, then prints each of them within a different string on the console
 */

let choices = ['purple', 'metal', 'uzumaki', 'tarantulas'];
console.log("My #1 choice is", choices[0],); // printing each element loaded into the array, the first element is in position 0
console.log("My #2 choice is", choices[1],);
console.log("My #3 choice is", choices[2],);
console.log("My #4 choice is", choices[3],);
console.log("My #5 choice is", choices[4],); // the 5th element is undefined and it shows that when printed
console.log("My #-1 choice is", choices[-1],); // the -1st element is undefined as well