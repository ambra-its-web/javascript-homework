## Author
- **Name:** Ambra Duca
- **Contact:** [fabrizio.duca@edu.itspiemonte.it](mailto:fabrizio.duca@edu.itspiemonte.it)

## Exercise: Verbing

### Requirements
- Create a function called `verbing`.
- It should take a single argument, a string. If its length is at least 3, it should add 'ing' to its end, unless it already ends in 'ing', in which case it should add 'ly' instead.
- If the string length is less than 3, it should leave it unchanged.
For example,
`verbing('swim')`: 'swimming'
`verbing('swimming')`: 'swimmingly'
`verbing('go')`: 'go'

### Approach to Solution
I implemented a function called `verbing` that:
- Checks if the input string `str` is at least 3 characters long by using the `length` property.
- Checks if the string ends with 'ing' by using the `endsWith` method.
- If it ends with 'ing', it adds 'ly' to the end of the string.
- If it doesn't end with 'ing', it adds 'ing' to the end of the string.
- If the string length is less than 3, it leaves the string unchanged.
- Returns the modified or unchanged string accordingly.

### Files
- `main.js`: Contains the implementation of the `verbing` function.
- `index.html`: HTML file with a reference to the `main.js` file and instructions to check the console for results.

To test the `verbing` function further, call it with different input strings and observe the returned results in the console.