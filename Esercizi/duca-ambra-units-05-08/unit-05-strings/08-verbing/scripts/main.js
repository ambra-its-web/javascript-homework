/**
 * @file: main.js
 * @author: Ambra
 * Implements a function called `verbing` that:
 * - Checks if the input string `str` is at least 3 characters long.
 * - Checks if the string ends with 'ing'.
 * - If it ends with 'ing', it adds 'ly' to the end of the string.
 * - If it doesn't end with 'ing', it adds 'ing' to the end of the string.
 * - If the string length is less than 3, it leaves the string unchanged.
 * - Returns the modified or unchanged string accordingly.
 *
 * Outside of the function, there's some test prints to check if it works correctly.
 */

/**
 * Function verbing
 * Returns a modified version of the input string, if it ends with ing adds ly, else adds ing, if its length was less than 3 returns it unchanged
 * @param {string} str - the string to modify
 * @returns {string} - a modified version of the input string
 */
function verbing(str) {
    if (str.length >= 3) { // check if the string length is at least 3
        if (str.endsWith('ing')) { // check if the string ends with 'ing'
            return str + 'ly'; // add 'ly' to the end of the string
        } else {
            return str + 'ing'; // add 'ing' to the end of the string
        }
    } else {
        return str; // If the string length is less than 3, leave it unchanged
    }
}

// Testing the verbing function
console.log(verbing('swim')); // can't recognize specific needs like doubling end consonants
console.log(verbing('swimming'));
console.log(verbing('go'));
console.log(verbing('a'));
console.log(verbing(''));
console.log(verbing('in the beginning')) // given the lack of grammatical context for the function to work with, it'll add the characters anyway
console.log(verbing("I swear I'm not sleeping"))
console.log(verbing("hello, my name isn't Archibald"))
console.log("Type 'console.log(verbing('Your String Here'))' to test it out.");