/**
 * @file: main.js
 * @author: Ambra
 * Implements a function called `notBad` that:
 * - Takes a single argument, a string.
 * - Finds the first appearance of the substring 'not' and 'bad'.
 * - If the 'bad' follows the 'not', it replaces the whole 'not'...'bad' substring with 'good' and returns the result.
 * - If it doesn't find 'not' and 'bad' in the right sequence (or at all), it just returns the original sentence.
 *
 * Outside of the function, there's some test prints to check if it works correctly.
 */

/**
 * Function notBad
 * Returns a modified version of the input string based on specific conditions
 * @param {string} str - The string to modify
 * @returns {string} - a modified version of the input string
 */
function notBad(str) {
    let indexNot = str.indexOf('not'); // Find the index of the first occurrence of 'not'
    let indexBad = str.indexOf('bad'); // Find the index of the first occurrence of 'bad'

    if (indexNot !== -1 && indexBad !== -1 && indexBad > indexNot) { // Check if both 'not' and 'bad' exist, and 'bad' comes after 'not'
        return str.slice(0, indexNot) + 'good' + str.slice(indexBad + 3); // Replace 'not'...'bad' substring with 'good'
    } else {
        return str; // Return the original sentence if conditions are not met
    }
}

// Testing the notBad function
console.log(notBad('this dinner is not that bad!'));
console.log(notBad('this movie is not so bad!')); // conditions met, 'not so bad' replaced with 'good'
console.log(notBad('this dinner is bad!')); // conditions unmet (indexBad found, indexNot not found), string unchanged
console.log(notBad('this is not good!')); // conditions unmet (indexNot found, indexBad not found), string unchanged
console.log(notBad("it's not bad not to stare"));
// Interesting edge cases
console.log(notBad("I'm not badly interested"));
console.log(notBad("I wish you not badwill")); //the string occurrences don't need to be standalone words
console.log(notBad('not bad not good'));
console.log(notBad('not good not bad')); // showcasing that the function can leave little to no trace of the previous value of the string
console.log(notBad("I'm not going to auntie's party and you can't convince me otherwise, understood? No? That's too bad"));
console.log(notBad("not far from the bustling city of Eldoria, nestled within the tranquil embrace of the Whispering Woods, there existed a small cottage. This humble abode belonged to an elderly sage named Alaric, known throughout the land for his wisdom and kindness. One crisp autumn evening, as the amber hues of sunset painted the sky, a weary traveler stumbled upon Alaric's doorstep. The traveler, a young maiden named Elara, sought refuge from the biting cold and the encroaching darkness of the forest. With a warm smile, Alaric welcomed Elara into his home, offering her a seat by the crackling fireplace. Over cups of steaming herbal tea, Elara shared her tale of adventure and peril, recounting her journey from distant lands in search of lost treasures and forgotten secrets. As the night deepened, Alaric regaled Elara with stories of ancient lore and mystical wonders, his words weaving a tapestry of magic and mystery. Enthralled by the sage's tales, Elara found herself drawn into a world of enchantment and wonder. But as the hours slipped away and the fire dwindled to embers, a shadow fell upon their conversation. With a heavy heart, Alaric revealed a grim prophecy that foretold of impending doom and a great evil that threatened to engulf the land. Determined to confront this darkness, Elara pledged to aid Alaric in his quest to unravel the secrets of the prophecy and vanquish the looming threat. Together, they embarked on a perilous journey across treacherous terrain and haunted forests, facing trials and tribulations at every turn. Yet, despite their bravery and perseverance, the shadow of despair lingered ever closer, casting doubt upon their quest. It seemed that fate itself conspired against them, testing their resolve and pushing them to the brink of despair. But just when all hope seemed lost, a glimmer of light pierced the darkness, illuminating a path forward. With renewed determination, Elara and Alaric pressed on, their hearts filled with courage and their spirits undaunted by adversity. And so, as the first rays of dawn painted the sky in hues of gold and crimson, Elara and Alaric stood upon the precipice of destiny, ready to face whatever trials awaited them. For they knew that their journey was far from over, and that the greatest challenges still lay ahead. But as they gazed out into the horizon, a sense of hope filled their hearts, for they knew that as long as they stood together, they would overcome any obstacle that dared to stand in their way. And so, with determination in their eyes and courage in their hearts, they set forth into the unknown, ready to write their own destiny and forge a future free from darkness and despair. Yet little did they know, lurking in the shadows, an ancient evil stirred, biding its time until the moment was ripe to strike. And as Elara and Alaric ventured forth, unaware of the danger that lay ahead, their fate became entwined with a darkness so malevolent, it would test their strength and resilience like never before. In the end, if the shadows prevailed, the outcome for the entirety of the realm would be decidedly bad"));
// AI generated long story string (3k+ characters) that turns into a single "good" because it begins with "not" and ends with "bad"
console.log("Type 'console.log(notBad('Your String Here'))' to test it out.");