## Author
- **Name:** Ambra Duca
- **Contact:** [fabrizio.duca@edu.itspiemonte.it](mailto:fabrizio.duca@edu.itspiemonte.it)

## Exercise: Not Bad

### Requirements
- Create a function called `notBad` that takes a single argument, a string.
- It should find the first appearance of the substring 'not' and 'bad'.
- If the 'bad' follows the 'not', then it should replace the whole 'not'...'bad' substring with 'good' and return the result.
- If it doesn't find 'not' and 'bad' in the right sequence (or at all), just return the original sentence.
For example,
`notBad('This dinner is not that bad!')`: 'This dinner is good!'
`notBad('This movie is not so bad!')`: 'This movie is good!'
`notBad('This dinner is bad!')`: 'This dinner is bad!'

### Approach to Solution
I implemented a function called `notBad` that:
- Finds the index of the first occurrence of 'not' and 'bad' in the input string using the `.indexOf` method.
- Checks if both 'not' and 'bad' exist through `!= -1` as the condition, and if 'bad' comes after 'not' by checking if its index value is greater than the other.
- If all 3 conditions are met, it replaces the 'not'...'bad' substring with 'good' using the `.slice` method.
- Returns the modified or original string accordingly.

### Files
- `main.js`: Contains the implementation of the `notBad` function.
- `index.html`: HTML file with a reference to the `main.js` file and instructions to check the console for results.

To test the `notBad` function further, call it with different input strings and observe the returned results in the console.