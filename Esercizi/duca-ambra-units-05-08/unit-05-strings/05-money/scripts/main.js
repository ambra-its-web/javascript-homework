/**
* @file: main.js
* @author: Ambra
* Introduces logic printed on the console accordingly with the specifications of the 5th exercise of unit 5.
* Implements a function called Money that:
- Checks if the amount is equal to one or minus one, if it is, it adds " dollar", turning `amount`, a number, into a string.
  Else, it adds " dollars".
- Checks if the amount is equal to one million, if it is, it adds a smiley, turning `amount`, a number, into a string
- Returns `amount`
* Outside of the function, there's some test prints to check if it works correctly.
*/

/**
 * Function Money
 * Returns a string by modifying a number variable it takes
 * @param {number} amount - The input amount.
 * @returns {string} amount - a modified version of the number with " dollar" added if it's equal to 1 or -1, " dollars" added in all other cases, with " :)" added if it's 1MIL
 */
function Money(amount) {
    if (amount == 1 || amount == -1){
        amount += " dollar";
    } else {
        amount += " dollars"
    };
    if (amount == "1000000 dollars"){
        amount += " :)";
    };
    return amount;
};
// Test the Money function
console.log(Money(1)); // edge case, singular dollar
console.log(Money(-1)); // edge case, negative singular dollar
console.log(Money(-1000000)); // "edge case", negative million
console.log(Money(1000000)); // "smiley case"
console.log("This last print had a smiley because the amount given was 1000000")
console.log("Type 'console.log(Money('any number'))' to test it out.")