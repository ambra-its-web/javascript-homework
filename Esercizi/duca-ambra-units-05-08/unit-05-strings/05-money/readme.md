## Author
- **Name:** Ambra Duca
- **Contact:** [fabrizio.duca@edu.itspiemonte.it](mailto:fabrizio.duca@edu.itspiemonte.it)

## Exercise: Palindrome

### Requirements
- Create a function called Money
- It should take a single argument, an amount, and return '\<amount> dollars'
- Add a smiley at the end if the amount is 1 million. Deal with edge cases
For example
Money(1): 1 dollar
Money(10): 10 dollars
Money(1000000): 1000000 dollars ;)

### Approach to Solution
I implemented a function called Money that:
- Checks if the amount is equal to one or minus one, if it is, it adds " dollar", turning `amount`, a number, into a string.
Else, it adds " dollars".
- Checks if the amount is equal to one million, if it is, it adds a smiley, turning `amount`, a number, into a string
- Returns `amount`
Outside of the function, there's some test prints to check if it works correctly.

### Files
- `main.js`: Contains the implementation of the `capitalizeFirst` function.
- `index.html`: HTML file with a reference to the `main.js` file and instructions to check the console for results.

To test the `capitalizeFirst` function further, call it with different input strings and observe the returned results in the console.