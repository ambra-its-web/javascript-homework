/**
* @file: main.js
* @author: Ambra
* Introduces logic printed on the console accordingly with the specifcations of the 1st exercise of unit 5.
* Defines a function `printReverse` that takes a single argument, `str`, representing the input string. 
- Within the function:
- Initialization of an empty string `reversedString`.
- Use of a loop to iterate through the characters of the input string from the end to the beginning.
- Appending each character to `reversedString`.
- Printing of the reversed string to the console.
*/

/**
 * Function printReverse
 * Prints a given string in reverse.
 * @param {string} str - The input string.
 */
function printReverse(str) {
    let reversedString = ""; // Initializing an empty string variable to store the reversed string in it
    for (let i = str.length - 1; i >= 0; i--) { // Iterates through the characters of the input string from the end to the beginning, so in reverse
        reversedString += str.charAt(i); // Append through += each character of the input string to the reversedString variable
    }
    console.log(reversedString); // Prints the reversed string within the function itself
}

// Invoking the function to test it in the terminal
printReverse("foobar"); // Output: raboof
console.log(''); // spacing print
printReverse("Ambra"); // Output: arbmA