## Author
- **Name:** Ambra Duca
- **Contact:** [fabrizio.duca@edu.itspiemonte.it](mailto:fabrizio.duca@edu.itspiemonte.it)

## Exercise: Print Reverse

### Requirements
- Write a JavaScript function named `printReverse` that:
  - Takes one argument, a string.
  - Prints the string in reverse order.
- Test the function with different input strings and ensure it produces the correct reversed output.

### Approach to Solution
I defined a function `printReverse` that takes a single argument, `str`, representing the input string. Within the function:
- Initialized an empty string `reversedString`.
- Used a loop to iterate through the characters of the input string from the end to the beginning.
- Appended each character to `reversedString`.
- Printed the reversed string to the console.

### Files
- `main.js`: Contains the implementation of the `printReverse` function.
- `index.html`: HTML file with a reference to the `main.js` file and instructions to check the console for results.

To view the results, open `index.html` in a web browser and check the console.