/**
 * @file: main.js
 * @author: Ambra
 * Implements a function called `aContainsb` that:
 * - Takes a two arguments, both strings
 * - Checks for the existence of the argument string as a substring within the first string.
 * - If it finds it, it returns true.
 * - If it doesn't, it returns false.
 *
 * Outside of the function, there's some test prints to check if it works correctly.
 */

/**
 * Function aContainsb
 * Returns true or false based on if strB is found within strA through the .indexOf method
 * @param {string, string} strA - The string that may contain the second one, strB - the string that may be contained in strA 
 * @returns {boolean} - returns true if strB is found in strA, else false
 */
function aContainsb(strA, strB) {
    if (strA.indexOf(strB) != -1)
        return true;
    else return false;
}

// Testing the aCpmtainsB function
console.log(aContainsb("you can't spell ostrich without kinda saying Asterix", "Asterix")); // true
console.log(aContainsb("yes or no?", "yesn't")); // false
console.log(aContainsb("apples shouldn't grow on trees, they're a registered trademark", "")); // true