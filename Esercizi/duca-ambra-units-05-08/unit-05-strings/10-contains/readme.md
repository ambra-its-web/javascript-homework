## Author
- **Name:** Ambra Duca
- **Contact:** [fabrizio.duca@edu.itspiemonte.it](mailto:fabrizio.duca@edu.itspiemonte.it)

## Exercise: Contains

### Requirements
Create a function called aContainsb
- It should take in two strings, and return true if the first string contains the second, otherwise it should return false
For example
aContainsB ("Another hello world", "hell");


### Approach to Solution
I Implemented a function called `aContainsb` that:
- Takes two arguments, `strA` and `strB`, both strings.
- Checks for the existence of `strB` as a substring within `strA`.
- If it finds it, it returns true.
- If it doesn't, it returns false.

Outside of the function, there's some test prints to check if it works correctly.

### Files
- `main.js`: Contains the implementation of the `aContainsb` function.
- `index.html`: HTML file with a reference to the `main.js` file and instructions to check the console for results.

To test the `aContainsb` function further, call it with different input strings and observe the returned results in the console.