## Author
- **Name:** Ambra Duca
- **Contact:** [fabrizio.duca@edu.itspiemonte.it](mailto:fabrizio.duca@edu.itspiemonte.it)

## Exercise: The group

### Requirements
Use the previous function to write another function called group that checks whether a string is part of another longer string that is a list of names of a group

The function should output the results to the console

``` JS
let group = "Mary, James, and John";
let oldGuy = "James";
// Outputs: "James IS part of the group"
let newGuy = "Philip";
// Outputs: "Philip is NOT part of the group
```

### Approach to Solution
I Implemented a function called `group` that:
- Takes one argument, `nominee`, a string.
- Checks for the existence of `strB` as a substring within `strA` and if it's not an empty string `""`.
- If it finds it and it's not an empty string, it returns `nominee + " IS part of the group"`
- If it doesn't, it returns `nominee + " is NOT part of the group"`.

Outside of the function, there's some test prints to check if it works correctly.

Most notably:
- The empty string variable returns the second string as expected due to the control in the if statement
- The two letter variable returns the first string because of the lack of regular expression controls while using a string to look into instead of an array of strings or an object.

### Files
- `main.js`: Contains the implementation of the `group` function.
- `index.html`: HTML file with a reference to the `main.js` file and instructions to check the console for results.

To test the `group` function further, call it with different input strings and observe the returned results in the console.