/**
 * @file: main.js
 * @author: Ambra
 * Implements a function called `group` that:
 * - Takes a two arguments, both strings
 * - Checks for the existence of the argument string as a substring within the first string.
 * - If it finds it, it returns true.
 * - If it doesn't, it returns false.
 *
 * Outside of the function, there's some test prints to check if it works correctly.
 */

const groupStr = "Mary, James, and John"; // String to be checked for the presence of substrings

/**
 * Function group
 * Returns true or false based on if nominee is found within groupStr through the .indexOf method
 * @param {string} nominee - the string that may be contained in groupStr 
 * @returns {boolean} - returns true if nominee is found in groupStr, else false
 */
function group(nominee) {
    if (groupStr.indexOf(nominee) != -1 && nominee != "")
        return nominee + " IS part of the group";
    else return nominee + " is NOT part of the group";
}

let member1 = "Mary";
let member2 = "James";
let member3 = "John";

let new1 = "Elisa";
let new2 = "Claudio";
// edge cases
let new3 = "";
let new4 = "oh"

// Testing the group function
// First three members return the first string as expected
console.log(group(member1));
console.log(group(member2));
console.log(group(member3));

// First two new variables return the second string as expected
console.log(group(new1));
console.log(group(new2));

// new3 returns the second string since it fails the "not an empty string" control
// in the if that leads to the first string being returned
console.log(group(new3));

// new4 returns the first string because of the lack of regEx controls 
// while using strings instead of an object or an array of strings
console.log(group(new4));