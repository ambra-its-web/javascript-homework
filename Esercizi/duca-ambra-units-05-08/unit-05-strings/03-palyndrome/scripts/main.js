/**
* @file: main.js
* @author: Ambra
* Introduces logic printed on the console accordingly with the specifications of the 3rd exercise of unit 5.
* Defines a function `reverse` that takes a single argument, `str`, representing the input string. 
- Within the function:
- Initialization of an empty string `reversedString`.
- Use of a loop to iterate through the characters of the input string from the end to the beginning.
- Appending each character to `reversedString`.
- Returning the reversed string.
*/

/**
 * Function reverse
 * Returns a given string in reverse.
 * @param {string} str - The input string.
 * @returns {string} reversedString - The reversed string.
 */
function reverse(str) {
    let reversedString = ""; // Initializing an empty string variable to store the reversed string in it
    for (let i = str.length - 1; i >= 0; i--) { // Iterates through the characters of the input string from the end to the beginning, so in reverse
        reversedString += str.charAt(i); // Append through += each character of the input string to the reversedString variable
    }
    return reversedString; // Returning the variable so that it can be printed through a console.log
}

/**
 * Function isPalindrome
 * Checks if a given string is a palindrome.
 * @param {string} str - The input string.
 * @returns {boolean} - True if the string is a palindrome, false otherwise.
 */
function isPalindrome(str) {
    // Convert the input string to lowercase to ignore case sensitivity
    str = str.toLowerCase();
    // Reverse the input string using the reverse function
    let reversedStr = reverse(str);
    // Check if the original string is equal to the reversed string
    return str === reversedStr;
};

// Test the isPalindrome function
console.log("madam");
console.log(isPalindrome("madam")); // Output: true
console.log("Madam");
console.log(isPalindrome("Madam")); // Output: true (case insensitive)
console.log("racecar");
console.log(isPalindrome("racecar")); // Output: true
console.log("hello");
console.log(isPalindrome("hello")); // Output: false
console.log("Amy must I jujitsu my ma");
console.log(isPalindrome("Amy must I jujitsu my ma")); // Output: false (case insensitive, but doesn't ignore whitespaces without a regEx)
// we could do that with: const reverseStr = str.replace(/\s/g, '').toLowerCase(); within the function
console.log(""); // Empty print for spacing
console.log("Type 'console.log(isPalindrome('Your String Here'))' to test it out. Doesn't ignore whitespaces, but is case insensitive.")