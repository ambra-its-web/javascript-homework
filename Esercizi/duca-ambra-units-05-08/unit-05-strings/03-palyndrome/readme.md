## Author
- **Name:** Ambra Duca
- **Contact:** [fabrizio.duca@edu.itspiemonte.it](mailto:fabrizio.duca@edu.itspiemonte.it)

## Exercise: Palindrome

### Requirements
- Write a JavaScript function named `isPalindrome` that:
  - Takes one argument, a string.
  - Returns `true` if the string is a palindrome (reads the same backwards as forwards), otherwise `false`.

### Approach to Solution
To check if a string is a palindrome, I used the `reverse` function implemented earlier to reverse the input string and then compared it with the original string. Within the `isPalindrome` function:
- Converted the input string to lowercase to ignore case sensitivity.
- Reversed the input string using the `reverse` function.
- Checked if the original string is equal to the reversed string.
The javascript file ends with a series of function print tests

### Files
- `main.js`: Contains the implementation of the `isPalindrome` function.
- `index.html`: HTML file with a reference to the `main.js` file and instructions to check the console for results.

To test the `isPalindrome` function further, call it with different input strings and observe the returned results in the console.