/**
* @file: main.js
* @author: Ambra
* Introduces logic printed on the console accordingly with the specifications of the 4th exercise of unit 5.
* Implements the `capitalizeFirst` function, which:
- Picks the first string character with `.charAt` and capitalizes it with `.toLocaleUpperCase` (which accounts for the local language) and assigns its value to `modStr`.
- Stores in the variable `rest` the rest of the string using `.slice`
- Adds `rest` to `modStr`
- Returns `modStr`
- Ends with a series of function print tests
*/

/**
 * Function capitalizeFirst
 * Returns a given string with its first character capitalized
 * @param {string} str - The input string.
 * @returns {string} modStr - a modified version of the input string, with the first character capitalized
 */
function capitalizeFirst(str) {
    modStr = str.charAt(0).toLocaleUpperCase(); // Picking the first string character
    rest = str.slice(1,);
    modStr += rest;
    return modStr;
}

// Test the capitalizeFirst function
console.log("madam");
console.log(capitalizeFirst("madam"));
console.log("racecar");
console.log(capitalizeFirst("racecar"));
console.log("hello world");
console.log(capitalizeFirst("hello world"));
console.log("amy must I jujitsu my ma");
console.log(capitalizeFirst("amy must I jujitsu my ma"));
console.log(""); // Empty print for spacing
console.log("Type 'console.log(capitalizeFirst('Your String Here'))' to test it out.")
/*
const toUpper3 = string => string.toUpper3();

const toUpper4 = (string) => string.toUpper4();

const toUpper5 = string => {
    return string.toUpperCase
}*/