## Author
- **Name:** Ambra Duca
- **Contact:** [fabrizio.duca@edu.itspiemonte.it](mailto:fabrizio.duca@edu.itspiemonte.it)

## Exercise: Palindrome

### Requirements
- Write a JavaScript function called capital which has one parameter, a string, and which returns that string with the first letter capitalized
- For example, the call capital("hello world") should return the string "Hello world".

### Approach to Solution
To capitalize the first letter of a string, I implemented the `capitalizeFirst` function, which:
- Picks the first string character with `.charAt` and capitalizes it with `.toLocaleUpperCase` (which accounts for the local language) and assigns its value to `modStr`.
- Stores in the variable `rest` the rest of the string using `.slice`
- Adds `rest` to `modStr`
- Returns `modStr`
The javascript file ends with a series of function print tests.

### Files
- `main.js`: Contains the implementation of the `capitalizeFirst` function.
- `index.html`: HTML file with a reference to the `main.js` file and instructions to check the console for results.

To test the `capitalizeFirst` function further, call it with different input strings and observe the returned results in the console.