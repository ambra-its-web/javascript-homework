## Author
- **Name:** Ambra Duca
- **Contact:** [fabrizio.duca@edu.itspiemonte.it](mailto:fabrizio.duca@edu.itspiemonte.it)

## Exercise: MixUp

### Requirements
- Create a function called mixUp
- It should take in two strings, and return the concatenation of the two strings (separated by a
space) slicing out and swapping the first 2 characters of each
- You can assume that the strings are at least 2 characters long
For example
mixUp('mix', 'pod'): 'pox mid'
mixUp('dog', 'dinner'): 'dig donner'

### Approach to Solution
I implemented a function called mixUp that:
- Checks if both `str1` and `str2` are greater than 1. If they are:
- Declares and initializes the variables `str1first2` and `str2first2`, whose values are the first two characters of the other string (value obtained through the string method `slice`: `strX.slice(0, 2)`)
- Declares the variable `strMixedUp` and assigns the concatenation of `str2first2`(the first two characters of `str2`) with the rest of `str1`(`str1.slice(2)`), a space, the first two characters of `str1` and the rest of `str2`.
- Returns `strMixedUp`
- If any of the parameters is lesser than 1, it returns "The function only works if both strings are at least 2 characters long."
Outside of the function, there's some test prints to check if it works correctly.

### Files
- `main.js`: Contains the implementation of the `mixUp` function.
- `index.html`: HTML file with a reference to the `main.js` file and instructions to check the console for results.

To test the `mixUp` function further, call it with different input strings and observe the returned results in the console.