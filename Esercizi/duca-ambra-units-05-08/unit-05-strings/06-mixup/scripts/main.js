/**
* @file: main.js
* @author: Ambra
* Introduces logic printed on the console accordingly with the specifications of the 6th exercise of unit 5.
* Implements a function called mixUp that:
- Checks if both `str1` and `str2` are greater than 1. If they are:
- Declares and initializes the variables `str1first2` and `str2first2`, whose values are the first two characters of the other string (value obtained through the string method `slice`: `strX.slice(0, 2)`)
- Declares the variable `strMixedUp` and assigns the concatenation of `str2first2`(the first two characters of `str2`) with the rest of `str1`(`str1.slice(2)`), a space, the first two characters of `str1` and the rest of `str2`.
- Returns `strMixedUp`
- If any of the parameters is lesser than 1, it returns "The function only works if both strings are at least 2 characters long."
Outside of the function, there's some test prints to check if it works correctly.
*/

/**
 * Function mixUp
 * Returns a string obtained from concatenating two strings it takes (putting a space between them), slicing out and swapping the first 2 characters of each
 * @param {string} str1 - The first string
 * @param {string} str2 - The second string
 * @returns {string} strMixedUp - a modified version of the 2 strings taken by the function, formed of the two values concatenated and separated by a space, with the two first characters of each swapped out
 * @returns {string} - if the parameters don't fulfill the if requirement, an explanation for the lack of a mixed up string result is logged instead
 */
function mixUp(str1, str2) {
    if (str1.length > 1 && str2.length > 1){ // checking if the values can be processed by the function, going through with the process if truthy
        // slicing the first two characters in each string
        let str1first2 = str1.slice(0, 2);
        let str2first2 = str2.slice(0, 2);
        
        // concatenating the strings obtained from swapping the two sliced pairs of characters in each string
        let strMixedUp = str2first2.concat(str1.slice(2), " ", str1first2, str2.slice(2));
        
        return strMixedUp;
    } else
    return "The function only works if both strings are at least 2 characters long."; //returned string value 
}

// Testing the mixUp function
console.log(mixUp("funny", "dog"));
console.log(mixUp("super", "duper"));
console.log(mixUp("cult", "following"));
console.log(mixUp("I", "want")) // checking if the minimum length limit works
console.log("Type 'console.log(mixUp('Your String Here', 'Your String Here'))' to test it out.")