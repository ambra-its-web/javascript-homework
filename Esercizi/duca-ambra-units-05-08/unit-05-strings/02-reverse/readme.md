## Author
- **Name:** Ambra Duca
- **Contact:** [fabrizio.duca@edu.itspiemonte.it](mailto:fabrizio.duca@edu.itspiemonte.it)

### Requirements
- Write a JavaScript function named `reverse` that:
  - Takes one argument, a string.
  - Returns the string in reverse order.

### Approach to Solution
I've updated the `reverse` function to correctly return the reversed string. Within the `reverse` function:
- Initialized an empty string `reversedString`.
- Used a loop to iterate through the characters of the input string from the end to the beginning.
- Appended each character to `reversedString`.
- Returned the reversed string.

### Files
- `main.js`: Contains the implementation of the `reverse` function.
- `index.html`: HTML file with a reference to the `main.js` file and instructions to check the console for results.

To view the results, open `index.html` in a web browser, and check the console.

You can test this solution further by calling the `reverse` function with different input strings.