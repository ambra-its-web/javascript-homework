/**
* @file: main.js
* @author: Ambra
* Introduces logic printed on the console accordingly with the specifications of the 2nd exercise of unit 5.
* Defines a function `reverse` that takes a single argument, `str`, representing the input string. 
- Within the function:
- Initialization of an empty string `reversedString`.
- Use of a loop to iterate through the characters of the input string from the end to the beginning.
- Appending each character to `reversedString`.
- Returning the reversed string.
*/

/**
 * Function reverse
 * Returns a given string in reverse.
 * @param {string} str - The input string.
 * @returns {string} reversedString - The reversed string.
 */
function reverse(str) {
    let reversedString = ""; // Initializing an empty string variable to store the reversed string in it
    for (let i = str.length - 1; i >= 0; i--) { // Iterates through the characters of the input string from the end to the beginning, so in reverse
        reversedString += str.charAt(i); // Append through += each character of the input string to the reversedString variable
    }
    return reversedString; // Returning the variable so that it can be printed through a console.log by assigning the call result to a variable out of function scope
}

// Assigning the result of reverse to a variable
let reversedResult = reverse("Lying on the grass by the river waiting for a change never coming");
console.log(reversedResult); // Output: "gninmoc reven egnahc a rof gnitiaw revir eht yb ssarg eht no gniyL"
console.log(""); // Empty print for spacing
console.log("Type 'console.log(reverse('Your String Here'))' to test it out")