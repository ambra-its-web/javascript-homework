/**
* @file: main.js
* @author: Ambra
* Implements a function called `fixStart` that:
* - Checks if the input string `str` is at least 1 character long.
* - Iterates through the string starting from the second character.
* - Replaces each occurrence of the first character, identified through (`str.charAt(0)`) with '*' using the `substring` method.
* - Returns the modified string.
* - If the input string is less than 1 character long, it instead returns "The function only works if the string is at least 1 character long."
*
* Outside of the function, there's some test prints to check if it works correctly.
*/

/**
 * Function fixStart
 * Returns a string obtained from concatenating two strings it takes (putting a space between them), slicing out and swapping the first 2 characters of each
 * @param {string} str - The string to modify
 * @returns {string} str - a modified version of the first string taken by the function, where each character that's the same as the first one, excluding the first one, is replaced with "*"
 * @returns {string} - if the parameters don't fulfill the if requirement, an explanation for the lack of a modified string result is logged instead
 */
function fixStart(str) {
    if (str.length > 0){ // checking if the value can be processed by the function, going through with the process if truthy
        for (i = 1; i < str.length; i++){ // iterate through the whole string from the 2nd character onward
            if (str.charAt(i) == str.charAt(0)){ // if the character in that position is the same as the first
                str = str.substring(0, i) + '*' + str.substring(i + 1); // reconstruct the string each iteration by adding * 
        }
    }
        return str; // returning the starting string with a new value
    } else
    return "The function only works if the string is at least 1 character long."; //returned string value 
}

// Testing the fixStart function
console.log(fixStart("fluffy"));
console.log(fixStart("baker betty botter bought some butter, but she said 'this butter is bitter, bitter butter is bad for batter'"));
console.log(fixStart("pampered"));
console.log(fixStart("sailed")); // checking if the function returns an equal value to the original parameter if no replaceable characters are found
console.log(fixStart("uU")); // showing that it's not case sensitive
console.log(fixStart(""));// checking if the minimum length limit works properly
console.log("Type 'console.log(fixStart('Your String Here'))' to test it out.")