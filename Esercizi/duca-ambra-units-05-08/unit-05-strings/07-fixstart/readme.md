## Author
- **Name:** Ambra Duca
- **Contact:** [fabrizio.duca@edu.itspiemonte.it](mailto:fabrizio.duca@edu.itspiemonte.it)

## Exercise: fixStart

### Requirements
- Create a function called `fixStart`.
- It should take a single argument, a string, and return a version where all occurrences of its
first character have been replaced with '*', except for the first character itself.
- You can assume that the string is at least one character long.
For example,
`fixStart('babble')`: 'ba**le'

### Approach to Solution
I implemented a function called `fixStart` that:
- Checks if the input string `str` is at least 1 character long.
- Iterates through the string starting from the second character.
- Replaces each occurrence of the first character, identified through (`str.charAt(0)`) with '*' using the `substring` method.
- Returns the modified string.
- If the input string is less than 1 character long, it instead returns "The function only works if the string is at least 1 character long."

Outside of the function, there's some test prints to check if it works correctly.

### Files
- `main.js`: Contains the implementation of the `fixStart` function.
- `index.html`: HTML file with a reference to the `main.js` file and instructions to check the console for results.

To test the `fixStart` function further, call it with different input strings and observe the returned results in the console.
