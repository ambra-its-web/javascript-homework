## Author
- Name: Ambra

## Exercise: Scheduling Functions in JavaScript

### Requirements
- Write a function “useful” that does something useful in Javascript
- Schedule it to run after 10 seconds
- Write another function that cancels the scheduling of the first function
- Use the second function to cancel the first one after 5 seconds and output
‘function cancelled’ to the console

### Approach to Solution
This script simulates a dishwasher system using stacks to represent dirty and clean dishes.
 - The simulation involves:
 - Random number of dirty dishes (between 10 and 50) in each of three dirty stacks, generated directly in the object as a function invocation
 - One clean stack to store clean dishes after washing
 - Functions to wash dishes, display the current state of the stacks, and run the washing simulation
 - Bonus feature: The dishwasher can wash two dishes at a time

 - Functions:
 - runSimulation: Serves as a main function and invokes the function that manages the process, which is:
 - nextStep: checks if any dirty dishes remain by declaring a boolean and setting it to true if dirty dishes length is above 0 (is first set to false so that the program actually ends when conditions aren't met)
 - contains the invocations to washDish, displayStacks and setTimeout (which uses getRandomNumber), invoked if the boolean set is true
 - if it's false, logs a string and exits the function, ending the program
 - generateRandomStack: generates a stack of dishes with random count using getRandomNumber, found directly as an element in the array dirtyStacks in the object dishwasher
 - getRandomNumber: generates a random number in a range given in its invocation
 - washDish: Moves dishes from dirty stacks to the clean stack
 - displayStacks: Displays the current state of all stacks in the console

### Files
- `main.js`: Contains the implementation of the two functions.
- `index.html`: HTML file with a reference to the `main.js` file and instructions to check the console for results.

To view the exercise solution, open the console and run the script in `main.js`.