/**
 * Dishwasher System Simulation
 * Author: Ambra Duca
 * Contact: fabrizio.duca@edu.itspiemonte.it
 * 
 * This script simulates a dishwasher system using stacks to represent dirty and clean dishes.
 * The simulation involves:
 * - Random number of dirty dishes (between 10 and 50) in each of three dirty stacks, generated directly in the object as a function invocation
 * - One clean stack to store clean dishes after washing
 * - Functions to wash dishes, display the current state of the stacks, and run the washing simulation
 * - Bonus feature: The dishwasher can wash two dishes at a time
 * 
 * Functions:
 * - runSimulation: Serves as a main function and invokes the function that manages the process, which is:
 *   - nextStep: checks if any dirty dishes remain by declaring a boolean and setting it to true if dirty dishes length is above 0
 *   - contains the invocations to washDish, displayStacks and setTimeout (which uses getRandomNumber), invoked if the boolean set is true
 *   - if it's false, logs a string and exits the function, ending the program
 * - generateRandomStack: generates a stack of dishes with random count using getRandomNumber, found directly as an element in the array dirtyStacks in the object dishwasher
 * - getRandomNumber: generates a random number in a range given in its invocation
 * - washDish: Moves dishes from dirty stacks to the clean stack
 * - displayStacks: Displays the current state of all stacks in the console
 */

// Object to hold the dirty stacks and the clean one
const dishwasher = {
  dirtyStacks: [
      generateRandomStack(),
      generateRandomStack(),
      generateRandomStack()
  ],
  cleanStack: []
};

/**
 * Function that runs the simulation of washing dishes with random delays.
 * It checks if any dirty stacks still have dishes and continues the process until all dishes are clean.
 * @param {Object} dishwasher - The dishwasher system state
 */
function runSimulation(dishwasher) {
  /**
   * Function that represents a step in the washing process
   * It washes dishes if any dirty stacks still have dishes and sets a random delay for the next step
   * If all dishes are clean, it ends the simulation
   */
  function nextStep() {
      // Check if any dirty dishes remain
      let dirtyDishesRemaining = false; // Setting it as false allows 
      for (let i = 0; i < dishwasher.dirtyStacks.length; i++) {
          if (dishwasher.dirtyStacks[i].length > 0) {
              dirtyDishesRemaining = true;
              break;
          }
      }

      if (dirtyDishesRemaining) {
          washDish(dishwasher); /* Invoking the function that puts 2 dirty dishes into the clean stack */
          displayStacks(dishwasher); /* Invoking the function that displays the current state of the stacks and refreshes the console each time */
          
          /* Invoking the function that generates random numbers and giving it two
          arguments that will become the random amount of milliseconds given to
          setTimeout, which will be the time to wait before the next iteration as it
          occupies the js task queue */
          setTimeout(nextStep, getRandomNumber(500, 1500));  // Random delay between 0.5 to 1.5 seconds, (arbitrarily picked range)
      } else {
          // If dirtyDishesRemaining is false, the function logs a string to the console and the program ends
          console.log('BEEP, BEEP, BEEP, BEEP');
      }
  }

  nextStep(); // Starts the simulation by invoking the first step
}

/**
* Function that generates a stack of dishes with a random number of plates.
* - Generates a random number between 10 and 50 for the stack size
* - Populates the stack with dish elements
* @returns {Array} - The generated stack of dishes
*/
function generateRandomStack() {
  const stackSize = getRandomNumber(10, 50); // Generate a random number between 10 and 50
  const stack = []; // Declaring and initializing this stack array
  // Populate the stack with dish labels by using the push method
  for (let i = 0; i < stackSize; i++) {
      stack.push('Dish ' + (i + 1));
  }
  return stack; // Returning the generated populated dirty dishes stack, which is now an element of the object dishwasher
}

/**
* Function that generates a random number between min and max
* Making a generic function comes in handy since it'll be used both for generating the
* random length of the dirty stacks and for generating the random delay in 
* @param {number} min - The minimum value for the random number
* @param {number} max - The maximum value for the random number
* @returns {number} - The generated random number
*/
function getRandomNumber(min, max) {
  let random = Math.random(); // Generates a random decimal between 0 included and 1 excluded
    let bigger = random * (max - min); // By multiplying the random decimal by the difference between the max and min of the range needed, the result is within the max limit
    let result = Math.round(bigger + min); // Adding the number with the minimum range ensures the result is equal to or greater than the min limit. The round method of the class Math rounds the sum of the two to its closest integer
    return result;
}

/**
* Function that moves up to two dishes from the dirty stacks to the clean stack
* - Iterates over the dirty stacks
* - Removes the top dish from the first non-empty dirty stack
* - Adds the removed dish to the clean stack
* @param {Object} dishwasher - The dishwasher object (containing the stacks)
*/
function washDish(dishwasher) {
  for (let i = 0; i < 2; i++) {  // BONUS: Wash two dishes at a time

      // Iterate over the dirty stacks
      for (let j = 0; j < dishwasher.dirtyStacks.length; j++) {
          if (dishwasher.dirtyStacks[j].length > 0) { // Check if the current dirty stack has dishes
              const dish = dishwasher.dirtyStacks[j].pop()// Remove the top dish from the dirty stack();
              dishwasher.cleanStack.push(dish); // Add the removed dish to the clean stack
              break;  // Stop after washing one dish from the stack
          }
      }
  }
}

/**
* Function that displays the current state of all stacks in the console.
* - Clears the console to avoid clutter
* - Logs each dirty stack array
* - Logs the clean stack array
* @param {Object} dishwasher - The dishwasher object
*/
function displayStacks(dishwasher) {
  console.clear();  // Clear the console at the start to avoid clutter
  // Iterate through the dirty stack array
  for (let i = 0; i < dishwasher.dirtyStacks.length; i++) {
      console.log('Dirty Stack ' + (i + 1) + ':', dishwasher.dirtyStacks[i]); // Log each dirty stack
  }
  console.log('Clean Stack:', dishwasher.cleanStack); // Log the clean stack
}

// Display the initial state of the stacks
displayStacks(dishwasher);  
// Start the washing simulation
runSimulation(dishwasher);  