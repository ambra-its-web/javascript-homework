## Author
- **Name:** Ambra
- **Contact:** [fabrizio.duca@edu.itspiemonte.it](mailto:fabrizio.duca@edu.itspiemonte.it)

## Exercise: Credit Card Validation

### Requirements
- Write a function called `validateCreditCard` that checks credit card numbers according to the following rules:
  - Number must be 16 digits, all of them must be numbers.
  - You must have at least two different digits represented (all of the digits cannot be the same).
  - The final digit must be even.
  - The sum of all the digits must be greater than 16.
- Call the function with several credit card numbers:
- The function returns an object saying that the credit card is valid, or what the error is:
- For each card check, print out the result to the log

### Approach to Solution
- Defined a function `validateCreditCard` that:
  - Uses a regular expression to remove dashes from the input string.
  - Checks if the card number is exactly 16 digits and contains only digits.
  - Uses a regular expression to check if all digits are the same.
  - Checks if the final digit is even.
  - Calculates the sum of all digits.
  - Checks if the sum is greater than 16.
  - Returns an object indicating the validity of the credit card number and any errors.
- Defined a function `printValidationResult` to format and print the validation results to the console.
- Tested the function with several credit card numbers and printed the results.

### Files
- `main.js`: Contains the implementation of the `validateCreditCard` and the `printValidationResult` functions, along with the examples in the console logs.
- `index.html`: HTML file with a reference to the `main.js` file and instructions to check the console for results.

To view the exercise solution, open the console and run the script in `main.js`.
