/**
 * @file: main.js
 * @author: Ambra
 * Implements a function called validateCreditCard that checks credit card numbers according to the given rules.
 * Another function manages the print layout of the each result
 * Both are used in several test prints at the end of the program
 */

/**
 * Function to validate a credit card number, it:
 * - Removes dashes from the input credit card number using a regular expression
 * - Checks if the card number is exactly 16 digits and contains only digits
 * - Check if all digits are the same using a regular expression
 * - Checks if the final digit is even
 * - Calculates the sum of all digits
 * - Checks if the sum is greater than 16
 * - Returns an object saying if the credit card is valid or not with the eventual error encountered
 * @param {string} cardNumber - The credit card number to validate
 * @returns {object} - An object indicating if the credit card is valid or not, and the error if any
 */
function validateCreditCard(cardNumber) {
    // Remove dashes from the input credit card number
    cardNumber = cardNumber.replace(/-/g, ''); // matches all (g) dashes (-) and replaces them with an empty string ('')
    parseInt(cardNumber);
    
    // Check if the card number is exactly 16 digits and contains only digits
    if (cardNumber.length !== 16 || isNaN(cardNumber)) {
        return { valid: false, number: cardNumber, error: 'wrong_length' };
    }

    // Check if all digits are the same
    let allDigitsSame = /^(\d)\1*$/.test(cardNumber); // checks if the first digit is -the only one- repeated from the start to the end of the string and returns a boolean value
    if (allDigitsSame) {
        return { valid: false, number: cardNumber, error: 'only_one_number' };
    }

    // Check if the final digit is even
    if (parseInt(cardNumber[cardNumber.length - 1]) % 2 !== 0) { // parseInt to allow the code to be in a numerical expression
        return { valid: false, number: cardNumber, error: 'odd_final_number' };
    }

    // Calculate the sum of all digits
    let sum = 0;
    for (let i = 0; i < cardNumber.length; i++) {
        sum += parseInt(cardNumber[i]);
    }
    // Check if the sum is greater than 16
    if (sum <= 16) {
        return { valid: false, number: cardNumber, error: 'sum_less_than_16' };
    }

    // If all checks pass, the credit card is valid
    return { valid: true, number: cardNumber };
}

/**
 * Function to print the result of credit card as described in the instructions
 * @param {object} result - The result object from validateCreditCard function.
 */
function printValidationResult(result) {
    console.log("`= number : " + result.number + " =`");
    console.log("`= valid : " + result.valid + " =`");
    if (!result.valid) {
        console.log("`= error : " + result.error + " =`");
    }
    console.log("================================");
}

// Calling the function with several credit card numbers and print out the result
let cardNumbers = [
    '9999-9999-8888-0000', // valid
    '9999-9999-8888-0001', // odd_final_number error
    '4444-4444-4444-4444', // only_one_number error
    '6666666666661666',    // valid
    '0000-0000-111-00010', // sum_less_than_16 error
    '123-6666-6666-1666'   // wrong_length error
];

// Iterate over each card number, validate it, and print the result
cardNumbers.forEach(cardNumber => {
    let result = validateCreditCard(cardNumber);
    printValidationResult(result);
});