/**
 * @file: main.js
 * @author: Ambra
 * Implements a reading list with objects to store book information, an array to hold multiple books,
 * and a function to print book information based on the alreadyRead boolean value.
 */

// Create an array of objects to hold several books
let books = [
    {
        title: "La Terra dei Figli", // First property used to make the bookNameAuthor string
        author: "Gipi", // Second property used to make the bookNameAuthor string
        alreadyRead: true // The boolean value to determine which of the two console.logs to invoke
    },
    {
        title: "The Hobbit",
        author: "J.R.R. Tolkien",
        alreadyRead: false
    },
    {
        title: "Pet Sematary",
        author: "Stephen King",
        alreadyRead: true
    },
    {
        title: "Dune",
        author: "Frank Herbert",
        alreadyRead: false
    },
    {
        title: "The Sorrows of young Werther",
        author: "Johann Wolfgang Goethe",
        alreadyRead: true
    }
];

// Iterate through the array of books and log the book title and author
books.forEach(function(book) {
    let bookNameAuthor = book.title + " by " + book.author;
    if (book.alreadyRead) {
        console.log('You already read "' + bookNameAuthor);
    } else {
        console.log('You still need to read "' + bookNameAuthor);
    }
});