## Author
- **Name:** Ambra Duca
- **Contact:** [fabrizio.duca@edu.itspiemonte.it](mailto:fabrizio.duca@edu.itspiemonte.it)

## Exercise: Reading list

### Requirements
- Create an array of objects, where each object describes a book and has properties for the title (a string), author (a string), and alreadyRead (a boolean indicating if you read it yet).
- Iterate through the array of books. For each book, log the book title and book author like so: "The Hobbit by J.R.R. Tolkien".
- Now use an if/else statement to change the output depending on whether you read it yet or not. If you read it, log a string like 'You already read "The Hobbit" by J.R.R. Tolkien', and if not, log a string like 'You still need to read "The Lord of the Rings" by J.R.R. Tolkien.'

### Approach to Solution
- I defined an array `books` of objects, where each object describes a book with properties for title, author, and alreadyRead
- I iterated through the array of books
- I logged the book title and author.
- Finally, I used an if/else statement to change the logged output based on the `alreadyRead` property of each book.

### Files
- `main.js`: Contains the implementation of the reading list, including the array of books and the logic to log book information based on reading status.
- `index.html`: HTML file with a reference to the `main.js` file and instructions to check the console for results.

To view the exercise solution, check the console after running the script in `main.js`.