/**
 * @file: main.js
 * @author: Ambra
 * Implements a favoriteRecipe object to hold information about a recipe
 * It has properties for title (a string), servings (a number), and ingredients (an array of strings).
 */

// Object that holds three properties
let favoriteRecipe = {
    // Object properties
    title: "Shrimp Cocktail",
    servings: 4,
    ingredients: ["shrimp", "cocktail sauce", "iceberg lettuce", "vecchia romagna", "salt", "black pepper"]
};

// Log the recipe with different console logs
console.log("Title:", favoriteRecipe.title);
console.log("Servings:", favoriteRecipe.servings);
console.log("Ingredients:", recipe.ingredients.join(", ")); // .join method to show each string in the array