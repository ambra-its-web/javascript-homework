## Author
- **Name:** Ambra Duca
- **Contact:** [fabrizio.duca@edu.itspiemonte.it](mailto:fabrizio.duca@edu.itspiemonte.it)

## Exercise: Recipe

### Requirements
- Create an object to hold information on your favorite recipe. It should have properties for title (a string), servings (a number), and ingredients (an array of strings).
- On separate lines (one console.log statement for each), log the recipe information.

### Approach to Solution
I defined an object named `favoriteRecipe` to hold information about a favorite recipe. It has properties for title, servings, and ingredients. Then, I logged each property of the recipe on separate lines using `console.log`.

### Files
- `main.js`: Contains the implementation of the `favoriteRecipe` object.
- `index.html`: HTML file with a reference to the `main.js` file and instructions to check the console for results.

To view the exercise solution, open the console and run the script in `main.js`.