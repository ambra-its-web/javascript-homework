## Author
- **Name:** Ambra
- **Contact:** [fabrizio.duca@edu.itspiemonte.it](mailto:fabrizio.duca@edu.itspiemonte.it)

## Exercise: Movie Database

### Requirements
- Create an object to store the following information about a movie: title (a string), duration (a number), and stars (an array of strings).
- Create an Array of objects that can hold several movies.
- Create a function to print out the movie information like so: "Puff the Magic Dragon lasts for 30 minutes. Stars: Puff, Jackie, Living Sneezes."
- Test the function by printing one movie.
- Use the function to print all the movies in the Array.

### Approach to Solution
- Implemented a movie database with objects to store movie information.
- Created an array to hold multiple movies.
- Developed a function, `printMovieInfo`, to print movie information.
- Defined properties for each movie object, including title, duration, and stars.
- Utilized the `printMovieInfo` function to log movie information to the console using string concatenation.
- Tested the `printMovieInfo` function by printing details of one movie.
- Used the `printMovieInfo` function to print details of all movies in the array.

### Files
- `main.js`: Contains the implementation of the movie database objects, array, and the `printMovieInfo` function.
- `index.html`: HTML file with a reference to the `main.js` file and instructions to check the console for results.

To view the exercise solution, open the console and run the script in `main.js`.