/**
 * @file: main.js
 * @author: Ambra
 * Implements a movie database with objects to store movie information, an array to hold multiple movies, and a function to print movie information.
 */

// Define an object to store movie information
let movie = {
    title: "Puff the Magic Dragon",
    duration: 30,
    stars: ["Puff", "Jackie", "Living Sneezes"]
};

// Create an array of objects to hold several movies
let movies = [
    {
        title: "The Lion King",
        duration: 90,
        stars: ["Matthew Broderick", "Jeremy Irons"]
    },
    {
        title: "Finding Nemo",
        duration: 100,
        stars: ["Albert Brooks", "Ellen DeGeneres", "Alexander Gould"]
    },
    {
        title: "Joker",
        duration: 121,
        stars: ["Joaquin Phoenix", "Robert De Niro", "Zazie Beetz", "Frances Conroy"]
    }
];

// Create a function to print out the movie information
function printMovieInfo(movie) {
    console.log(movie.title + " lasts for " + movie.duration + " minutes. Stars: " + movie.stars.join(', ') + ".");
}

// Test the function by printing one movie
printMovieInfo(movie);

// Use the function to print all the movies in the array
movies.forEach(function(movie) {
    printMovieInfo(movie);
});