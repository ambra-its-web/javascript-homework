## Author
- **Name:** Ambra
- **Contact:** [fabrizio.duca@edu.itspiemonte.it](mailto:fabrizio.duca@edu.itspiemonte.it)

## Exercise: Cash Register

### Requirements
- Implement a function called `cashRegister` that takes a shopping cart object.
- The shopping cart object contains item names and prices (itemName: itemPrice).
- The function should return the total price of the shopping cart.
- The function should:
  - Create an array containing each item in the cart object.
  - Iterate through this array, summing all the prices.
  - Return the total price.

### Approach to Solution
- Initialized a variable `total` to 0 to hold the sum of the item prices.
- Used `Object.keys(cart)` to make an array of the items in the `cart` object.
- Iterated through the `items` array using a `for` loop.
- For each item, accessed the price, converted it to a floating-point number using `parseFloat`, and added it to the `total`.
- Returned the `total`, which contains the sum of all item prices.

### Files
- `main.js`: Contains the implementation of the `cashRegister` function, example shopping cart objects, and the log of total prices calculated by the function.
- `index.html`: HTML file with a reference to the `main.js` file and instructions to check the console for results.

To view the exercise solution, open the console and run the script in `main.js`.