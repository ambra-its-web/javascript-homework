/**
 * @file: main.js
 * @author: Ambra
 * Implements a function called cashRegister that takes a shopping cart object,
 * which contains item names and prices (itemName: itemPrice).
 * Within the function, an array containing the items is made through Object.keys(cart),
 * which is then iterated through, each item's price being added to the total every loop.
 * The total is then returned and logged for the example objects
 */

/** #FIXME
 * Function cashRegister
 * Returns the total price of the shopping cart by
 * - Making an array containing each item in the cart object
 * - Iterating through it to sum all the prices
 * @param {object} cart - The shopping cart object
 * @returns {number} - The total price of the shopping cart
 */
function cashRegister(cart) {
    // Initialize total to 0 since it will be a sum of the prices
    let total = 0;

    // Making an array of the keys (item names) in the cart object
    const items = Object.keys(cart);

    // Iterating through the items array with a for loop
    for (let i = 0; i < items.length; i++) {
        // Accessing the current item's price and converting it to a floating-point number with the built-in object parseFloat
        total += parseFloat(cart[items[i]]);
    }

    // Returning the total, which now contains the sum of all item prices
    return total;
}

// Object cartForParty given as an example in the exercise instruction
let cartForParty = {
    banana: "1.25",
    handkerchief: ".99",
    Tshirt: "25.01",
    apple: "0.60",
    nalgene: "10.34",
    proteinShake: "22.36"
};

// Example object 2
let cartForQualityTime = {
    chocolate: "2.99",
    cokeCan: "2",
    rentedMovie: "5.99",
    pineappleSlices: "4.37",
    goodHeadphones: "129.43",
    warmBlanket: "11.3"
};

// Log of the total price calculated by the function cashRegister for the two example objects
console.log(cashRegister(cartForParty)); // 60.55
console.log(cashRegister(cartForQualityTime)); // 156.08