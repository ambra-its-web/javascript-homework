/**
 * @file main.js
 * @author Ambra
 * @description This script:
 * - Imports the CSS file
 * - Defines a function to encapsulate the promise fetch
 *    - Sets the endpoint of the API to the constant `url`
 *    - Sets a counter for fetch attempts
 *    - Defines a function `fetchJoke` which makes the fetch request and handles errors
 *    - Returns a promise that resolves with the joke text, which has also a small chance of returning a "manual error" instead
 * - Gets the joke element by ID
 * - Invokes the fetchRandomJoke function displays the joke or error notification on the page in the joke element
 */

import "../styles/main.css";

/**
 * Function to fetch a random joke from the API
 * - Sets the endpoint of the API to the constant `url`
 * - Sets a counter for fetch attempts
 * - Makes a fetch request to the API endpoint
 * - Handles errors that may occur during the fetching process
 * - Retries the fetch operation up to 3 times before giving up
 * - Returns a promise that resolves with the joke text or rejects with an error
 *
 * @returns {Promise} - A promise that resolves with the joke text or rejects with an error
 */
function fetchRandomJoke() {
  // API endpoint URL
  const url = "https://official-joke-api.appspot.com/random_joke";

  // Counter for the number of fetch attempts
  let attempts = 0;

  // Return a new promise that resolves with the joke text or rejects with error
  return new Promise((resolve, reject) => {
    // Define a function to make a fetch request and handle errors
    function fetchJoke() {
      fetch(url)
        .then((response) => {
          // If the response is not OK, throw an error
          if (!response.ok) {
            throw new Error(`HTTP error! status: ${response.status}`);
          }
          // Parse the response as JSON
          return response.json();
        })
        .then((data) => {
          // Extract joke text from the response data
          const joke = data.setup + " " + data.punchline;

          // Due to how unlikely it is to get an error for this, I included a chance to get a manual error to check functionality
          // Generating a random number
          const random = Math.random();
          // If the number is less than 0.2, manually reject the promise with an error
          if (random < 0.2) {
            const error = new Error("You got unlucky");
            reject(error);
          } else {
            // Else resolve the promise with the joke text
            resolve(joke);
          }
        })
        .catch((error) => {
          // If amount of attempts < 3, retry fetching
          if (attempts < 3) {
            attempts++;
            fetchJoke();
          } else {
            // Else, reject the promise with the error
            reject(error);
          }
        });
    }

    // Call the fetchJoke function to start the fetch operation
    fetchJoke();
  });
}

// Get the joke element by its ID here since it's used in both the error handler and the success handler and the resolved promise
const jokeElement = document.getElementById("joke");

// Invoking the fetchRandomJoke function
fetchRandomJoke()
  .then((joke) => {
    // Update the content of the 'joke' element with the fetched joke
    jokeElement.textContent = joke;
  })
  .catch((error) => {
    // If an error occurs, remove the 'joke' element from the page
    jokeElement.remove();
    // Log the error to the console
    console.error("Failed to be funny:", error);

    // Use a new p element to display the error message
    const errorElement = document.createElement("p");
    errorElement.id = "error";
    errorElement.textContent = "An error has occurred, check the console!";
    document.body.appendChild(errorElement);
  });
