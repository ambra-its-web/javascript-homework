## Author

- **Name:** Ambra

## Exercise: You're joking

### Requirements

Create a function called `fetchRandomJoke()` that fetches one random joke from a Random joke API and returns a promise that resolves with the text of the joke.

Create a page that uses the function and displays the joke on the page or an error message if the promise rejects.

The function:

- Should use error handling to handle errors that may occur during the fetching
- Should return a Promise that resolves with the joke text, not the joke text itself
- If the fetch operation fails, the function should retry the operation up to 3 times before giving up
- If the fetch operation fails after 3 attempts, the function should reject the promise

### Approach to Solution

#### Javascript

The [main.js](./src/scripts/main.js) file contains the bulk of the exercise's logic

At the beginning of the script I import the main.css file due to this being a webpack project to style the webpage

## 2 Defining the `fetchRandomJoke()` function

The `fetchRandomJoke()` function is the main function of the script. It is responsible for setting the counter of fetch attempts starting from 0,

### Files

- `index.html`: The entry point for the application, containing instructions for the exercise.
- `main.css`: Provides basic styling for the application.
- This project was made in Webpack and thus contains many generated files and a dist folder containing the tested build bundle
