/**
 * Roulette
 * Author: Ambra Duca
 * This code:
 * - defines a function `round` which takes an optional `label` and `delay` as parameters.
 * - uses the `setTimeout` function to create a promise that resolves or rejects after the specified delay.
 * - returns the promise and logs a message to the console.
 * - uses the `Promise.allSettled` method to wait for all the promises to settle before logging the results to the console.
 * - logs the results of each promise to the console, whether they were resolved or rejected.
 */

/**
 * @function round
 * @description A function that simulates a roulette round. It does so by:
 * - creating a promise that resolves or rejects after a specified delay
 * - returns the promise and logs a message to the console.
 *
 * @param {string} [label="round"] - A label for the round.
 * @param {number} [delay=500] - The delay in milliseconds before resolving or rejecting.
 * @returns {Promise} A promise that resolves with a success message or rejects with a failure message.
 */
const round = (label = "round", delay = 500) => {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      const outcome = Math.random() < 0.5; // 50/50 chance
      if (outcome) {
        resolve(`${label}: success`);
      } else {
        reject(new Error(`${label}: failure`));
      }
    }, delay);
  });
};

// Calling the round function 3 times
Promise.allSettled([
  round("Round 1", 300),
  round("Round 2", 400),
  round("Round 3", 500),
])
  .then((results) => {
    results.forEach((result) => {
      if (result.status === "fulfilled") {
        console.log(`Success! ${result.value}`);
      } else {
        console.error(`Failure! ${result.reason.message}`);
      }
    });
  })
  .catch((err) => {
    console.error("Unexpected error:", err);
  });
