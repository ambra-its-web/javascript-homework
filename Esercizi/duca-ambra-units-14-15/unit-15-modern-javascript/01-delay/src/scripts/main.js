/**
 * Delay
 * Author: Ambra Duca
 * This file:
 * - implements a function `delay` that returns a promise that resolves after a specified delay
 * - defines 3 test functions for it to make sure it works on regular functions, arrow functions and anonymous ones
 * - tests these 3 functions on the `delay` function to log their messages with a delay
 * - also tests an inline anonymous function as a bonus
 */

const delay = (ms) => {
  return new Promise((resolve) => {
    setTimeout(resolve, ms);
  });
};

// Examples of compatibility with the types of function requested

// Regular function
function normalFunc() {
  console.log("A perfectly normal function just executed after the delay!");
}

// Arrow function
const arrowFunc = () => {
  console.log("A pointy arrow function just executed after the delay!");
};

// Anonymous function
const anonFunc = function () {
  console.log("A mysterious anonymous function just executed after the delay!");
};

// Invoking the delay function on all 3 others
delay(300).then(normalFunc); // Regular function
delay(500).then(arrowFunc); // Arrow function
delay(700).then(anonFunc); // Anonymous function

// Extra: inline anonymous functions also work
delay(1000).then(() => {
  console.log("An inline anonymous function just executed after the delay!");
});
