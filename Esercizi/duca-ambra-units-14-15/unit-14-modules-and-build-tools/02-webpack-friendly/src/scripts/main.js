/**
 * Dishwasher System Simulation
 * Author: Ambra Duca
 * 
 * This script simulates a dishwasher system using three stacks to represent dirty and clean dishes.
 * The simulation involves:
 * - Random number of dirty dishes (between 10 and 15) in three dirty stacks
 * - One clean stack to store clean dishes after washing
 * - Functions to wash dishes, display the current state of the stacks, and run the washing simulation
 * - Bonus feature: The dishwasher can wash two dishes at a time
 * 
 * Functions:
 * - runSimulation: Main function to start the washing process
 * - nextStep: Manages washing of dishes and schedules the next step
 * - generateRandomStack: Creates a stack of dirty dishes with a random count
 * - getRandomNumber: Generates a random number within a specified range
 * - washDish: Moves dishes from dirty stacks to the clean stack
 * - drawStacks: Updates the DOM to reflect the current state of the stacks\
 * 
 * In this version I added a helper function that removes the DOM elements when the dishes move over, and I made use of for of, for each, Array.from and template literals instead of string concatenation to make the code cleaner and more readable
 */

import '../styles/main.css';

// Object to hold the dirty stacks and the clean one
const dishwasher = {
  dirtyStacks: [
      generateRandomStack(), // Create the first dirty stack with a random number of dishes
      generateRandomStack(), // Create the second dirty stack with a random number of dishes
      generateRandomStack()  // Create the third dirty stack with a random number of dishes
  ],
  cleanStack: [] // Initialize the clean stack as empty
};

/**
* Function that runs the simulation of washing dishes with random delays.
* It checks if any dirty stacks still have dishes and continues the process until all dishes are clean.
* @param {Object} dishwasher - The dishwasher system state containing dirty and clean stacks
*/
function runSimulation(dishwasher) {
  /**
   * Function that represents a step in the washing process.
   * It washes dishes if any dirty stacks still have dishes and sets a random delay for the next step.
   * If all dishes are clean, it ends the simulation.
   */
  function nextStep() {
      // Check if any dirty dishes remain
      let dirtyDishesRemaining = false;
      for (const stack of dishwasher.dirtyStacks) {
          if (stack.length > 0) {
              dirtyDishesRemaining = true;
              break;
          }
      }

      if (dirtyDishesRemaining) {
          washDish(dishwasher); // Move dishes from dirty to clean stack
          drawStacks(dishwasher); // Update the DOM with the current state of the stacks
          setTimeout(nextStep, getRandomNumber(500, 1500)); // Random delay between steps
      } else {
          console.log('BEEP, BEEP, BEEP, BEEP'); // End the simulation when all dishes are clean
      }
  }

  nextStep(); // Start the simulation
}

/**
* Function that generates a stack of dishes with a random number of plates.
* - Generates a random number between 10 and 15 for the stack size
* @returns {Array} - The generated stack of dishes, represented as an array of strings
*/
function generateRandomStack() {
  return Array.from({ length: getRandomNumber(10, 15) }, (_, i) => `Dish ${i + 1}`);
}

/**
* Function that generates a random number between min and max.
* - Utilizes Math.random to generate a number within the specified range
* @param {number} min - The minimum value for the random number
* @param {number} max - The maximum value for the random number
* @returns {number} - The generated random number
*/
function getRandomNumber(min, max) {
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

/**
* Function that moves up to two dishes from the dirty stacks to the clean stack.
* - Iterates over dirty stacks to move dishes to the clean stack
* @param {Object} dishwasher - The dishwasher object containing the stacks
*/
function washDish(dishwasher) {
  let washedCount = 0;

  for (const stack of dishwasher.dirtyStacks) {
      if (stack.length > 0 && washedCount < 2) {
          dishwasher.cleanStack.push(stack.pop());
          washedCount++;
      }
      if (washedCount === 2) break; // Stop after washing two dishes
  }
}

/**
* Function that updates the DOM to display the current state of all stacks.
* - Clears the current content of the stacks in the DOM
* - Displays the updated state of the dirty and clean stacks
* @param {Object} dishwasher - The dishwasher object containing the stacks
*/
function drawStacks(dishwasher) {
  // Update each dirty stack container
  document.querySelectorAll('.dirty-stack').forEach((container, index) => {
      clearChildren(container); // Clear existing child elements
      const stackTitle = document.createElement('h2');
      stackTitle.textContent = `Dirty Stack ${index + 1}`;
      container.append(stackTitle);

      const dishList = document.createElement('ul');
      dishwasher.dirtyStacks[index].forEach(dish => {
          const dishItem = document.createElement('li');
          dishItem.textContent = dish;
          dishList.append(dishItem);
      });

      container.append(dishList);
  });

  // Update the clean stack container
  const cleanStackContainer = document.querySelector('.clean-stack');
  clearChildren(cleanStackContainer); // Clear existing child elements

  const cleanTitle = document.createElement('h2');
  cleanTitle.textContent = 'Clean Stack';
  cleanStackContainer.append(cleanTitle);

  const cleanDishList = document.createElement('ul');
  dishwasher.cleanStack.forEach(dish => {
      const dishItem = document.createElement('li');
      dishItem.textContent = dish;
      cleanDishList.append(dishItem);
  });

  cleanStackContainer.append(cleanDishList);
}

/**
* Helper function to clear all child nodes of a given DOM element.
* @param {HTMLElement} element - The DOM element to clear
*/
function clearChildren(element) {
  while (element.firstChild) {
      element.removeChild(element.firstChild);
  }
}

// Initialize the simulation and display the initial state of the stacks when the DOM content is loaded
document.addEventListener('DOMContentLoaded', () => {
  drawStacks(dishwasher); // Display the initial state of the stacks
  runSimulation(dishwasher); // Start the washing simulation
});