/**
 * @file main.js
 * @author Ambra
 * @description This file adds a method to the String prototype and uses it for 2 console outputs.
 * - First the method dogSpeak is added to the String prototype and defined
 *  - It logs a string containing the current variable through `this`
 * - Invokes the method from a defined string variable and then a string not declared as variable, both instances of String
 */

// Adding an anonymous function as a method to the String prototype
String.prototype.dogSpeak = function() {
    console.log(`${this} Woof!`) // The method prints the string, instance of String, that first invokes the method concatenated with a predefined one
}

// Declaring a string variable and using the new String method on it 
let s = 'We like to learn';
s.dogSpeak(); // Output: 'We like to learn Woof!'

// Using the new String method on a string not explicitly declared as variable
'Dogs are smart'.dogSpeak(); // Output: 'Dogs are smart Woof!'