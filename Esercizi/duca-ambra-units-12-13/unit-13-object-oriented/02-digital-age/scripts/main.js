/**
 * @file main.js
 * @author Ambra
 * @description 
 * 
 * A Video has the following methods and properties
○ title (a string)
○ seconds (a number)
○ watch(x seconds [optional]) prints "You watched X seconds of 'TITLE'" e.g. "You watched 
120 seconds of 'Lord of the rings'". If x is missing prints "You watched all SECONDS seconds 
of 'TITLE'" e.g. "You watched all 160 seconds of 'Lord of the rings'"
● A MusicVideo extends Video and has these extra methods and properties
○ artist (a string)
○ play() prints "You played 'TITLE' by 'ARTIST'" e.g. "You played 'Another Brick in the Wall' by 
'Pink Floyd'"
 * Use the prototype method, not classes, to write a constructors for Video and 
MusicVideo
○ The constructor functions accept a single config object
○ All arguments are optional, use defaults if missing
● Create an array that contains a mix of Video and MusicVideo instances
● Loop on the Array and for each item 
○ call the watch() method 
○ call the play() method only if it's a MusicVideo. Hint: Use instanceof
● Optional:
○ in a new folder, repeat the exercise using the class syntax rather than the prototype method
○ All behaviors should be identical
41

 */

// Adding an anonymous function as a method to the String prototype
function Video(config){
  this.title = config.title || 'unnamed';
  this.seconds = config.seconds || 'undetected';
}

function MusicVideo(config) {
  Video.call(this, config);
  this.artist = config.artist || 'Unknown';
}

// extending the Video function's prototype with a method
Video.prototype.watch = function (x) {
  if (!x) console.log(`You watched all ${this.seconds} seconds of ${this.title}`)
    else console.log(`You watched ${x} seconds of ${this.title}`)
}

MusicVideo.prototype = Object.create(Video.prototype);

// extending the Video function's prototype with a method
MusicVideo.prototype.play = function () {
  console.log(`You played ${this.title} by ${this.artist}`)
}

// TODO - item array for looping through
const videoA = new Video(
  {
  title: 'VideoA',
  seconds: 100
  }
);

const videoB = new Video(
  {
  title: 'VideoB',
  }
);

const videoC = new Video(
  {
  seconds: 73
  }
);

const musicVideoA = new MusicVideo(
  {
  title: 'Under the White Rainbow',
  seconds: 480,
  artist: 'Haken'
  }
);

const musicVideoB = new MusicVideo(
  {
  title: 'Goku vs Majin Vegeta',
  seconds: 320
  }
);

const musicVideoC = new MusicVideo(
  {
  title: 'MusicVideoA',
  seconds: 320
  }
);

// Test prints - TO DELETE
videoA.watch();
videoB.watch();
videoC.watch(33);      // adding argument as time watched
// videoC.play();     can't use it, it's not a method of Video
musicVideoA.watch(); // adding argument as time watched
musicVideoA.play();
musicVideoB.watch(12);
musicVideoB.play();
musicVideoC.watch();
musicVideoC.play();