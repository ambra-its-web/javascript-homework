## Author
- Name: Ambra

## Exercise: DogSpeak

### Requirements
Add a method to the String prototype called dogSpeak() that works as follows:
```js
let s = 'We like to learn';
s.dogSpeak();
'Dogs are smart'.dogSpeak();
// Console output
// We like to learn Woof!
// Dogs are smart Woof!
```
Think about the following question

*Is it a good idea to extend prototypes of built-in Javascript objects such as String, Array, etc?*

### Approach to Solution
In the JS I:
- Added the method `dogSpeak()` to the String prototype and defined it
- The method logs a string containing the current variable through `this`
  - Invoked the method from a defined string variable and then a string not declared as variable, both instances of String
  
Overall, adding methods to a built-in Javascript object feels like bad practice to me mainly for these reasons:
- It won't be any easier, if not just harder, to port it over to other files, or in general, other parts of the project
- It attaches your codebase to something that may be changed in the future, and even its slightest shifts could lead to major issues and the need for a time wasting refactoring
- Having your own objects with their methods within your code is clearer to both yourself and your colleagues, as it won't need others to scour through documentation outside of the codebase's in case the method that extends the built-in object relies on more obscure aspects of it
- Prototype inheritance usage has already been on the decline since ES6, so using them in unorthodox ways for no benefit instead of adapting to the pre-established uses found in pre-existing, older projects, can only lead to confusion

### Files
- `main.js`: Contains the logic of the exercise.
- `index.html`: HTML file with a reference to the `main.js` file and instructions to check the console for results.

To view the exercise solution, open the console and run the script in `main.js`