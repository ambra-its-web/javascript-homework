## Author
- Name: Ambra

## Exercise: Soundwave

### Requirements
- Given the array `let noisesArray = ['quack', 'sneeze', 'boom'];`
- Produce the following array: `['Quack!','qUack!!','quAck!!!','quaCk!!!!','quacK!!!!!','Sneeze!','sNeeze!!','snEeze!!!','sneEze!!!!','sneeZe!!!!!','sneezE!!!!!!','Boom!','bOom!!','boOm!!!','booM!!!!']`
- Print the resulting array to the console

### Approach to Solution
In the JS I:
- Initialized an empty array `noisierArray` to store the modified noises
- Used a `forEach` loop to iterate through each word in the `noisesArray`
- Converted each word to an array of characters using the `split` method
- Used another `forEach` loop to iterate through each character in the array of characters
- Generated a modified word by concatenating three strings:
  - The original word's characters from the first to the ith
  - The original word's ith character, capitalized
  - The original word's characters from the ith + 1 to the end
- Added an increasing number of exclamation marks to the modified word
- Stored the modified word in the `noisierArray`
- Printed the `noisierArray` to the console

### Files
- `main.js`: Contains the logic of the exercise.
- `index.html`: HTML file with a reference to the `main.js` file and instructions to check the console for results.

To view the exercise solution, open the console and run the script in `main.js`