/**
 * @file main.js
 * @author Ambra
 * @description This file modifies an array of noises by performing the following steps:
 * - Initializes an array of noises.
 * - Converts each word in the array to an array of characters using the `split` method.
 * - Iterates through each character in the word.
 * - Capitalizes the current character and adds an increasing number of exclamation marks.
 * - Stores the modified word in a new array.
 * - Prints the modified array to the console.
 */

let noisesArray = ['quack', 'sneeze', 'boom'];
let noisierArray = [];

// 1st forEach iterates through each word
noisesArray.forEach(word => {
    // Converting each string to an array of characters with the split method
    let chars = word.split('');

    // forEach iterates through each character
    chars.forEach((char, i) => {
        /* Generating a modified word by concatenating three strings:
         - The word's characters from the first to the ith not included
         - The word's ith character capitalized
         - The word's characters from the ith + 1 (so after the capitalized one) to the end
        */
        let noisierWord = word.slice(0, i) + char.toUpperCase() + word.slice(i + 1);
    
        // Adding an increasing amount of exclamation marks to the modified word per iteration
        noisierWord += '!'.repeat(i + 1);
    
        // Storing the modified word in the noisierArray
        noisierArray.push(noisierWord);
    });
    
});

// Printing the modified array to the console
console.log(noisierArray);