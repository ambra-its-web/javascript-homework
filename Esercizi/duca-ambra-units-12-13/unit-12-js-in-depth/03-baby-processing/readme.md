## Author
- Name: Ambra

## Exercise: Baby processing

### Requirements
- Using the babies array from the previous exercise:
  - Write a `getBabyOutfit()` function that returns a description of a baby's outfit.
    - e.g. "Lyla is wearing a blue shirt and red pants and a green hat"
  - Write a `feedBaby()` function that prints what a baby is eating.
    - e.g. "Lyla is eating food3, food1, food4 and food2"
    - All foods in favouriteFoods should appear but randomly each time the function is called
  - Run both functions on all the babies

### Approach to Solution
In this JS I:
- Imported the previous exercise's code which created and populated the array of babies
- Created a `getBabyOutfit()` function that takes a baby object as an argument and returns a string that describes the baby's outfit.
- Created a `feedBaby()` function that takes a baby object as an argument and prints a string that describes what the baby is eating. The function mixes the `favouriteFoods` array using the `sort()` method with a random comparison function to ensure that the foods appear in a random order each time the function is called.
- Ran both functions on all the babies using a `forEach` loop.

### Files
- `main.js`: Contains the logic of the exercise.
- `index.html`: HTML file with a reference to the `main.js` file and instructions to check the console for results.

To view the exercise solution, open the console and run the script in `main.js`.