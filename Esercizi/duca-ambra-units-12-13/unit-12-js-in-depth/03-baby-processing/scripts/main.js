  /**
 * @file main.js
 * @author Ambra
 * @description This file manages an array of elements and performs the following steps:
 * - Initializes an empty array of elements.
 * - Adds 4 different elements to the array using various methods.
 * - Iterates through the array, printing each baby's key and value pairs.
 * - Adds an "outfit" property to each baby, describing their clothing.
 * - Prints each baby again with their outfit in a formatted object.
 */

/* Code from previous exercise */
// Initializing an empty array of elements
let babiesArray = [];

// Adding object literals to the array using various methods

// array.push() to add an object to the END of the array
babiesArray.push({
    name: 'Leshawna',
    months: 6,
    noises: ['gurgle', 'giggle'],
    favouriteFoods: ['shrimps', 'lemon']
});

// array.unshift() to add an object to the START of the array
babiesArray.unshift({
    name: 'Gwen',
    months: 9,
    noises: ['giggle', 'laugh'],
    favouriteFoods: ['royal jelly', 'milk']
});

// array.concat() to add an object to the END of the array
babiesArray = babiesArray.concat([{
    name: 'Duncan',
    months: 8,
    noises: ['laugh', 'skadoosh'],
    favouriteFoods: ['mashed potato', 'plasmon biscuits']
}]);

// array spread operator to add an object to the END of the array
babiesArray = [...babiesArray, {
    name: 'Owen',
    months: 11,
    noises: ['scream', 'cry'],
    favouriteFoods: ['mango', 'lopadotemachoselachogaleokranioleipsanodrimhypotrimmatosilphiokar']
}];

// Adding an outfit property to each baby
babiesArray.forEach(baby => {
    baby.outfit = {
        shirt: 'purple',
        pants: 'white',
        shoes: 'black'
    };
});

/* Baby Processing */
// Function to get a random element from an array, will be used for food
function getRandomElement(array) {
    return array[Math.floor(Math.random() * array.length)];
}

// Function to get a description of a baby's outfit
function getBabyOutfit(baby) {
    return `${baby.name} is wearing a ${baby.outfit.shirt} shirt, ${baby.outfit.pants} pants, and ${baby.outfit.shoes} shoes.`;
}

// Function to print what a baby is eating
function feedBaby(baby) {
    // mix favouriteFoods array
    let randomFoods = baby.favouriteFoods.sort(() => Math.random() - 0.5); // negative 1st element 1st, positive it's 2nd
    console.log(`${baby.name} is eating ${randomFoods.join(', ')}.`);
}
// Run both functions on all babies
babiesArray.forEach(baby => {
    console.log(getBabyOutfit(baby));
    feedBaby(baby);
    console.log(); // separating logs between loops
});