/**
 * @file main.js
 * @author Ambra
 * @description This file clones an object using a custom clone() function.
 * Said function:
 * - creates an empty object
 * - gets an array of its keys
 * - loops through each key in the array
 * - checks if the property value is an object
 *   - if yes, it calls itself recursively to clone the nested object
 *   - if not, the function assigns the property value to the new object
 * - it declares and defines the test object
 * - it changes its name directly in code
 * - it prints out the original name's value and then the changed clone's
 * - it prints out the full cloned object
 */

/**
 * Function that clones any object
 *
 * It takes an object as an argument and returns a new one that's a clone of it
 * It does this by:
 * - creating an empty object
 * - getting an array of its keys
 * - looping through each key in the array
 * - checking if the property value is an object
 *   - if yes, it calls itself recursively to clone the nested object
 *   - if not, the function assigns the property value to the new object
 *
 * @param {Object} obj - The object to be cloned
 * @returns {Object} - A new object, clone of the original one
 */
function clone(obj) {
    // Creating an empty object
    let clonedObject = {};
  
    // Geting an array of its keys
    let keys = Object.keys(obj);
  
    // Looping through each key in the array
    for (let i = 0; i < keys.length; i++) {
      let key = keys[i];
  
      // Checking if the property value is an object and not null
      if (typeof obj[key] === 'object' && obj[key] !== null) {
        // If it is, call the clone function recursively to clone the nested object
        clonedObject[key] = clone(obj[key]);
      } else {
        // If not, assign the property value to the new object
        clonedObject[key] = obj[key];
      }
    }
  
    // Return the new object
    return clonedObject;
}  

// Object to clone and test
let person = {
    name: 'Green Mueller',
    email: 'Rigoberto_Muller47@yahoo.com',
    address: '575 Aiden Forks',
    bio: 'Tenetur voluptatem odit labore et voluptatem vel qui placeat sit.',
    active: false,
    salary: 37993,
    birth: new Date('Sun Apr 18 1965 13:38:00 GMT+0200 (W. Europe Daylight Time)'),
    bankInformation: {
      amount: '802.04',
      date: new Date('Thu Feb 02 2012 00:00:00 GMT+0100 (W. Europe Standard Time)'),
      business: 'Bernhard, Kuhn and Stehr',
      name: 'Investment Account 8624',
      type: 'payment',
      account: '34889694'
    }
};

// Cloning the object
let clonedPerson = clone(person);
  
// Changing the name of the cloned object
clonedPerson.name = 'Ambra Duca';

// Checking that the original object did not change
console.log(person.name); // Output: Green Mueller
console.log(clonedPerson.name); // Output: Ambra Duca 

// Printing the original object in full
console.log(person)

// Printing the cloned object in full
console.log(clonedPerson)