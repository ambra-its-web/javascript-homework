## Author
- Name: Ambra

## Exercise: Clone

### Requirements
- Write a function `clone()` that clones any object.
- Test it on the object provided.
- Change the name of the cloned object and make sure that the original did not change.
- Important: Write the function yourself, do not use built-in functions such as `Object.assign()`, `jQuery.extend()`, or `JSON.parse(JSON.stringify())`.

### Approach to Solution
In this JS I:
- Created a `clone()` function that takes an object as an argument and returns a new object that is a clone of the original object.
- The function does this by creating an empty object, getting an array of the object's keys using `Object.keys()`, looping through each key in the array, and checking if the property value is an object. If it is, the function calls itself recursively to clone the nested object. If it is not, the function assigns the property value to the new object.
- Tested the function on the provided object and changed the name of the cloned object to make sure that the original did not change.

### Files
- `main.js`: Contains the logic of the exercise.
- `index.html`: HTML file with a reference to the `main.js` file and instructions to check the console for results.

To view the exercise solution, open the console and run the script in `main.js`.