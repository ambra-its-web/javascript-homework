/**
 * @file main.js
 * @author Ambra
 * @description This JavaScript file implements custom versions of the Array methods: `forEach()`, `map()`, `filter()`, `indexOf()`, and `slice()` using the `reduce()` method
 * - Custom forEach: loops through each element of an array and performs a given function on it
 * - Custom map: Creates a new array populated with the results of invoking a given function on every element of the array
 * - Custom filter: Creates a new array with all elements that pass the check by the given function
 * - Custom indexOf: Returns the first index of a given element in the array, or -1 if not present
 * - Custom slice: Returns a copy of a portion of an array into a new array object selected from start to end (end not included)
 * These all do their function by using the `reduce()` method
 * 
 * Log tests are then done on the functions with test arguments
 */

/**
 * Custom implementation of Array.prototype.forEach using reduce
 * @param {Array} arr - The array to loop over
 * @param {Function} callback - The function to execute on each element
 * @returns {void} - This function does not return a value, it simply executes the callback function on each element of the array
 */
function myForEach(arr, callback) {
  arr.reduce(function (_, current, index) {
      // The callback function is invoked with the current element, its index, and the original array as args
      callback(current, index, arr);
  }, null);
}

// Test myForEach
console.log('=== Testing Array.foreach method ===');
let testArr1 = [1, 2, 3, 4];
testArr1.forEach(function (num) {
  console.log(num);
});
console.log('=== Testing myForEach function ===');
myForEach(testArr1, function (num) {
  console.log(num);
});

/**
 * Custom implementation of Array.prototype.map using reduce
 * @param {Array} arr - The array to map over
 * @param {Function} callback - The function to apply to each element
 * @returns {Array} - A new array with the mapped elements
 */
function myMap(arr, callback) {
  return arr.reduce(function (acc, current, index) {
      // The callback function is invoked with the current element, its index, and the original array as args
      // The result of the callback function is pushed to the accumulator array
      acc.push(callback(current, index, arr));
      return acc;
  }, []);
}

// Test myMap
let testArr2 = [1, 2, 3, 4];
let builtInMapResult = testArr2.map(function (num) {
  return num * 2;
});
let customMapResult = myMap(testArr2, function (num) {
  return num * 2;
});

console.log('=== Testing Array.map method ===');
console.log(builtInMapResult);
console.log('=== Testing myMap function ===');
console.log(customMapResult);

/**
 * Custom implementation of Array.prototype.filter using reduce
 * @param {Array} arr - The array to filter
 * @param {Function} callback - The function to test each element
 * @returns {Array} - A new array with the elements that pass the test
 */
function myFilter(arr, callback) {
  return arr.reduce(function (acc, current, index) {
      // The callback function is invoked with the current element, its index, and the original array as args
      // If the callback function returns true, the current element is pushed to the accumulator array
      if (callback(current, index, arr)) {
          acc.push(current);
      }
      return acc;
  }, []);
}

// Test myFilter
let testArr3 = [1, 2, 3, 4];
let builtInFilterResult = testArr3.filter(function (num) {
  return num % 2 === 0;
});
let customFilterResult = myFilter(testArr3, function (num) {
  return num % 2 === 0;
});
console.log('=== Testing Array.filter method ===');
console.log(builtInFilterResult);
console.log('=== Testing myFilter function ===');
console.log(customFilterResult);

/**
 * Custom implementation of Array.prototype.indexOf using reduce
 * @param {Array} arr - The array to search in
 * @param {} searchElement - The element to locate in the array
 * @returns {number} - The index of the element in the array, or -1 if not found
 */
function myIndexOf(arr, searchElement) {
  return arr.reduce(function (acc, current, index) {
      // If element is found, the acc is returned right away
      if (acc !== -1) return acc;
      // If current element is equal to the search element, the current index is returned
      if (current === searchElement) return index;
      // If current element is not equal to the search element, the acc is returned as is
      return acc;
  }, -1);
}

// Test myIndexOf
let testArr4 = [1, 2, 3, 4, 5];

console.log('=== Testing Array.indexOf method ===');
console.log(testArr4.indexOf(3)); // Output: 2
console.log(testArr4.indexOf(6)); // Output: -1
console.log('=== Testing myIndexOf function ===');
console.log(myIndexOf(testArr4, 3)); // Output: 2
console.log(myIndexOf(testArr4, 6)); // Output: -1

/**
 * Custom implementation of Array.prototype.slice using reduce
 * @param {Array} arr - The array to slice
 * @param {number} start - Index at which to start extraction. If not given, extraction starts from the beginning of the array.
 * @param {number} end - Index before which to end extraction. If not given, extraction ends at the end of the array.
 * @returns {Array} - A new array with the extracted elements
 */
function mySlice(arr, start, end) {
  // If start not given, set to 0
  if (start === undefined) {
    start = 0;
  }
  // If end not given, set it to length of array
  if (end === undefined) {
    end = arr.length;
  }

  // Use the reduce method to loop over the array
  return arr.reduce(function (acc, current, index) {
    // If the current index is in range, add the current element to the acc array
    if (index >= start && index < end) {
      acc.push(current);
    }
    // Return the acc array
    return acc;
  }, []);
}

// Test mySlice
let testArr5 = [1, 2, 3, 4, 5];
console.log('=== Testing Array.slice method ===');
console.log(testArr5.slice(1, 3)); // Output: [2, 3]
console.log(testArr5.slice(0, 4)); // Output: [1, 2, 3, 4]
console.log('=== Testing mySlice function ===');
console.log(mySlice(testArr5, 1, 3)); // Output: [2, 3]
console.log(mySlice(testArr5, 0, 4)); // Output: [1, 2, 3, 4]