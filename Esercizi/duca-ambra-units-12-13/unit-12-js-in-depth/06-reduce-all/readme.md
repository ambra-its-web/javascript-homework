# Custom Array Methods using `reduce()`

## Author
- Name: Ambra

## Exercise: Reduce All

### Requirements
Make sure that you fully understand the Array reduce method. Write functions that use the reduce method to implement your version of the following Array methods: 

- `forEach()`
- `map()`
- `filter()`
- `indexOf()`
- `slice()`

For each method, implement parameters and return values as in the documentation:
- **Do not use `Array.prototype`.**
- Your functions receive, as a first parameter, the array on which to operate
- All other parameters should be identical to the documentation
- You do not have to implement the `thisArg` parameter

For example, your implementation of `forEach()` could be something like this:

```js
function myForEach(arr, ...) {
  // Custom implementation here
}
```

**Note**: This exercise is harder than the ones you have done so far. Dedicate enough time for it

### Testing:
- Write tests that compare the output of your functions to those of the Array methods
- Write several and comprehensive tests for each method
- Make sure that your methods give the same output as the originals

**Note**: See the following slide with an example of how to test `myMap()`

```js
// Example of testing myMap
let testGroup = [
  [1, 2, 3, 4, 5],
  [0, 0, 3, 4, 5],
  [7, 0, 9, 74, 85, 1, 42, 3, 88]
];

let testFunc = function(num) {
  return num * 2;
};

function myMap(arr, ...) {
  // Custom map implementation here
}

console.log('==== Testing Array.map() method ====');
testGroup.forEach(function(arr) {
  console.log(arr.map(testFunc));
});

console.log('\n==== Testing the function myMap() ====');
testGroup.forEach(function(arr) {
  console.log(myMap(arr, testFunc));
});
```

**Note**: Tests for `forEach()`, `indexOf()`, `filter()`, and `slice()` will be different because the methods behave differently

### Approach to Solution

In this JavaScript exercise, I made and tested the required custom functions to mimic the `Array` methods using the method `reduce` 

- **Custom `forEach()`**:  
  The custom `myForEach()` uses `reduce()` to iterate through each element of the array and invokes a callback function on each element. Since `forEach()` does not return anything, the accumulator isn't needed and is simply ignored. This implementation ensures that the callback function is applied in the same manner as the native `forEach()` method

- **Custom `map()`**:  
  The `myMap()` function creates a new array by transforming each element of the original array using the callback provided. With `reduce()`, we accumulate the transformed elements in a new array. Each iteration applies the callback to the current element, and the result is pushed into the accumulator array, ensuring a result identical to `Array.prototype.map()`

- **Custom `filter()`**:  
  In `myFilter()`, `reduce()` is used to build a new array of elements that pass a specified test function. Each element of the array is passed to the callback function, and if the result is true, the element is added to the accumulator array. This mimics the behavior of `filter()`, where only elements satisfying the condition are included in the final result

- **Custom `indexOf()`**:  
  The `myIndexOf()` function searches for a specified element in the array and returns its index. By using `reduce()`, the accumulator stores the first found index or `-1` if the element is not present. Once the element is found, the accumulator returns the index immediately and skips further iterations

- **Custom `slice()`**:  
  The `mySlice()` method returns a shallow copy of a portion of the array, specified by a start and end index. Using `reduce()`, the accumulator collects elements within the given range and ignores the rest. This approach ensures that only the selected portion of the array is returned, just like the native `slice()` method

### General Strategy:
- **Leverage `reduce()`**:  
  `reduce()` is a versatile function that can replicate many behaviors of array methods. For each custom method, I carefully used `reduce()` to accumulate results in a way that mimics the intended functionality. This allowed me to replicate array operations without directly invoking `Array.prototype` methods
  
- **Functional consistency**:  
  The custom methods were designed to take the same parameters and return the same types of results as the native methods, ensuring seamless behavior and compatibility when tested against the original methods
  
- **Comprehensive testing**:  
  Each custom method is tested with various inputs to ensure it behaves exactly like the corresponding native method. These tests include edge cases, such as empty arrays, arrays with duplicate elements, and arrays with various data types, to ensure robustness and accuracy in the implementations

This approach to solving the exercise demonstrates a deep understanding of `reduce()` and its potential to replicate various array operations through functional programming techniques
### Files
- **`main.js`**: Contains the logic of the exercise
- **`index.html`**: HTML file with a reference to the `main.js` file and instructions to check the console for results

### Instructions to Run
To view the exercise solution, open the console and run the script in `main.js`