## Author
- Name: Ambra

## Exercise: Babies

### Requirements
- Create an empty array of babies
- Each baby should have the following properties:
  - "name" (a string)
  - "months" (age in months as a number)
  - "noises" (an array of strings)
  - "favouriteFoods" (an array of strings)
- Add 4 different babies to the array using as many different ways as possible
- Iterate through the array printing key and value pairs, e.g. `[name: "Lyla"]`
- Now add an "outfit" property to each baby in the array
  - Outfit should describe at least 3 parts of their clothing using different properties, for example `"shirt": "blue"`
- Print each baby again with their outfit in a nicely formatted object

### Approach to Solution
In the JS I:
- Initialized an empty array of babies.
- Added 4 different babies to the array using 4 different methods:
-    - array.push() to add an object to the end of the array
-    - array.unshift() to add an object to the start of the array
-    - array.concat() to add an object to the end of the array
-    - Array spread operator to add an object to the end of the array
- Looped through the array and print each baby's key and value pairs using a forEach loop and Objec- keys() method
- Added an "outfit" property to each baby in the array using a forEach loop
- Printed each baby again with their outfit in a better formatted object using a forEach loop and template literals


### Files
- `main.js`: Contains the logic of the exercise
- `index.html`: HTML file with a reference to the `main.js` file and instructions to check the console for results

To view the exercise solution, open the console and run the script in `main.js`