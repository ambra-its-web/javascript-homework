/**
 * @file main.js
 * @author Ambra
 * @description This file manages an array of elements and performs the following steps:
 * - Initializes an empty array of elements.
 * - Adds 4 different elements to the array using various methods.
 * - Iterates through the array, printing each baby's key and value pairs.
 * - Adds an "outfit" property to each baby, describing their clothing.
 * - Prints each baby again with their outfit in a formatted object.
 */

// Initializing an empty array of elements
let babiesArray = [];

// Adding object literals to the array using various methods

// array.push() to add an object to the END of the array
babiesArray.push({
    name: 'Leshawna',
    months: 6,
    noises: ['gurgle', 'giggle'],
    favouriteFoods: ['shrimps', 'lemon']
});

// array.unshift() to add an object to the START of the array
babiesArray.unshift({
    name: 'Gwen',
    months: 9,
    noises: ['giggle', 'laugh'],
    favouriteFoods: ['royal jelly', 'milk']
});

// array.concat() to add an object to the END of the array
babiesArray = babiesArray.concat([{
    name: 'Duncan',
    months: 8,
    noises: ['laugh', 'skadoosh'],
    favouriteFoods: ['mashed potato', 'plasmon biscuits']
}]);

// array spread operator to add an object to the END of the array
babiesArray = [...babiesArray, {
    name: 'Owen',
    months: 11,
    noises: ['scream', 'cry'],
    favouriteFoods: ['mango', 'lopadotemachoselachogaleokranioleipsanodrimhypotrimmatosilphiokar']
}];

// Iterating through the array and printing key-value pairs
babiesArray.forEach(baby => {
    console.log('Baby details:');
    Object.keys(baby).forEach(key => {
        console.log(`${key}: ${baby[key]}`); // using template literals
    });
    console.log(); // separating logs between loops
});

console.log('---'); // separating logs for the next loop of prints

// Adding an outfit property to each baby
babiesArray.forEach(baby => {
    baby.outfit = {
        shirt: 'purple',
        pants: 'white',
        shoes: 'black'
    };
});

// Printing each baby with their outfit in a formatted way
babiesArray.forEach(baby => {
    console.log('Baby with outfit details:');
    console.log(`Name:       ${baby.name}`);
    console.log(`Months:     ${baby.months}`);
    console.log(`Noises:     ${baby.noises.join(', ')}`);
    console.log(`Favorite Foods: ${baby.favouriteFoods.join(', ')}`);
    console.log(`Outfit:     ${baby.outfit.shirt} shirt, ${baby.outfit.pants} pants, ${baby.outfit.shoes} shoes`);
    console.log(); // separating logs between loops
});

// Note that the prototypes are printed too, and they are {}, the default