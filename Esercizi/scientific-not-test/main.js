let bigNumbers = [
    imBig = 14e+4,
    imBigger = 28e+7,
    iLookBig = 0.2e+3279,
    giBmi = 14e-4
]

let body = document.body

bigNumbers.forEach(function(number) {
    let aNumber;
    if (number > 1.7976931348623157e+308) {
        aNumber = document.createElement('p');

        aNumber.textContent('28e+7 is greater than 1.7976931348623157e+308, the max JavaScript can handle! See? it says: ')
    } else aNumber = document.createElement('p').textContent(number);
    document.appendChild(aNumber);
})