/**
 * @file: main.js
 * @author: Ambra
 * @description: This script:
 * - Defines JSON strings for car and factory objects
 * - Parses these strings into JavaScript objects using JSON.parse()
 * - Creates list items for each object and appends them to styled uls
 * - Adds the lists to the DOM to display them on the page
 */

/* Five different JSON strings for car objects */
let carOne = '{ "serialNum": 5501771, "brand": "Fiat", "model": "500c", "electric": true, "colors": ["red","blue","black"],"date": {"year": 2021,"month": 6,"day": 13},"owner": null}';

let carTwo = '{ "serialNum": 4421552, "brand": "Jeep", "model": "Good one", "electric": false, "colors": ["green","black","white"],"date": {"year": 2020,"month": 8,"day": 15},"owner": "John Doe"}';

let carThree = '{ "serialNum": 3478192, "brand": "Peugeot", "model": "French", "electric": true, "colors": ["blue","grey","black"],"date": {"year": 2019,"month": 5,"day": 10},"owner": "Franca Mente"}';

let carFour = '{ "serialNum": 6863242, "brand": "Alfa Romeo", "model": "Giulia", "electric": false, "colors": ["red","white","black"],"date": {"year": 2019,"month": 11,"day": 25},"owner": "Poe Ratcho"}';

let carFive = '{ "serialNum": 5501775, "brand": "Maserati", "model": "Pretty", "electric": false, "colors": ["black","blue","silver"],"date": {"year": 2023,"month": 4,"day": 2},"owner": null}';

// JSON string for factory object
let factory = '{ "phoneContact": 3101239939, "mainCompany": "Fiat-Chrysler Automobiles", "bankrupcy": false, "subsidiaries": ["Fiat","Chrysler","Peugeot","Jeep","Dodge","Citroen","Mopar","Ram","Opel","Maserati","DS","Alfa Romeo","Vauxhall"], "basicHumanRights": null}';

/**
 * Creates a list item element for a given object and returns the list item element
 * 
 * - Parses the JSON string into a JavaScript object using JSON.parse()
 * - Creates a new <li> element to hold the object's information
 * - Converts the JavaScript object to a formatted JSON string using JSON.stringify() with indentation for readability
 * - Sets the text content of the <li> element to this formatted JSON string
 * - Returns the <li> element containing the formatted JSON string
 * 
 * @param {string} jsonString - The JSON string to be converted into an object and displayed
 * @returns {HTMLElement} The list item element with the object's formatted data
 */
function createListItem(jsonString) {
    // Parse the JSON string into a JavaScript object
    let obj = JSON.parse(jsonString);

    // Create a new list item (<li>) element
    let li = document.createElement('li');
    
    // Convert the JavaScript object to a formatted JSON string with indentation for readability
    li.textContent = JSON.stringify(obj, null, 2);
    
    // Return the created <li> element
    return li;
}

/**
 * Renders car and factory information to the DOM by creating lists of these items and appending them to the body
 * 
 * - Creates an ul for the car objects and assigns it a class name for styling
 * - Creates a list of car JSON strings
 * - Iterates over each car JSON string, parses it into an object, creates a list item for it, and appends it to the car list
 * - Creates a second ul for the factory object and assigns it a class name for styling
 * - Parses the factory JSON string into an object, creates a list item for it, and appends it to the factory list
 * - Appends both lists to the body of the document to display them
 */
function domFactorize() {
    // Create an ul element for the car objects
    let carList = document.createElement('ul');
    carList.className = 'car'; // Adding a class to style the car list

    // Define an array of car JSON strings
    let carStrings = [
        carOne,
        carTwo,
        carThree,
        carFour,
        carFive
    ];

    // Loop over each car JSON string
    carStrings.forEach(carString => {
        // Parse each element to get the JavaScript object
        // Create a list item element for each car object
        // Append the list item to the car list
        carList.appendChild(createListItem(carString));
    });

    // Create an ul element for the factory object
    let factoryList = document.createElement('ul');
    factoryList.className = 'factory'; // Adding a class to style the factory list

    // Parse the factory JSON string to get the JavaScript object
    // Create a list item element for the factory object
    // Append the list item to the factory list
    factoryList.appendChild(createListItem(factory));

    // Append both the car and factory lists to the document body
    document.body.appendChild(carList);
    document.body.appendChild(factoryList);
}

// Invoke the domFactorize function to render the elements
domFactorize();