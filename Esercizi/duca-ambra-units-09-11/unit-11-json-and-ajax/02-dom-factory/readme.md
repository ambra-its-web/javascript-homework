## Author
- **Name:** Ambra

## Exercise: Stylish interactions

### Approach to Solution

**In the HTML** I:
- Added a `<pre>` tag with the id `content` where the JSON data would be rendered in a readable format.
- Included a script tag to link the JavaScript file that handles the JSON parsing and DOM manipulation.

**In the CSS** I:
- Defined a general style for the body
- Gave the `<h1>` general styling
- Removed bullet points froms <`ul`> along with general common styling
- Gave `<li>` elements general styling
- Used class selectors `.car` and `.factory` to differentiate their styling

**In the JS** I:
- Defined JSON strings for both car and factory objects, taken from the [cars.json](./data/json/cars.json) and[factory.json](./data/json/factory.json) files from the [previous exercise](../01-factory/data/json/)

- Made a `createListItem` function to convert JSON strings into formatted `<li>`s:
  - This function takes a JSON string as an input parameter
  - Uses `JSON.parse()` to convert the JSON string into a JavaScript object
  - Creates a new `<li>` element to hold the object’s data
  - Converts the JavaScript object back into a formatted JSON string using `JSON.stringify()` with indentation to make it more readable
  - Sets the text content of the `<li>` element to the formatted JSON string
  - Returns the `<li>` element, which will be appended to the lists on the webpage

- Made a `domFactorize` function to render the car and factory data to the DOM:
  - Creates an <`ul`> for the car objects and assigns it a class name `.car` to apply specific styling
  - Defines an array of JSON strings for the car objects
  - Iterates over each JSON string in the array, parses it, creates a `<li>` for it using the `createListItem` function, and appends each `<li>` to the car list
  - Creates another <`ul`> for the factory object and assigns it a class name `.factory`
  - Parses the factory JSON string, creates a `<li>` for it using the `createListItem` function, and appends the `<li>` to the factory list
  - Appends both the car and factory lists to the document body to display them on the webpage

- Invoked the `domFactorize` function to execute the rendering process and ensure that the JSON data is parsed, formatted, and displayed as styled `<li>`s on the page

### Files
- [index.html](./index.html): Contains the HTML structure of the webpage
- [reset.css](./styles/reset.css): Contains CSS rules to reset default browser styles
- [style.css](./styles/style.css): Contains CSS styles for the layout and presentation of the webpage
- [favicon.ico](./favicon.ico): Favicon included in the root directory as good practice
- [main.js](./scripts/main.js): Contains the JavaScript code and bulk of the exercise