#### Author
- **Name:** Ambra

## Exercise: Factory

### Requirements
- Write car.json, a JSON that represents a car object
    - Make your object complete, having at least one property of the following types
    - Number, String, Boolean, Array, Object, Null
- Write a factory.json that represents a car factory
    - Follow the same rules above
- Transform car.json into cars.json with 5 cars
- Cars should belong to a factory
    - Write two variants of factory.json
    - One that has cars directly embedded in the factory JSON structure
    - Another that uses cars referring to their IDs

### Approach to Solution
#### 1) [car.json](./data/json/car.json)
    It has all the required properties in the form of
        - Serial number, number
        - Brand, string
        - Car model, string
        - Electric, boolean
        - Colors available, array of strings
        - Date of production, object with 3 number properties
            - year
            - month
            - day
        - Owner name, null or string

#### 2) [factory.json](./data/json/factory.json)
    It has all the required properties in the form of
        - Phone contact, a number
        - Main company name, a string
        - Bankrupcy status, a boolean
        - Subsidiaries list, an array of strings
        - The truth, a null value

#### 3) [cars.json](./data/json/cars.json)
    It has an array of 5 objects that respect car.json's object structure

#### 4) [factoryEmbed.json](./data/json/factoryEmbed.json)
    It has all factory.json's properties but also has the array of objects in cars.json manually added in

#### 5) [factoryIDs.json](./data/json/factoryIDs.json)    
    It has all of factory.json's properties but also has an array of numbers called car
    thtat contains all the numerical serialNum values of the 5 objects


### Files
In the json folder found in the data directory:

- [car.json](./data/json/car.json)

- [factory.json](./data/json/factory.json)

- [cars.json](./data/json/cars.json)

- [factoryEmbed.json](./data/json/factoryEmbed.json)

- [factoryIDs.json](./data/json/factoryIDs.json)    