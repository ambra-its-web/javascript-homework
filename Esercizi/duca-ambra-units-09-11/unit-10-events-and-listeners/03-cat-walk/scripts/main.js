/**
 * @file: main.js
 * @author: Ambra
 * @description: Defines 5 functions, the first 4 returning a value
 * - squareNumber: handles the squaring of a number
 * - halfNumber: handles the halving of a number
 * - percentOf: calculates what percentage of number2 number1 is
 * - areaOfCircle: handles the calculation of the area of a circle of a given radius 
 * - calculator: while printing logs of each step and storing each result
 *  - takes one number
 *  - uses halfNumber to halve it
 *  - uses squareNumber to square that result
 *  - uses areaOfCircle to calculate the area of the value used as radius
 *  - uses percentOf to calculate what percentage that area is of the squared result from squareValue
 * Then invokes the function multiple times and prints an empty line between each
*/

// Declaring variables used throughout all variants
 // The image element gathered from the nodelist as index 0 of it
let img = document.getElementsByTagName('img')[0];

// Setting the img element's style left property to 0px
img.style.left = '0px';

// Declaring the current position as the value of the string in the property left turned into an integer
let currentPos = parseInt(img.style.left);

/**
 * Function to animate the cat
 * Moves the image 10 pixels to the right of where it started
 * by changing the left style property of the absolute positioned element
 * @param {none}
 * @returns {void}
*/
function catWalk() {
    
    img.style.left = (currentPos + 10) + 'px';
}
// window.setInterval(catWalk, 50); function scheduling commented so other variants don't speed up

/**********************     Variant 1    ***************************/

/**
 * Function variant 1
 * When the image reaches the right side of the screen, it restarts from the left
 * by checking if the current left value parsed is greater than the inner width of the window
 *  and img width difference, if so it sets the left property's value of the img to 0px
 * @param {none}
 * @returns {void}
*/
function catWalk() {
    // Parsing the string value into an integer for the current position from left property value of img
    currentPos = parseInt(img.style.left);
    img.style.left = (currentPos + 10) + 'px';
    // If the current value of left is greater than window inner width minus the img's width (so if the img risks going out)
    if (currentPos > (window.innerWidth-img.width)) {
        img.style.left = '0px'; // if true, set left's value to 0px
  }
}
// window.setInterval(catWalk, 50);

/**********************     Variant 2    ***************************/

/* Declared outside of the function variant 2
    to avoid constant reset at each call*/
let goRight = true; // Boolean that handles direction of movement

/**
 * Function variant 2
 * The cat moves backwards when it reaches the right side of the screen,
 * and forwards when it reaches the left
 * Achieved by parsing the current value of the left style property of img,
 * then checking if it has reached the right side of the screen:
 *  - if goRight is true and current position is greater or equal than
 *  diff between window inner width and img width
 *    - then set the direction to "left" by setting goRight to false
 * 
 * Else check if the cat has reached the left edge of the window.
 *    - if it has, set the direction to "right" by setting goRight to true
 * 
 * Then, to move the cat 10 pixels in the current direction:
 *    - If goRight is true, add 10px to the current left position
 *    - If goRight is false, subtract 10px
 * @param {none}
 * @returns {void}
 */
function catWalk() {
    // Parsing the string value into an integer for the current position from left property value of img
    currentPos = parseInt(img.style.left);

    /* Check if the img has reached the right edge of the window and
       if it has, set the direction to 'left' by setting goRight to false */
    if (goRight && (currentPos >= (window.innerWidth - img.width))) {
        goRight = false;
    } 
    /* Check if the img has reached the left edge of the window and
       if it has, set the direction to 'right' by setting goRight to true */
       else if (!goRight && (currentPos <= 0)) {
        goRight = true;
    }

    // Move the img 10 pixels in current direction
    // If goRight true, add 10px to pos
    // If goRight false, minus 10px to pos
    if (goRight) {
        img.style.left = (currentPos + 10) + 'px';
    } else {
        img.style.left = (currentPos - 10) + 'px';
    }
}

// window.setInterval(catWalk, 50);

/**********************     Variant 3    ***************************/

let pausing = false; // To check if the animation is paused
let originalSrc = img.src; // Save the original image source
let betterCat = './assets/bingus.webp'; // Path to the alternate image
let hasPaused = false; // Boolean to check if the img already stopped to avoid looping

/**
 * Function variant 3
 * The cat moves backwards when it reaches the right side of the screen,
 * and forwards when it reaches the left, when it reaches
 * the middle of the window, it changes the image to an alternate image,
 * pauses for 10 seconds, and then resumes walking with the original image.
 * 
 * Achieved by parsing the current value of the left style property of img,
 * then checking if it has reached the right side of the screen:
 *    - if goRight is true and current position is greater or equal than
 *  diff between window inner width and img width
 *    - then set the direction to "left" by setting goRight to false
 * 
 * Else check if the img has reached the left edge of the window.
 *    - if it has, set the direction to "right" by setting goRight to true
 *  Check if the img has reached the middle of the window.
 *    - If it has, replace the image with an alternate image, pause for 10 seconds,
 *      then replace it back with the original image and resume walking.
 * 
 * Then, to move the img 10 pixels in the current direction:
 *    - If goRight is true, add 10px to the current left position
 *    - If goRight is false, subtract 10px
 * 
 * @param {none}
 * @returns {void}
 */
 function catWalk() {
    if (hasPaused) return; // Skip the rest of the function if paused

    // Parsing the string value into an integer for the current position from left property value of img
    currentPos = parseInt(img.style.left);

    /* Check if the img has reached the right edge of the window and
       if it has, set the direction to 'left' by setting goRight to false */
    if (goRight && (currentPos >= (window.innerWidth - img.width))) {
        goRight = false;
    }
    /* Check if the img has reached the left edge of the window and
       if it has, set the direction to 'right' by setting goRight to true */
    else if (!goRight && (currentPos <= 0)) {
        goRight = true;
    }

    /* Check if the img has reached the middle of the window - ( if not paused and currentPos is >= than
    half window width minus half img width, and same for <= other side )
    /   If it has, replace the img with an alternate one, pause for 10 secs,
    /    then replace it with the original and keep walking */
    if (!hasPaused && currentPos >= (window.innerWidth / 2 - img.width / 2) && currentPos <= (window.innerWidth / 2 + img.width / 2)) {
        img.src = betterCat;
        hasPaused = true; // Set the flag to indicate the cat has paused
        setTimeout(() => {
            img.src = originalSrc;
            hasPaused = false;
        }, 10000); // Pause for 10 secs
        return; // Exit the function to stop moving the cat during the pause
    }

    // Move the img 10 pixels in current direction
    // If goRight true, add 10px to pos
    // If goRight false, subtract 10px to pos
    if (goRight) {
        img.style.left = (currentPos + 10) + 'px';
    } else {
        img.style.left = (currentPos - 10) + 'px';
    }
}

// Schedule the catWalk function to be called every 50 millisecs
window.setInterval(catWalk, 50);