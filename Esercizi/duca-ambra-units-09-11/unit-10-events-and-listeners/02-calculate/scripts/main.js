/**
 * @file: main.js
 * @author: Ambra
 * @description: Defines 5 functions, the first 4 returning a value
 * - squareNumber: handles the squaring of a number
 * - halfNumber: handles the halving of a number
 * - percentOf: calculates what percentage of number2 number1 is
 * - areaOfCircle: handles the calculation of the area of a circle of a given radius 
 * - calculator: while printing logs of each step and storing each result
 *  - takes one number
 *  - uses halfNumber to halve it
 *  - uses squareNumber to square that result
 *  - uses areaOfCircle to calculate the area of the value used as radius
 *  - uses percentOf to calculate what percentage that area is of the squared result from squareValue
 * Then invokes the function multiple times and prints an empty line between each
*/

/**
 * Function that squares a given number, returning it after
 * @param {Number} - A number to be squared
 * @returns {Number} - the squared number
*/
function squareNumber(number) {
    // Squaring of the number
    let squaredNumber = number*number;

    // Returns the result
    return squaredNumber;
}

/**
 * Function that halves a given number and returns it after
 * @param {Number} - A number to be halved
 * @returns {Number} - the halved number
*/
function halfNumber(number) {
    // Halving of the number
    let halvedNumber = number/2;

    // Returns the result
    return halvedNumber;
}

/**
 * Function that calculates what percentage a given number is of another given number, returning it after
 * @param {Number} - number1, the dividend
 * @param {Number} - number2, the divisor
 * @returns {Number} - the percentage
*/
function percentOf(number1, number2) {
    // Calculates the percentage value of number1 for number2
    let percentage = (number1/number2)*100;

    // Returns the result
    return percentage;
}

/**
 * Function that takes a radius and gets the resulting circle area, returning it after
 * @param {radius} - The radius of the circle
 * @returns {void}
*/
function areaOfCircle(radius) {
    // Calculates the area of the circle with the formula A = pi * r^2
    let area = Math.PI * Math.pow(radius, 2);

    // Returns the result
    return area;
}

// Getting all the DOM elements needed and storing them as variables

// The calculate buttons
let squareButton = document.getElementById('square-button');
let halveButton = document.getElementById('halve-button');
let percentButton = document.getElementById('percent-button');
let areaButton = document.getElementById('area-button');

// The input fields
let numToSquare = document.getElementById('square-input');
let numToHalve = document.getElementById('halve-input');
let numDividend = document.getElementById('dividend-input');
let numDivisor = document.getElementById('divisor-input');
let radius = document.getElementById('radius-input');

// The result div
let solution = document.getElementById('solution');


/* Using functions to handle the result displaying 
    without having to add it to the functions themselves (they return the value instead)
    and so that it can be applied to both click events and keyboard ones */
    
function squareEvent(){ 

    // Getting the value from the respective input field
    let num = numToSquare.value;

    // Squaring the number
    let squaredNum = squareNumber(num);

    // Adding the squared number as text of the solution div
    solution.textContent = squaredNum;

};

function halveEvent(){ 

    // Getting the value from the respective input field
    let num = numToHalve.value;

    // Halving the number
    let halvedNum = halfNumber(num);

    // Adding the halved number as text of the solution div
    solution.textContent = halvedNum;

};

function percentEvent(){ 

    // Getting the values from the respective input fields
    let num1 = numDividend.value;
    let num2 = numDivisor.value;

    // Calculating what percentage of num2 num1 is
    let percent = percentOf(num1, num2).toFixed(2);

    // Adding the percentage as text of the solution div
    solution.textContent = percent + "%";

};

function areaEvent(){ 

    // Getting the value from the respective input field
    let num = radius.value;

    // Calculating area from radius, rounding if necessary
    let area = areaOfCircle(num).toFixed(2);

    // Adding the halved number as text of the solution div
    solution.textContent = area;

};

// To avoid repeating this in an anonymous function, this the handles of all keyboard event listeners
function enterEvent(event, func) {
    if (event.key === 'Enter'); func();
};

// Adding event listeners to all buttons
// Click event listeners and keydown listeners

// For the square button
squareButton.addEventListener('click', squareEvent);
// Enter key listener
squareButton.addEventListener('keydown', function(event){
    enterEvent(event, squareEvent)
});

// For the halve button
halveButton.addEventListener('click', halveEvent);
// Enter key listener
halveButton.addEventListener('keydown', function(event){
    enterEvent(event, halveEvent)
});

// For the percent button
percentButton.addEventListener('click', percentEvent);
// Enter key listener
percentButton.addEventListener('keydown', function(event){
    enterEvent(event, percentEvent)
});

// For the area button
areaButton.addEventListener('click', areaEvent);
// Enter key listener
areaButton.addEventListener('keydown', function(event){
    enterEvent(event, areaEvent)
});