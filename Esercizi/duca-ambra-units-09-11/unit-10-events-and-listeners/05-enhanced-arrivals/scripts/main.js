/**
 * @file main.js
 * @author Ambra
 * @description This JavaScript file expands on the DOM manipulation arrivals list by adding user interaction that applies changes to the style
 * - Creates random flight information like flight numbers, statuses, destinations, and arrival times using functions such as `generateFlightNumber()`, `generateStatus()`, and `generateFlight()`.
 * - Flight List Management:
 * - Updates and displays the flight list in the DOM. Every 10 seconds, it adds new flights, updates their statuses, and removes flights that have arrived. This is done using `updateFlightList(flights)`.
 * - Lets you click a table row to see more details about a flight. It opens the row with additional details and closes it when clicked again. Only one row stays open at a time, just like an accordion, and it uses `toggleRowDetails(event)` to handle this.
 * - Allows switching between "Arrivals" and "Departures" sections with a fade-in/fade-out effect. The function `switchTab(event)` manages which section is visible.
 * - Regularly updates flight statuses, adds new flights, and removes flights that have arrived after a set delay. This is done using `setInterval()`.
 */

/**
 * @constant {Array} flights An array to store flight data.
 */
const flights = [];

/**
 * Generates a random flight number in the format "XX123".
 * @returns {string} A flight number composed of two uppercase letters followed by three digits.
 * - Defines two sets of characters: letters (A-Z) and numbers (0-9).
 * - Randomly selects two letters from the letters set.
 * - Randomly selects three numbers from the numbers set.
 * - Concatenates the two letters and three numbers to form a flight number.
 * - Returns the generated flight number.
 */
function generateFlightNumber() {
    const letters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"; // Letters for flight number
    const numbers = "0123456789"; // Numbers for flight number
    let flightNumber = ""; // Initialize flight number
    for (let i = 0; i < 2; i++) {
        flightNumber += letters.charAt(Math.floor(Math.random() * letters.length)); // Append random letter
    }
    for (let i = 0; i < 3; i++) {
        flightNumber += numbers.charAt(Math.floor(Math.random() * numbers.length)); // Append random number
    }
    return flightNumber; // Return generated flight number
}

/**
 * Generates a random status for a flight.
 * @returns {string} A flight status which can be one of "departing", "on_time", "delayed", or "arrived".
 * - Defines an array of possible flight statuses: "departing", "on_time", "delayed", and "arrived".
 * - Randomly selects one of these statuses.
 * - Returns the selected status.
 */
function generateStatus() {
    const statuses = ["departing", "on_time", "delayed", "arrived"]; // Possible flight statuses
    return statuses[Math.floor(Math.random() * statuses.length)]; // Return a random status
}

/**
 * Generates a random flight object with various details.
 * @returns {object} A flight object with:
 * - {string} flightNumber - A randomly generated flight number.
 * - {string} destination - A randomly picked city from a predefined list.
 * - {string} status - A randomly assigned flight status.
 * - {Date} arrivalTime - A randomly set arrival time within the next hour.
 * - {string} airplane - An identifier for the airplane.
 * - Defines an array of possible destination cities.
 * - Uses the `generateFlightNumber()` function to get a random flight number.
 * - Uses the `generateStatus()` function to get a random flight status.
 * - Generates a random arrival time within the next hour from the current time.
 * - Creates a unique identifier for the airplane.
 * - Returns an object with all these details.
 */
function generateFlight() {
    const cities = ["New York", "London", "Paris", "Tokyo", "Berlin", "Sydney", "Dubai", "Rome", "Moscow", "Beijing"]; // List of possible destinations
    return {
        flightNumber: generateFlightNumber(), // Generate a random flight number
        destination: cities[Math.floor(Math.random() * cities.length)], // Pick a random destination city
        status: generateStatus(), // Generate a random status
        arrivalTime: new Date(Date.now() + Math.floor(Math.random() * 3600000)), // Set arrival time within the next hour
        airplane: "Airplane " + Math.floor(Math.random() * 1000) // Create a unique airplane identifier
    };
}

/**
 * Updates the flight list displayed in the DOM for both arrivals and departures.
 * @param {Array} flights - An array of flight objects to be shown in the tables.
 * - Selects the `<tbody>` elements of both the arrivals and departures tables.
 * - Clears the existing content of the `<tbody>` to prepare for new data.
 * - Sorts the flights array by the arrival time so that the earliest flights appear first.
 * - Iterates through each flight object in the sorted array and appends it to the appropriate table based on its status.
 */
function updateFlightList(flights) {
    const arrivalsTableBody = document.querySelector("#arrivals-section tbody"); // Select the arrivals table body element
    const departuresTableBody = document.querySelector("#departures-section tbody"); // Select the departures table body element
    
    arrivalsTableBody.innerHTML = ""; // Clear existing rows in arrivals table
    departuresTableBody.innerHTML = ""; // Clear existing rows in departures table

    flights.sort((a, b) => new Date(a.arrivalTime) - new Date(b.arrivalTime)); // Sort flights by arrival time

    flights.forEach(flight => {
        const row = document.createElement("tr"); // Create a new table row
        row.className = "flight status-" + flight.status; // Set row class based on flight status
        row.dataset.flightId = flight.flightNumber; // Store flight number in dataset

        const dateCell = document.createElement("td"); // Create cell for date
        dateCell.textContent = new Date(flight.arrivalTime).toLocaleDateString(); // Set date cell content
        row.appendChild(dateCell); // Append date cell to row

        const timeCell = document.createElement("td"); // Create cell for time
        timeCell.textContent = new Date(flight.arrivalTime).toLocaleTimeString('en-GB'); // Set time cell content
        row.appendChild(timeCell); // Append time cell to row

        const destinationCell = document.createElement("td"); // Create cell for destination
        destinationCell.textContent = flight.destination; // Set destination cell content
        row.appendChild(destinationCell); // Append destination cell to row

        const flightNumberCell = document.createElement("td"); // Create cell for flight number
        flightNumberCell.textContent = flight.flightNumber; // Set flight number cell content
        row.appendChild(flightNumberCell); // Append flight number cell to row

        const statusCell = document.createElement("td"); // Create cell for status
        statusCell.className = "status " + flight.status.replace("_", "-"); // Set status cell class based on flight status
        statusCell.textContent = flight.status.replace("_", " "); // Set status cell content
        row.appendChild(statusCell); // Append status cell to row

        const airplaneCell = document.createElement("td"); // Create cell for airplane
        airplaneCell.textContent = flight.airplane; // Set airplane cell content
        row.appendChild(airplaneCell); // Append airplane cell to row

        // Create detail row for expanded details
        const detailRow = document.createElement("tr");
        detailRow.className = "flight-details";
        detailRow.dataset.flightId = flight.flightNumber;
        const detailCell = document.createElement("td");
        detailCell.colSpan = 6;
        const flightInfoDiv = document.createElement("div");
        flightInfoDiv.className = "flight-info";
        flightInfoDiv.innerHTML = "<strong>Flight Number:</strong> " + flight.flightNumber + "<br>" +
            "<strong>Destination:</strong> " + flight.destination + "<br>" +
            "<strong>Status:</strong> " + flight.status.replace("_", " ") + "<br>" +
            "<strong>Arrival Time:</strong> " + new Date(flight.arrivalTime).toLocaleTimeString('en-GB') + "<br>" +
            "<strong>Airplane:</strong> " + flight.airplane;
        detailCell.appendChild(flightInfoDiv);
        detailRow.appendChild(detailCell);
        detailRow.style.display = "none";
        
        row.addEventListener("click", () => toggleRowDetails(detailRow)); // Add click event to toggle row details
        
        // Append detail row to the table body
        if (flight.status === "departing") {
            departuresTableBody.appendChild(row); // Add to departures table
            departuresTableBody.appendChild(detailRow); // Add detail row to departures table
        } else {
            arrivalsTableBody.appendChild(row); // Add to arrivals table
            arrivalsTableBody.appendChild(detailRow); // Add detail row to arrivals table
        }
    });
}

/**
 * Toggles the display of additional details for a flight row.
 * @param {HTMLElement} detailRow - The detail row to be toggled.
 * - Retrieves the currently open detail row (if any).
 * - Checks if there's an open detail row and it's different from the clicked one.
 * - If so, hides the currently open detail row.
 * - Toggles the display of the clicked detail row.
 */
function toggleRowDetails(detailRow) {
    const currentlyOpenRow = document.querySelector("tr.flight-details:not([style='display: none;'])"); // Find open detail row

    if (currentlyOpenRow && currentlyOpenRow !== detailRow) { // Close currently open row if different
        currentlyOpenRow.style.display = "none";
    }

    detailRow.style.display = detailRow.style.display === "none" ? "table-row" : "none"; // Toggle display of clicked row
}

/**
 * Switches between "Arrivals" and "Departures" sections with a fade-in/fade-out effect.
 * @param {Event} event - The click event object.
 * - Determines which tab was clicked by accessing the `data-tab` attribute of the clicked element.
 * - Selects the sections for arrivals and departures, as well as the tab buttons.
 * - Based on which tab is clicked, shows the corresponding section (Arrivals or Departures) and hides the other one.
 * - Updates the tab buttons' styles to indicate which tab is currently active.
 * - Applies a fade-in effect to the visible section and a fade-out effect to the hidden section for a smooth transition.
 */
function switchTab(event) {
    const tab = event.target.dataset.tab; // Get the clicked tab from dataset
    const arrivalsSection = document.querySelector("#arrivals-section"); // Select arrivals section
    const departuresSection = document.querySelector("#departures-section"); // Select departures section
    const arrivalsTab = document.querySelector("#arrivals-tab"); // Select arrivals tab
    const departuresTab = document.querySelector("#departures-tab"); // Select departures tab

    // Check which tab is clicked
    if (tab === "arrivals") {
        // Show arrivals section with fade-in effect
        arrivalsSection.classList.remove("fade-out");
        arrivalsSection.classList.add("fade-in", "show");

        // Hide departures section with fade-out effect
        departuresSection.classList.remove("fade-in", "show");
        departuresSection.classList.add("fade-out", "hide");

        // Set active tab styling
        arrivalsTab.classList.add("active");
        departuresTab.classList.remove("active");
    } else {
        // Show departures section with fade-in effect
        departuresSection.classList.remove("fade-out");
        departuresSection.classList.add("fade-in", "show");

        // Hide arrivals section with fade-out effect
        arrivalsSection.classList.remove("fade-in", "show");
        arrivalsSection.classList.add("fade-out", "hide");

        // Set active tab styling
        arrivalsTab.classList.remove("active");
        departuresTab.classList.add("active");
    }
}

// Regularly update flight statuses, add new flights, and remove arrived flights.
setInterval(() => {
    if (flights.length > 0) {
        flights.forEach(flight => {
            if (flight.status !== "arrived") { // Update status if not arrived
                const statusIndex = ["departing", "on_time", "delayed", "arrived"].indexOf(flight.status); // Get current status index
                flight.status = ["departing", "on_time", "delayed", "arrived"][statusIndex + 1] || "arrived"; // Move to next status
            }
        });

        flights.forEach((flight, index) => {
            if (flight.status === "arrived") { // Schedule removal for arrived flights
                setTimeout(() => {
                    flights.splice(index, 1); // Remove flight from array
                    updateFlightList(flights); // Update flight list
                }, 60000); // Wait 60 seconds before removal
            }
        });
    }
    flights.push(generateFlight()); // Add a new flight
    updateFlightList(flights); // Update the flight list
}, 10000); // Repeat every 10 seconds

// Set up event listeners after DOM content is loaded
document.addEventListener('DOMContentLoaded', () => {
    document.querySelector("#arrivals-tab").addEventListener("click", switchTab); // Add click event to arrivals tab
    document.querySelector("#departures-tab").addEventListener("click", switchTab); // Add click event to departures tab
});