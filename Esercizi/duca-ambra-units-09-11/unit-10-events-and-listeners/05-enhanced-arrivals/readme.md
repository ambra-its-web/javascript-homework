## Author
- Name: Ambra

## Exercise: Arrivals

### Requirements


### Approach to Solution


### Files
- `main.js`: Contains the implementation of the two functions.
- `index.html`: HTML file with a reference to the `main.js` file and instructions to check the console for results.

To view the exercise solution, open the console and run the script in `main.js`.