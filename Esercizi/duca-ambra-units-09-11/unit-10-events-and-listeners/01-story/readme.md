#### Author
- **Name:** Ambra

## Exercise: Story

### Requirements
- Start with a given HTML
- Add an event listener to the button so that it calls a makeStory function when clicked.
- In the makeStory function, retrieve the current values of the form input elements, make a story from them, and output that in the story div (like "Joseph really likes pink cucumbers.")

### Approach to Solution


### Files
- `main.js`: Contains the bulk of the exercise
- `index.html`: HTML file with a reference to the `main.js` file and instructions to check the console for results.