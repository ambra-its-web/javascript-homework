/**
 * @file main.js
 * @author Ambra
 * @description This script generates a simple story based on user input from an HTML form.
 * It listens for a button click event and uses the values entered in the form fields to create a story,
 * which is then displayed on the page.
 *
 * The script includes:
 * - Event listener setup for the button click
 * - A function to gather input values, construct a story, and update the page content
 *
 * HTML elements used:
 * - A button with the ID 'gen-button' which triggers the story generation
 * - Input fields with IDs 'noun', 'adjective', and 'person' for user input
 * - A div with the ID 'story' to display the generated story
 *
 * Functions:
 * - makeStory: is the click event handler, gathers input values, adds them as a string, and updates the content of the story div
 */

// Get the button element by its ID
let button = document.getElementById("gen-button");

/**
 * Function to generate and display a story based on user input in a form
 * - Gets the values from the input fields with IDs 'noun', 'adjective', and 'person'
 * - Makes a story string by concatenating these values
 * - Updates the content of the div with the ID 'story' to display the generated story
 *
 * @param {Event} event - The click event object from the button click
 */
function makeStory(event) {
  // Getting info from the form
  let noun = document.getElementById("noun").value; // Gets the value from the 'noun' input field
  let adjective = document.getElementById("adjective").value; // Gets the value from the 'adjective' input field
  let name = document.getElementById("person").value; // Gets the value from the 'person' input field

  // Making a story string by concatenating the input values
  let story = name + " listens to " + adjective + " " + noun;

  // Updating the content of the 'story' div with the constructed story
  document.getElementById("story").textContent = story;
}

/* Adding an event listener to the button for the 'click' event
which triggers the makeStory function, event handler here, when the button is clicked */
button.addEventListener("click", makeStory); // Event listener added with default useCapture = false
