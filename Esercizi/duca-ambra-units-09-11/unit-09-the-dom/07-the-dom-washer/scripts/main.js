/**
 * Dishwasher System Simulation
 * Author: Ambra Duca
 * 
 * This script simulates a dishwasher system using three stacks to represent dirty and clean dishes.
 * The simulation involves:
 * - Random number of dirty dishes (between 10 and 15) in three dirty stacks
 * - One clean stack to store clean dishes after washing
 * - Functions to wash dishes, display the current state of the stacks, and run the washing simulation
 * - Bonus feature: The dishwasher can wash two dishes at a time
 * 
 * Functions:
 * - runSimulation: Main function to start the washing process
 * - nextStep: Manages washing of dishes and schedules the next step
 * - generateRandomStack: Creates a stack of dirty dishes with a random count
 * - getRandomNumber: Generates a random number within a specified range
 * - washDish: Moves dishes from dirty stacks to the clean stack
 * - drawStacks: Updates the DOM to reflect the current state of the stacks
 */

// Object to hold the dirty stacks and the clean one
const dishwasher = {
    dirtyStacks: [
        generateRandomStack(), // Create the first dirty stack with a random number of dishes
        generateRandomStack(), // Create the second dirty stack with a random number of dishes
        generateRandomStack()  // Create the third dirty stack with a random number of dishes
    ],
    cleanStack: [] // Initialize the clean stack as empty
};

/**
 * Function that runs the simulation of washing dishes with random delays.
 * It checks if any dirty stacks still have dishes and continues the process until all dishes are clean.
 * @param {Object} dishwasher - The dishwasher system state containing dirty and clean stacks
 */
function runSimulation(dishwasher) {
    /**
     * Function that represents a step in the washing process.
     * It washes dishes if any dirty stacks still have dishes and sets a random delay for the next step.
     * If all dishes are clean, it ends the simulation.
     */
    function nextStep() {
        // Check if any dirty dishes remain
        let dirtyDishesRemaining = false;
        for (let i = 0; i < dishwasher.dirtyStacks.length; i++) {
            if (dishwasher.dirtyStacks[i].length > 0) {
                dirtyDishesRemaining = true;
                break;
            }
        }

        if (dirtyDishesRemaining) {
            washDish(dishwasher); // Move dishes from dirty to clean stack
            drawStacks(dishwasher); // Update the DOM with the current state of the stacks
            setTimeout(nextStep, getRandomNumber(500, 1500)); // Random delay between steps
        } else {
            console.log('BEEP, BEEP, BEEP, BEEP'); // End the simulation when all dishes are clean
        }
    }

    nextStep(); // Start the simulation
}

/**
 * Function that generates a stack of dishes with a random number of plates.
 * - Generates a random number between 10 and 15 for the stack size
 * @returns {Array} - The generated stack of dishes, represented as an array of strings
 */
function generateRandomStack() {
    const stackSize = getRandomNumber(10, 15); // Generate a random number between 10 and 15
    const stack = [];
    for (let i = 0; i < stackSize; i++) {
        stack.push('Dish ' + (i + 1)); // Add a dish to the stack
    }
    return stack; // Return the populated stack
}

/**
 * Function that generates a random number between min and max.
 * - Utilizes Math.random to generate a number within the specified range
 * @param {number} min - The minimum value for the random number
 * @param {number} max - The maximum value for the random number
 * @returns {number} - The generated random number
 */
function getRandomNumber(min, max) {
    let random = Math.random(); // Generate a random decimal between 0 (inclusive) and 1 (exclusive)
    let bigger = random * (max - min); // Scale the random number to the desired range
    let result = Math.round(bigger + min); // Adjust to the minimum value and round to the nearest integer
    return result; // Return the result
}

/**
 * Function that moves up to two dishes from the dirty stacks to the clean stack.
 * - Iterates over dirty stacks to move dishes to the clean stack
 * @param {Object} dishwasher - The dishwasher object containing the stacks
 */
function washDish(dishwasher) {
    for (let i = 0; i < 2; i++) { // Wash two dishes at a time
        for (let j = 0; j < dishwasher.dirtyStacks.length; j++) {
            if (dishwasher.dirtyStacks[j].length > 0) { // Check if the current dirty stack has dishes
                const dish = dishwasher.dirtyStacks[j].pop(); // Remove the top dish from the dirty stack
                dishwasher.cleanStack.push(dish); // Add the removed dish to the clean stack
                break; // Stop after washing one dish from the stack
            }
        }
    }
}

/**
 * Function that updates the DOM to display the current state of all stacks.
 * - Clears the current content of the stacks in the DOM
 * - Displays the updated state of the dirty and clean stacks
 * @param {Object} dishwasher - The dishwasher object containing the stacks
 */
function drawStacks(dishwasher) {
    const dirtyStackContainers = document.querySelectorAll('.dirty-stack');
    dirtyStackContainers.forEach(function(container, index) {
        // Update each dirty stack container with the current dishes
        let dirtyStackHTML = '<h2>Dirty Stack ' + (index + 1) + '</h2><ul>';
        dishwasher.dirtyStacks[index].forEach(function(dish) {
            dirtyStackHTML += '<li>' + dish + '</li>';
        });
        dirtyStackHTML += '</ul>';
        container.innerHTML = dirtyStackHTML;
    });

    const cleanStackContainer = document.querySelector('.clean-stack');
    // Update the clean stack container with the current clean dishes
    let cleanStackHTML = '<h2>Clean Stack</h2><ul>';
    dishwasher.cleanStack.forEach(function(dish) {
        cleanStackHTML += '<li>' + dish + '</li>';
    });
    cleanStackHTML += '</ul>';
    cleanStackContainer.innerHTML = cleanStackHTML;
}

// Initialize the simulation and display the initial state of the stacks when the DOM content is loaded
document.addEventListener('DOMContentLoaded', function() {
    drawStacks(dishwasher); // Display the initial state of the stacks
    runSimulation(dishwasher); // Start the washing simulation
});