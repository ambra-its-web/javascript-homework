/**
 * @file: main.js
 * @author: Ambra
 * This javascript code:
 * - Assigns the variable bodyNode the DOM element body
 *  - Modifies its fontFamily attribute to 'Arial, sans-serif'
 * - Selects each of the spans by their IDs (nickname, favorites,
 *  hometown) and assigns them to a respective variable (nickSpan, favSpan, homeSpan)
 *  - Replaces each one's content one at a time with the .textContent method
 *  - Retrieves every li element through tag name as an array-like,
 *  storing it into a variable called listItems
 *  - Turns listItems into an array with Array.from() and iterates
 *  through it with .forEach, with arguments item and an anonymous
 *  function that assigns the current item, one of the li elements, the class list-item
 * - Creates an img element assigning its value to the variable
 *  myImg, sets its src attribute, and appends it to bodyNode
 * - Assigns the link element to a variable called delayedCSS, finding it by its href attribute
 *  - Initially sets its disabled value to true, so the page renders without it at first.
 * - Declares the function enableCSS which:
 *  - Sets the delayedCSS element's disabled property to false, enabling the CSS file.
 *  - Logs its successful execution to the console.
 * - Invokes setTimeout and gives it enableCSS and 5000 (milliseconds) as arguments
 *  - This leads the style.css file to be loaded after no less than 5 seconds
 */

// Assigning a variable the DOM element body
let bodyNode = document.body;

// Modifying an attribute of the body element, the fontFamily
bodyNode.style.fontFamily = 'Arial, sans-serif';

// Selecting each of the spans by their IDs and assigning them to different variables
let nickSpan = document.getElementById('nickname');

let favSpan = document.getElementById('favorites');

let homeSpan = document.getElementById('hometown');

// Replacing each span one at a time since they were selected by ID
nickSpan.textContent = 'AmbraLemon';

favSpan.textContent = 'metal, tarantulas, make-up, videogames, visual novels, poetry';

homeSpan.textContent = 'Venaria Reale';

// Retrieving every li through the tag name as an array-like
let listItems = document.getElementsByTagName('li')

/* Turning the array-like into an array with Array.from() and
iterating through it with .forEach, with arguments item and
an anon function that assigns the current item, one of the li
elements, the class list-item */
Array.from(listItems).forEach(item => {
    item.className = 'list-item'
});

// Creating img element and appending it in the body
let myImg = document.createElement('img');

// myImg.src = '../assets/normal-pic-of-me.jpg';
// Relative paths are interpreted relative to the .html, not the .js

myImg.src = './assets/normal-pic-of-me.jpg';

bodyNode.appendChild(myImg);


/********************   Delaying style.css's loading ********************/

/* Alternative method
Creating an element in JS and appending it to the HTML in a function
scheduled by setTimeout. Due to the assignment's instructions of making
a separate style.css file, this method is commented */

/* setTimeout(function() {

    // Creating a link element
    let delayedCSS = document.createElement('link');

    // Setting attributes for it
    delayedCSS.rel = 'stylesheet';
    delayed.CSS.href = './style/style.css'

    // Appending it to the head
    document.head.appendChild(delayedCSS)
    }
, 4000); */


/* Assignment method
Using .disabled on the link and enabling it in a scheduled function.
For this method a link element with href value with the style.css path needs to be manually added to the header, as per exercise instructions */

// Assign the link element to a variable, finding it by its href attribute
let delayedCSS = document.querySelector('link[href="./style/style.css"]');

// Initially disabling the CSS
delayedCSS.disabled = true;

/** CSS enabling function
 * Enables the CSS file by removing the disabled attribute from the link element.
 * 
 * This function sets the delayedCSS element's disabled property to false, enabling the CSS file.
 * It also logs a message to the console indicating that the function succesfully executed.
 */
function enableCSS() {
    delayedCSS.disabled = false;

    console.log('CSS ready to rumble')
}
// setTimeout enables the CSS file after 5 seconds
setTimeout(enableCSS, 5000);