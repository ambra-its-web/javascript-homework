#### Author Dageils
- **Name:** Ambra

## Exercise 1. About me

### Requirements
**Start with this HTML**

```html
<!DOCTYPE html>
<html>
       <head>       
        <meta charset="utf-8"/>       
        <title>About Me</title>   
        </head>   
        <body>       
            <h1>About Me</h1>       
            <ul>           
                <li>Nickname: <span id="nickname"></span></li>
                <li>Favorites: <span id="favorites"></span></li>
                <li>Hometown: <span id="hometown"></span></li>
            </ul>   
        </body>
</html>
```

- Add an external javascript file called main.js
- In JavaScript:   - Change the body style so it has a font-family of "Arial, sans-serif"   - Replace each of the spans (nickname, favorites, hometown) with your own information   - Iterate through each li and change the class to "list-item"   - Create a new img element and set its src attribute to a picture of you   - Append that element to the page

- Add an external css file using Javascript   - The external css file should make items with the .list-item class white, bold and with an orange background   - The external css file should be applied after 4 seconds


### Approach to Solution

#### HTML
In [index.html](./index.html) I:
- Used the code provided by the assignment
- Added the element `script` to link the page to `main.js`
- Added the element `link` to link the page to `style.css`

#### JavaScript
In [main.js](./scripts/main.js) I:

***Modified the document's body***
by:
- Assigning the variable `bodyNode` the DOM element body
    - Modifying its `fontFamily` attribute to 'Arial, sans-serif'

***Replaced the text content of the spans*** by:
- Selecting each of the `span` elements by their IDs ("nickname", "favorites", "hometown") and assigning them to a respective variable (`nickSpan`, `favSpan`, `homeSpan`)
- Replacing each one's content one at a time with the `.textContent` method, since I selected them by unique IDs

***Gave a collection of elements a new class*** by:
- Retrieving every `li` element through tag name with the `getElementByTagName` method as an array-like, storing it into a variable called `listItems`
- Turning `listItems` into an array with `Array.from()` and iterating through it with .forEach, with arguments 
    1) `item`
    2) an anonymous function that assigns the current item, one of the `li` elements, the class `list-item`

***Created an element and appending it to the body*** by:
- Creating an `img` element and assigning its value to the variable `myImg`, then setting its `src` attribute and appending it to `bodyNode`

***Delayed the CSS loading***
by:
- Assigning the `link` element to a variable called `delayedCSS`, finding it by its href attribute. **_NOTE:_** this method requires the link to already be present in the HTML.
- Initially setting its disabled value to **true**, so the page renders without it at first.
- Declaring the function `enableCSS` which:
    - Sets the `delayedCSS` element's disabled property to **false**, enabling the CSS file.
    - Logs its successful execution to the console.
- Invoking `setTimeout` and giving it `enableCSS` and **5000** (milliseconds) as arguments
    - This leads the [style.css](./style/style.css) file to be loaded after no less than 5 seconds

#### CSS
In [style.css](./style/style.css) I:

- Changed `img`'s width and height so it displays fully in the page, not in main.js for separation of concerns.
- Gave the style rules specified by the assignment to `.list-item`, the class created through JavaScript

### Files
- [main.js](./scripts/main.js): Contains the implementation of the DOM manipulation.
- [index.html](./index.html): The provided HTML file with the references to the `main.js` and `style.css` files added.
- [style.css](./style/style.css): The CSS that loads after 5 seconds.