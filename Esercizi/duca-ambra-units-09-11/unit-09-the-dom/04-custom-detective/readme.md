## Author
- Name: Ambra

## Exercise: Book list

### Requirements
- Choose a news website that you like
- Use the devtools to view the DOM and write Javascript in the console
- Use the DOM access methods to find:
    - At least 10 different elements or collections of elements in the page
    - Choose interesting elements that require complex selectors to reach
- Produce a readme.md file with
    - A link to the website that you chose
    - snippets of your Javascript code
    - explanations of what which elements they select

### Approach to Solution
#### 1) Choose a news website that you like
I chose [theonion.com](https://www.theonion.com/), the most reliable source of verifiable and unbiased information on the internet

#### 2) Use the DOM access methods to find at least 10 different elements or collections of elements in the page

1) Anchors in the nav bar

``` js
let navElements = document.querySelectorAll('nav a');
console.log(navElements)
```

Result:
``` js
NodeList(24) [ a.sc-1out364-0.dPMosf.sc-dfwuc8-0.dxEWgq.js_link, a.sc-1out364-0.dPMosf.sc-dfwuc8-0.dOLUJI.js_link, a.sc-1out364-0.dPMosf.sc-dfwuc8-0.dOLUJI.js_link, a.sc-1out364-0.dPMosf.sc-dfwuc8-0.dOLUJI.js_link, a.sc-1out364-0.dPMosf.sc-dfwuc8-0.dOLUJI.js_link, a.sc-1out364-0.dPMosf.sc-dfwuc8-0.dOLUJI.js_link, a.sc-1out364-0.dPMosf.sc-dfwuc8-0.dOLUJI.js_link, a.sc-1out364-0.dPMosf.sc-dfwuc8-0.dOLUJI.js_link, a.sc-1out364-0.dPMosf.sc-1op20w0-1.hVkYOh.js_link, a.sc-1out364-0.dPMosf.sc-1op20w0-1.hVkYOh.js_link, … ]
```
We get a node list, so a full on array, with all the anchors within the nav as its elements. We also get to see that working with classes won't be a good idea on this site, since they seem to be generated instead of having a characterizing name

2) The last span of every container
``` js
let spannungs = document.querySelectorAll('span:last-of-type');
console.log(spannungs)
```

Result:
``` js
NodeList(83) [ span.sc-iyvn34-0.keEGDF, span.sc-iyvn34-0.keEGDF, span.sc-yijpc3-1.ekMpje, span.sc-iyvn34-0.keEGDF, span.sc-yijpc3-1.ekMpje, span.sc-1il3uru-3.huoaKL, span.sc-iyvn34-0.keEGDF, span.sc-iyvn34-0.keEGDF.icon-hamburger, span, span, … ]
```

We get another node list with all the last spans present in each container

3) The very last element present in the page
**W.I.P**
**_[NOTE:]_** Try putting the code in a setTimeout to delay its execution so that every request is fulfilled and the page is fully loaded to avoid unpredictable results

4) Every fourth h4 element
``` js
let fourthFours = document.querySelectorAll('h4:nth-of-type(4)');
console.log(fourthFours)
```
0 offset, 4th as a pattern

Result
``` js

```