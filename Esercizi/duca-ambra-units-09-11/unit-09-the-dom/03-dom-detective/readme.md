## Author
- Name: Ambra

## Exercise: DOM Detective

### Requirements
Choose a news website that you like
- Use the devtools to view the DOM and write Javascript in the console
- Use the DOM access methods to find:
    - At least 10 different elements or collections of elements in the page
    - Choose interesting elements that require complex selectors to reach
- Produce a readme.md file with
    - A link to the website that you chose
    - snippets of your Javascript code
    - explanations of what which elements they select

### Approach to Solution
1) Find every image on the page
``` js
let images = document.querySelectorAll('img');
console.log(images)
```
I used the querySelectorAll method with the string 'img' as argument to get the list of img element nodes from the DOM of the page.

Result:
``` js
NodeList(278) [ img.ng-star-inserted, img.ng-star-inserted, img.ng-star-inserted, img.ng-star-inserted, img.ng-star-inserted, img.ng-star-inserted, img.ng-star-inserted, img.ng-star-inserted, img.ng-star-inserted, img.ng-star-inserted, … ]
```
Note that when a statement is ran through the DevTools console, it will return undefined if it has no return value. The definition of the variable returned undefined right after the NodeList. One way to avoid it is to avoid defining variables like this:
``` js
console.log(document.querySelectorAll('img'))
```

2) Find the main menu at the top of the page
I looked for the main menu at the top, which is a nav element, by using the document method `getElementsByTagName`, which returns an HTMLCollection, an array-like
``` js
let mainMenu = document.getElementsByTagName('nav');
console.log(mainMenu)
```
Result:
``` js
HTMLCollection { 0: nav.menu.menu-prices-in-eur.menu--windows.menu-curr-symbol-before.menu-language-en-us, length: 1 }
```
We get an array-like of length equal to one, the only nav element present in the page

Another solution would have been to search the element through a `querySelector` like this:
``` js
let mainMenu = document.querySelector('nav');
console.log(mainMenu)
```

To get this result instead:
``` html
<nav class="\n            menu\n    …n            \n        " gog-menu="">
```
If we toggle the dropdown, we get every information about the nav, with their own dropdown arrows, which is way too many to bring here since they go above 100 lines of code without opening further dropdowns. This information includes its HTML attributes, ARIA attributes (like meaning roles for better accessibility), its structure and its children, its styles, positioning, its event listeners and so on


3) Find all the news items under "News"
For this one I had to inspect one of the items in the news section and check what it was in the HTML structure and if it had common classes with the other. I searched by their common classes to get every element, regardless of type or hierarchy
``` js
let newsItems = document.getElementsByClassName('ng-star-inserted');
console.log(newsItems)
```

Result:
``` js
HTMLCollection { 0: takeover.ng-star-inserted, 1: big-spot-section.ng-star-inserted, 2: section.big-spot__wrapper.ng-star-inserted, 3: span.section-header__text.ng-star-inserted, 4: slider-navigation.ng-star-inserted, 5: div.swiper-pagination.swiper-pagination-clickable.swiper-pagination-bullets.swiper-pagination-horizontal.ng-star-inserted, 6: div.swiper-slide.swiper-slide-duplicate.ng-star-inserted, 7: big-spot.ng-star-inserted, 8: div.big-spot__background-picture.big-spot__background-picture--mobile.ng-star-inserted, 9: source.ng-star-inserted, … }
```
We get yet another HTMLCollection, so another array-like

4) Find the footer
The same logic applied to the nav could be applied here. For the sake of brevity, I used `getElementsByTagName`
``` js
let footer = document.getElementsByTagName('footer');
console.log(footer)
```

Result:
``` js
HTMLCollection { 0: footer.footer-microservice.main-footer, length: 1 }
```
We get another collection containing one element

5) Find all the social media links at the bottom of the page
By inspecting the social icons I found out that the anchors in the footer that link to a social media all share a class, so I searched them by class:
``` js
let socialLinks = document.getElementsByClassName('footer-microservice-socials__item');
console.log(socialLinks)
```

Result:
``` js
HTMLCollection { 0: a.footer-microservice-socials__item, 1: a.footer-microservice-socials__item, 2: a.footer-microservice-socials__item, 3: a.footer-microservice-socials__item, 4: a.footer-microservice-socials__item, 5: a.footer-microservice-socials__item
, length: 6 }
​
0: <a class="footer-microservice-socials__item" target="_blank" href="https://facebook.com/gogcom">​
1: <a class="footer-microservice-socials__item" target="_blank" href="https://twitter.com/gogcom">​
2: <a class="footer-microservice-socials__item" target="_blank" href="http://www.twitch.tv/gogcom">​
3: <a class="footer-microservice-socials__item" target="_blank" href="https://facebook.com/gogcom">​
4: <a class="footer-microservice-socials__item" target="_blank" href="https://twitter.com/gogcom">​
5: <a class="footer-microservice-socials__item" target="_blank" href="http://www.twitch.tv/gogcom">
​
length: 6
​
<prototype>: HTMLCollectionPrototype { item: item(), namedItem: namedItem(), length: Getter, … }
```
This is the view upon opening the HTMLCollection's dropdown. It's interesting to see there's 6 elements in it despite the displayed and interactable links being only 3 at a time.
The links seem to be duplicated because the latter 3 are copies made for the mobile view of the page to replace the previous 3 meant for desktop view, verifiable by inspecting the items before and after changing viewport width to meet the breakpoint.