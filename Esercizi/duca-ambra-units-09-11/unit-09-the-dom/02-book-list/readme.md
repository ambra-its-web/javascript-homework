## Author
- Name: Ambra

## Exercise: Book list

### Requirements
- Create a complete webpage with a title, description and all other HTML tags
- In the body add an h1 title of "My Book List"
- In javascript, iterate through the array of books.
    - For each book, create HTML element with the book title and author and append it to the page
    - Use a ul and li to display the books
    - Add a url property to each book object that contains the cover image of the book
    - Add the image to the HTML using Javascript
    - Using javascript change the style of the book depending on whether you have read it or not
- Add an external css file that applies after 5 seconds
    - Now change the style of the book depending on whether you have read it or not using both css and javascript (the CSS should use a different color for read books)

### Approach to Solution
This script is based on the initial structure of the exercise _Reading list_ from _unit-07_.

In `index.html`, I only:
 - Linked the `reset.css` file
 - Added an `<h1>` element saying "My Book List"
 - Linked the `main.js` file using `defer`

In `main.js` I:
 - Started from the same object array `books` from _Reading list_
    - Each item has a 4 properties:
        - `title`, string
        - `author`, string
        - `alreadyRead`, boolean
        - `cover`, string - an url to an image resource. **_NOTE:_** it was added during the implementation of the image elements in the javascript, directly to the objects in the array and not by adding the class in the middle of the execution itself
 - Assigned the body DOM property to the variable `body`
 - Assigned a created `ul` element as value of the variable `bookList`
 - Appended `bookList` to the body
 - Defined the **function `generateBook`** with `book` as parameter:
    - It creates a `li` element and assigns it as value of the variable `listedBook`
    - Modifies the `textContent` property of the list item, making it a string composed of the book properties `title` and `author`
    - Creates the img element and assigned it as value of the variable`bookCover`
    - Assigned to `bookCover`'s `src` property the value of the `book.cover` property, the url. Using `book` so when the function is cycled through the array it applies it to all array objects
    - Appends the list item to the unordered list
    - Appends the image to the list item
    - Styles the borders of the book covers through the `style` object of `bookCover`
        - Its color is based on the `alreadyRead` property value:
            - if true, the border is blue
            - else, it's red
            **_NOTE:_** this is bad practice, javascript shouldn't be used to directly assign inline styling because of 1) mixing typing conventions, 2) its high specificity (only beat by changing it again or using `!important`), 3) low maintainability, 4) lack of separation of concerns (logic handling presentation). I only did it because I interpreted the exercise to be requesting it specifically here:
            > **Using javascript** change the style of the book depending on whether you have read it or not
    - Used the method `forEach` with the function `generateBook` as argument

    - The following function, `jsRestyling`, was implemented later in the code as it's logically subsequent to the one right after, but since the latter makes use of it, I moved it up in the main to avoid unnecessary hoisting. It will be explained later on.

    - I defined a function `jsStyling` which:
        - Defines a variable `delayedCSS` and assigns it a created `link` element as value
        - Sets attributes for the newly created DOM element, specifically `rel` set to 'stylesheet' and `href` set to the stylesheet path
        - Appends the element to the head of the document
        - Schedules the invocation of `jsRestyling` after no less than 3 seconds. 
        **_NOTE:_** the nested setTimeout is not enough as the stylesheet itself won't load at a predictable speed. To make sure it's actually no less than 3 seconds after the first one, we'd need to use an event listener `.onload` to the variable `delayed.CSS` and then starting the function 

    - The function `jsRestyling`:
        - Sets an `id` attribute with value `restyled-body`
        - Declares a `covers` variable and gives it the value of the list of CSS queries `img` as nodes through the `document method querySelectorAll`
        - Iterates over said list with `forEach` with an anonymous function with `cover` as parameter, and adds to each one a class as value `restyled-cover` to their `.classList` property
        - Defines a variable `bookTest`, whose value is a list of the list item elements gathered through the body method `getElementsByTagName`
        - Defines a variable `textList`, its value is the array obtained through the `Array.from` method applied to the array-like of list items
        - Iterates through the array with the `forEach` method, with an anonymous function as parameter which itself has `li` as one. The function adds the class 'restyled-li' to the variable's `classList` property for each list item in `textList`

- At the end, I scheduled `jsStyling`'s invocation and set it to happen in no less than 5 seconds.

In `style.css`, which is only linked when the function `reStyling` is executed, I:
- Gave the body, h1 and list a general style
- Set each list item to `display: inline-block` to allow them to display side by side
- I gave the `li img` selector general styling and the border changes asked in the exercise. I had to set it to `!important`. 
**_NOTE:_** this was necessary due to the previously mentioned exercise request to modify the border in javascript itself, requiring higher specificity to override the rule set inline. This is ***bad*** practice
- Gave the js generated `restyled-body` id some visual changes. Used gradients to make the difference more noticeable
- Gave the js generated `restyled-cover` class, applied to all images, **two** `!important` rules to override the specificity of both the inline style injected from javascript and the previous bad practice in the css file.

### Files
- `main.js`: Contains the bulk of the exercise.
- `index.html`: HTML file with a reference to the `main.js` and `reset.css` files.
- `style.css`: contains the rules to be applied upon linking of the file and upon the invocation of `reStyling`

To view the exercise solution, open the console and run the script in `main.js`.