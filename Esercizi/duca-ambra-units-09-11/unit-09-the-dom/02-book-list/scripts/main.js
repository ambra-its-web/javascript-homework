/**
 * @file: main.js
 * @author: Ambra
 * @description Implements a book list object array and displays it on the page through DOM manipulation
 * and styles them through both js and css
 *  - Starts from the same object array books from _eading list
    - Each item has a 4 properties:
        - title, string
        - author, string
        - alreadyRead, boolean
        - cover, string - an url to an image resource.
 - Assigns the body DOM property to the variable body
 - Creates an ul and stores it in a variable called bookList
 - Appendeds bookList to the body
 - Defines the function generateBook with book as parameter:
    - It creates a li element stored in the variable listedBook
    - Modifies the textContent property of the list item to display the title and author properties
    - Creates the img element and stores it in the variable bookCover
    - Stores book.cover's value, an url string, into bookCover's src property
    - Appends the list item to the unordered list
    - Appends the image to the list item
    - Styles the borders of the book covers through the style object of bookCover
        - Its color is based on the alreadyRead property value:
            - if true, the border is blue
            - else, it's red
            - this is bad practice but a deliberate choice, since the assignment specified: "Using javascript change the style of the book"
    - Uses the method forEach with the function generateBook as argument to iterate through the object array

    - The function jsRestyling, is defined before the one that invokes it to avoid hoisting

    - Defines a function jsStyling which:
        - Stores a created link element as value of delayedCSS
        - Sets attributes for it, specifically rel set to 'stylesheet' and href set to the stylesheet path
        - Appends the element to the head of the document
        - Schedules the invocation of jsRestyling after no less than 3 seconds. Due to unpredictable
        css load times, the event handler .onload as a property of delayedCSS would help the timer be no less than 3 seconds

    - The function jsRestyling:
        - Sets an id attribute with value restyled-body to body
        - Uses querySelectorAll to give a list of css queries img as value to the variable covers
        - Iterates over said list with forEach with an anonymous function with cover as parameter, and adds to each one a class as value restyled-cover to their .classList property
        - Defines a variable bookTest, whose value is a list of the list item elements gathered through the body method getElementsByTagName
        - Defines a variable textList, its value is the array obtained through the Array.from method applied to the array-like of list items
        - Iterates through the array with the forEach method, with an anonymous function as parameter which itself has li as one. The function adds the class 'restyled-li' to the variable's classList property for each list item in textList

- Schedules jsStyling's invocation and sets it to happen in no less than 5 seconds.
 */

// Array of objects
let books = [
    {
        title: "La Terra dei Figli", // First property
        author: "Gipi", // Second property
        alreadyRead: true, // Boolean value
        cover: "./assets/la-terra-dei-figli.jpg" // Property for the img src
    },
    {
        title: "The Hobbit",
        author: "J.R.R. Tolkien",
        alreadyRead: false,
        cover: "./assets/the-hobbit.jpg"
    },
    {
        title: "Pet Sematary",
        author: "Stephen King",
        alreadyRead: true,
        cover: "./assets/pet-sematary.jpg"
    },
    {
        title: "Dune",
        author: "Frank Herbert",
        alreadyRead: false,
        cover: "./assets/dune.webp"
    },
    {
        title: "The Sorrows of young Werther",
        author: "Johann Wolfgang Goethe",
        alreadyRead: true,
        cover: "./assets/young-werther.jpg"
    }
];
  
  let body = document.body
  
  // Creating element ul and holding it in the variable bookList
  let bookList = document.createElement('ul');
  
  // Appending the ul element to the body
  body.appendChild(bookList);
  
  /**
  * Function that generates a list-item for a book to be appended to an unordered list
  * - Creates a li element
  *   - Changes li's text content, concatenating two properties as its value
  * - Creates an img element and gives it as a value for its src property the current item property containing the resource
  * - Appends the li element to an ul, then the img to the li
  * 
  * @param {Object}
  * @returns {void}
  */
function generateBook(book) {
  
    // Creating li element
    let listedBook = document.createElement('li');  
  
    // Changing li's text content, composing it of .title and .author
    listedBook.textContent = book.title + ", by " + book.author;
  
    // Creating the img element
    let bookCover = document.createElement('img');
  
    // Using the element's src property and assigning the cover value of book (books[i] in the future forEach)
    bookCover.src = book.cover;
  
    // Appending the li
    bookList.appendChild(listedBook);
  
    // Then appending the img element to the current li
    listedBook.appendChild(bookCover)
  
    // Styling the img border width and style
    bookCover.style.borderWidth = '2px';
    bookCover.style.borderStyle = 'solid';
    // Styling the img through javascript based on alreadyRead's property, a boolean value
    if (book.alreadyRead)
          bookCover.style.borderColor = 'blue';
          else bookCover.style.borderColor ='red';
          // Bad practice - concerns not separated and camelcase variables that refer to css, where they're hyphenated
}
  
// Iterate through the array of books and execute generateBook for each element
books.forEach(generateBook)
  
/**
 * Function that adds classes and ids to some HTML elements
 * - Adds an id to the body
 * - Adds classes to all img elements
 * - Gets li elements by tag name and turns the array-like into an actual array
 * - Iterates through the array with the forEach method and executes a function
 * which adds a class to all the list items
 * 
 * @param {void}
 * @returns {void}
 */
function jsRestyling() {
    // Giving an id to the body
    body.setAttribute("id", "restyled-body");

    // Selecting all img elements, getting an array
    let covers = document.querySelectorAll('img');
    covers.forEach(function(cover) {
        cover.classList.add("restyled-cover");
    });

    let bookText = body.getElementsByTagName('li');
    let textList = Array.from(bookText);

    textList.forEach(function(li) {
        li.classList.add('restyled-li');
    })
}

  /**
  * Function that generates a link to an external stylesheet
  * - Creates a link element
  *   - Changes its rel and href attributes so it links to an external style.css
  * - Appends the link element to the head of the document
  * - Invokes a 3 seconds setTimeout to schedule the function jsRestyling at the end
  * 
  * @param {void}
  * @returns {void}
  */
function jsStyling() {
  
    // Creating a link element
    let delayedCSS = document.createElement('link');
  
    // Setting attributes for it
    delayedCSS.rel = 'stylesheet';
    delayedCSS.href = './styles/style.css';
  
    // Appending it to the head
    document.head.appendChild(delayedCSS);

    setTimeout(jsRestyling, 3000) // The nested setTimeout is not enough as the CSS won't load at a predictable speed. To make sure it's actually no less than 3 seconds after the first one, we'd need to use an event listener .onload to the variable delayed.CSS and then starting the function
};
  
setTimeout(jsStyling, 5000)