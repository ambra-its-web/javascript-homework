// property access - bad way
const alphabetLowercase = "abcdefghijklmnopqrstuvwxyz";
let firstChar = alphabetLowercase[0]; // 'a'
console.log(firstChar);
/* shouldn't be used since:
- uses square brackets, reserved for array notation
- strings are immutable (read only), so attempting to change a string variable with this throws no errors, while charAt does */
// charAt() - good way
let greeting = "HELLO WORLD";
let result = greeting.charAt(0); // 'H', typo on slides
console.log(result);

// .concat() vs ""+""
/* concat is generally less used, but is better at handling some specific scenarios:
- it throws an error when something that isn't a string is trying to be concatenated. this can be both good and bad ofc. + instead turns the value into a string
- it doesn't make a new string at each subsequent string concatenation, but directly concatenates into one
- it handles arrays better due to not needing a for loop to iterate through them*/
let arrConc = ["Hello", "World", "!"];
let resultConc = arr.concat().join(" ");
console.log(result2); // Output: "Hello World !"
/* In this case concat along with join made it nice and easy to handle it without a for loop. With + it'd be rougher the longer the array:*/
let arrPlus = ["Hello", "World", "!"];
let resultPlus = arr[0] + " " + arr[1] + " " + arr[2];
console.log(result); // Output: "Hello World !"
/* So in general, with arrays, concat is better
We can turn the string to an array with split if we need to start from a string too , still resulting in less steps. So if we know we need to turn a string into an array say, to use splice, we do this */