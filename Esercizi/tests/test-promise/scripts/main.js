/*
// riscrivimi con una promise
// sumTwoValues è una funz asincrona
function sumTwoValues(x, y, cb) {
  if(typeof cb !== "function") {
    return;
  }
  setTimeout(() => {
    let sum = x + y;
    cb(sum);
  }, 3000);
}

// chiamata
sumTwoValues(2, 2, result => {
  console.log(result);
});

// se sumTwoValues fosse una funz sincrona ( non funzia )
let result = sumTwoValues(3, 8);
console.log(result);


// riscrivimi con setTimeout
console.log(`main starts`);
new Promise((resolve, reject) => {
 setTimeout(() => {
 Math.random() > 0.5 ? resolve('won toss') : reject(new Error('failed toss'));
 }, 500);
})
 // handles the result
 .then(result => console.log(result))
 // handles the error
 .catch(err => console.error(err.message))
 // runs when promise resolves or rejects
 .finally(() => console.log('end of game'));
console.log(`main continues`);
*/









let italian = {greeting: "Ciao"};

function sayCiao(...words) {
  return this.greeting + " " + words.join(" ");
}

sayCiao.apply(italian, ["means", "hello", "in", "italian"]); // Ciao means hello in italian